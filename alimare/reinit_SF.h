/**
#Redistancing method of Sussman and Fatemi (SF)


*/

void smooth_heavyside(scalar phi, scalar heavyside){
  double myPi = 3.14159265358979323846 ;
  foreach(){
    if(phi>Delta)heavyside[] = 1.;
    else if(phi<-Delta)heavyside[] = 0.;
    else heavyside[] = (1.+phi[]/Delta + sin(myPi*dist[]/Delta)/myPi)*0.5;
  }
  boundary({heavyside});
  restriction({heavyside});
}

void SignDelta(scalar phi, scalar sign){
  foreach(){
    sign[] = 2.*(smooth_heavyside(phi[])-0.5);
  }
  boundary({heavyside});
  restriction({heavyside});
}


void grad_phi(vector gradphi, scalar phi,  scalar sign0){
  foreach()
    foreach_dimension()
      gradphi.x[] = 0.
  boundary({gradphi});
  restriction({gradphi});

  foreach(){
    foreach_dimension(){
      double dplusphi, dminphi;
      dplusphi = WENO_gradient_x(dist,0);
      dminphi  = WENO_gradient_x(dist,-1);
      if(dplusphi*sign0[]< 0 && dplusphi + dminphi < 0)
        gradphi.x[] = dplusphi;
      if(dminphi*sign0[]> 0 && dplusphi + dminphi < 0)
        gradphi.[] = dminphi;
    }
  }

  boundary({gradphi});
  restriction({gradphi});
}

struct reinitSF {
  scalar dist;
  scalar cs;
  face vector fs;
  double dt;
  int it_max;
};


void reinit_SF(struct reinitSF p){
  scalar dist = p.dist; 

  double dt   = p.dt;     // standard timestep (0.5*Delta)
  int it_max  = p.it_max;// maximum number of iteration (100)
  if(dt == 0) dt = 0.5 * L0/(1 << grid->maxdepth);


/**
Default number of iterations is 20 times, which is sufficient to have the first
10 neighbor cells to the 0-level-set properly redistanced.
*/

  if(it_max == 0)it_max = 5;

  
  scalar dist0[];

  foreach()
    dist0[] = dist[];

  boundary({dist0});
  restriction({dist0});

  SignDelta(dist0);
  vector graddist[];

  grad_phi(graddist, dist, dist0);
  if(dt == 0) dt = 0.5 * L0/(1 << grid->maxdepth);

  void RK3(scalar f, vector ndist, double dt, double maxd){
}