/**
#Melt of a solid particle

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = 1$. The ice particle is initially
at $T_S = -1$.

The temperature on the interface is derived from the Gibbs-Thomson relation,
withtout any anisotropic effect :
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$

![Animation of the temperature field](crystal/temperature.mp4)(loop)

~~~gnuplot Curvature
set key left
set yrange [-30:30]
set xrange [0:0.095]
plot 'log' u 1:2 w l t 'curvature min',  \
     'log' u 1:3 w l  t 'curvature max'
~~~

~~~gnuplot Evolution of the interface (zoom)
K  = "`head -1 log | awk '{print $1}'`"
K2 = "`head -1 log | awk '{print $2}'`"
K  = K + 0
K2 = K2 + 0
graph(n,n2)  = sprintf("epsK = %.1e , epsV = %.1e",n,n2)
set size ratio -1
set yrange [*:*]
set xrange [*:*]
set key top right
unset xlabel
unset xtics
unset ytics
unset border
unset margin
unset ylabel
plot 'out' w l lw 3 lc 'black' t ''
~~~

*/
#define Gibbs_Thomson 1

#include "../myheader.h"

#define T_eq         0.
#define TL_inf       1.
#define TS_inf       -1.

double TL_init(double val,double V, double t){
    return 1-exp(-V*(val-V*t));
}




#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
#ifdef EPSK
double epsK = EPSK;
#else
double epsK = 1.e-4;
#endif
#ifdef EPSV
double epsV = EPSV;
#else
double epsV = 1.e-4;
#endif

#else // Gibbs_Thomson
double  epsK = 0.000, epsV = 0.000;
#endif // Gibbs_Thomson

#define GT_aniso 0
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif

int     nb_cell_NB;
double  NB_width ;    // length of the NB

double s_clean = 1.e-10; // used for fraction cleaning


scalar curve[];


TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));
TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));

TL[top]    = dirichlet(TL_inf); 
TL[bottom] = dirichlet(TL_inf); 
TL[left]   = dirichlet(TL_inf); 
TL[right]  = dirichlet(TL_inf); 

TS[top]    = dirichlet(TS_inf); 
TS[bottom] = dirichlet(TS_inf); 
TS[left]   = dirichlet(TS_inf); 
TS[right]  = dirichlet(TS_inf); 

/**
Initial geometry definition. Here the interface equation is :

$$
r\left(1+ 0.2 *cos(8\theta) \right) - R
$$
where $r = \sqrt{x^2 + y^2}$, $\theta = \arctan(x,y)$ and $R = \frac{L_0}{5}$

Notice that the initial dist[] field is not really a distance, it is modified
after a few iterations of the LS_reinit() function.
*/
double geometry(double x, double y, double Radius) {

  coord center;
  center.x = 0.5;
  center.y = 0.5;

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = -( sqrt(R2)*(1.+0.2*cos(6*theta)) - Radius);
  // double s = -( sqrt(R2)*(1.+0.*cos(6*theta)) - Radius);


  return s;
}

/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {
  latent_heat = 1.;
  MAXLEVEL  = 6;
  MINLEVEL = 5;
  int N     = 1 << MAXLEVEL;
  init_grid (N);
  
  TL.third = true;
  TS.third = true;  
  fprintf(stderr, "%g %g\n", epsK, epsV);
  run();
}


event init(t=0){
  DT2         = 0.5*L0/(1<<MAXLEVEL);  // Delta
  // fprintf(stderr, "%g\n", DT2);
  // exit(1);

  nb_cell_NB = 1 << 3 ;               // number of cell in the 
                                      // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    dist[] = clamp(-geometry(x,y,L0/3.),-1.5*NB_width,1.5*NB_width);
  }
  boundary ({dist});
  restriction({dist});

  vertex scalar distn[];
  cell2node(dist,distn);

  fractions (distn, cs, fs);
  fractions_cleanup(cs,fs,smin = s_clean);
  boundary({cs,fs});
  restriction({cs,fs});

  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc});
  
  curvature(cs,curve);

  boundary({curve});
  foreach() {
    if(cs[] > 0)
      TL[] = TL_init(dist[],30,0.);
    else
      TL[] = 0.;
    TS[] = TS_inf;
  }
  boundary({TL,TS});
  restriction({TL,TS});

  myprop(muv,fs,lambda[0]);
#if GHIGO // mandatory for GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif

}

event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
  dist,latent_heat,cs,fs,TS,TL,T_eq,
  vpc,vpcf,lambda1,lambda2,
  epsK,epsV,aniso,deltat=0.45*L0/(1<<MAXLEVEL),
  itrecons = 60,tolrecons = 1.e-3,
  NB_width
  );
  double DT3 = 0.4*timestep_LS(vpcf,DT2,dist,NB_width);
  tnext = t+DT3;
  dt = DT3;
}

event tracer_diffusion (i++) {
  boundary({TL});
  myprop(muv,fs,lambda[0]);
  mgT = diffusion (TL,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  boundary({TS});
  mgT = diffusion (TS,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
}

/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  double lambda1 = lambda[0], lambda2 = lambda[1];    
  // fprintf(stderr, "DT2 %.2e\n", DT2);
  advection_LS(
  dist,
  latent_heat,
  cs,fs,
  TS,TL,
  T_eq,
  vpc,vpcf,
  lambda1,lambda2,
  epsK,epsV,aniso,
  curve,
  &k_loop,
  deltat = 0.45*L0 / (1 << grid->maxdepth),
  itredist = 10,
  tolredist = 3.e-3,
  itrecons = 60,
  tolrecons = 1.e-3,
  s_clean = 1.e-10,
  NB_width);

  foreach_face(){
    uf.x[] = 0.;
  }
  boundary((scalar *){uf});
  restriction((scalar *){uf});  
}

event snapshot(t+=0.012){
  output_facets (cs, stdout);
}

event movies ( t+=0.0012,last;t<0.12)
{
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  view (tx = -0.489668, ty = -0.453663);
  squares("visu", min = -1 , max = 1.);
  draw_vof("cs");
  save("temperature.mp4");
}

#if 1
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[]      = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-14);
  fractions_cleanup(cs2,fs2,smin = 1.e-14);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  stats s2 = statsf(curve);
  fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});

  adapt_wavelet ({cs,curve,visu},
    (double[]){1.e-3,1.e-3,1.e-4},MAXLEVEL, MINLEVEL);
  foreach(reduction(+:n)){
    n++;
  }
  curvature(cs,curve);
  boundary({curve});
  fprintf(stderr, "##nb cells %d\n", n);
}
#endif

/**


*/