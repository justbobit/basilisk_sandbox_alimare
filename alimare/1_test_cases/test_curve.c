/**
#Test for advection based on local curvature


*/
#define LevelSet      1
#define Gibbs_Thomson 0
#define VOF 1

#include "embed.h"

#include "../advection_A.h"

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_reinit.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../LS_curvature.h"

#include "view.h"


/**
Setup of the numerical parameters
*/
int MAXLEVEL = 8; 
int MINLEVEL = 4;
double H0;

/**
Setup of the physical parameters + level_set variables
*/
vector v_pc[];

scalar * tracers  = NULL;
scalar * tracers2 = NULL;

scalar dist[];
scalar * level_set  = {dist};
scalar * LS_speed   = {v_pc.x,v_pc.y};


int nb_cell_NB;
double  NB_width ;    // length of the NB

scalar curve[];

#define Pi 3.14159265358979323846


/**
The initial geometry is a crystal seed:

$$
r\left(1+ - 0.25 *cos(4\theta) \right) - R
$$
*/

double geometry(double x, double y, coord center, double Radius, double theta0) 
{

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = ( sqrt(R2)*(1.-0.25*cos(4.*theta+theta0)) - Radius);

  return s;
}


/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {
  origin(-L0/2., -L0/2.);
  init_grid (1 << MAXLEVEL);
  run();
}


event init(t=0){
  DT         = 0.4*L0 / (1 << MAXLEVEL); // Delta
  nb_cell_NB = 1 << 3 ; // number of cell in the 
                        // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  vertex scalar dist_n[];

  coord center1 = {0.,L0/20.};
  coord center2 = {L0/16.,-L0/16.};
  coord center3 = {-L0/16.,-L0/16.};

/**
And because, why not, we put two crystal seeds, to see them compete during
crystal growth.
*/

  double size = L0/19.99;
  foreach_vertex() {
    dist_n[] = clamp(min(min(geometry(x,y,center1,size,0),
      geometry(x,y,center2,size,3.*180/Pi)),
      geometry(x,y,center3,size,-3.*180/Pi)),
    -1.2*NB_width,1.2*NB_width);
  }

  boundary ({dist_n});
  restriction({dist_n});

  fractions (dist_n, cs, fs);

  boundary({cs,fs});
  restriction({cs,fs});

  foreach() {
    dist[] = clamp(min(min(geometry(x,y,center1,size,0),
      geometry(x,y,center2,size,3.*180/Pi)),
      geometry(x,y,center3,size,-3.*180/Pi)),
    -1.2*NB_width,1.2*NB_width);
  }

  boundary ({dist});
  restriction({dist});

  foreach_face(){
    v_pc.x[] = 0.;
  }
  boundary((scalar *){v_pc});

  // curvature(cs,curve);
  // boundary({curve});
  curvature_LS(dist,curve);
}

/**
Stuff happening.

*/
event LS_advection(i++,last){
/**
First, we calculate the velocity on the face centroid
*/
  stats s3 = statsf(curve);
  fprintf(stderr, "%g %g %g\n",t, s3.min, s3.max );
  // double maxk = max(fabs(s3.min),fabs(s3.max));


  double epsK = 0.00, epsV = 0.00 ;
  foreach(){
    if(interfacial(point,cs)){

      coord n = normal (point, cs);
      v_pc.x[] = -n.x*(1.-epsK*curve[]-epsV*v_pc.x[]);
      v_pc.y[] = -n.y*(1.-epsK*curve[]-epsV*v_pc.y[]);
    }
    else{
      v_pc.x[] = 0.;
      v_pc.y[] = 0.;
    }
  }
  boundary((scalar *){v_pc});
  
  double deltat  = 0.45*L0 / (1 << MAXLEVEL);  // Delta
  int err = 0;
  int k_limit = 0;

  recons_speed(dist, deltat, nb_cell_NB, NB_width, LS_speed,
    k_limit, 2.e-7, &err, overshoot = 0.25);


  stats s4 = statsf(v_pc.x);
  fprintf(stderr, "#SPEED %g %g\n",s4.min, s4.max);

  double dt_LS = timestep_LS (v_pc, DT);

  face vector v_pc_f[];
  foreach_face(){
    v_pc_f.x[] = face_value (v_pc.x, 0);
  }
  boundary ((scalar *){v_pc_f});

  advection_LS (level_set, v_pc_f, dt_LS);
  
  boundary ({dist});
  restriction({dist});

  scalar dist_n[];
  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    double coeff[4];
    dist_n[] = mybilin( point , dist, Stencil, p_interp, coeff);
  }
  boundary({dist_n});

  fractions (dist_n, cs, fs);
  boundary({cs,fs});
  restriction({cs,fs});

  curvature(cs,curve);
  boundary({curve});
}

event LS_reinitialization(i++,last){
  LS_reinit(dist,0.5*L0/(1 << MAXLEVEL),8);
}


event movies ( i++,last; t<0.4)
{

  view (fov = 18.953, width = 800, height = 800);
  // cells();
  draw_vof("cs");
  squares("dist",min = -NB_width, max = NB_width);
  save ("curve.mp4");
  fprintf(stderr, "time %g\n", t);

  if(i%45==1) {
    output_facets (cs, stdout);
  }
}

/**
Mesh adaptation. still some problems with coarsening inside the embedded
boundary.
*/
event adapt (i++, last) {
  int n = 0;

  adapt_wavelet ({cs,curve},
    (double[]){1.e-3,1.e-3},MAXLEVEL, MINLEVEL);
  foreach(reduction(+:n)){
    n++;
  }

  scalar dist_n[];
  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    double coeff[4];
    dist_n[] = mybilin( point , dist, Stencil, p_interp, coeff);
  }
  boundary({dist_n});

  fractions (dist_n, cs, fs);
  // curvature(cs,curve);
  // boundary({curve});
  curvature_LS(dist,curve);
  fprintf(stderr, "##nb cells %d\n", n);
}
/**

![Animation of the approximated temperature field](test_curve/curve.mp4)(loop)


~~~gnuplot Evolution of the interface (zoom)
set key top right
set size square
plot 'out' w l lw 3 t 'Interface' 
~~~


~~~gnuplot Curvature
plot 'log' u 1:2 t 'min', '' u 1:3 t 'max'
~~~
*/

