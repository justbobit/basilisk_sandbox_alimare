/**
#Wall of water colliding a wedge

We simulate a mass water impinging on a wedge. Using different intial
conditions and contact angles, we want to study the presence or
not of an attached gas bubble after inpingement.

![ux velocity field](wedgeContactAngle/ux_theta_150.mp4)(loop)

![VOF function](wedgeContactAngle/f_theta_150.mp4)(loop)

*/


#define FILTERED 1 // SMEARING OF THE INTERFACE is required

/**
The wedge is represented using the embedded boundary formulation and we set the
contact angle using contact.h and the contact-embed.h library (still Work In
Progress by taveres, lopez and popinet, see their respective sandboxes).

Other .h files are pretty much standard.
*/

#include "embed.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "view.h"
#include "contact.h"
#include "../../../wiki/sandbox/popinet/contact/contact-embed.h"
#include "tag.h"

#include "view.h"

#include <stdio.h>
#include <time.h>



#define MAXLEVEL 10
#define MINLEVEL 6

/**
The speed of the water $V_{in}$ can be defined at compilation using:

CFLAGS+=-DVin={VALUE} make wedgeContactAngle.tst

default value is set at 5
*/
#ifndef Vin
double  Vin   = 5 ; 
#endif

/**
The ending time and the output frequency of the images/movies are set
accordingly.
*/
#define L   80.   // domain length
#define tEnd L/Vin*1.15
#define freqIm tEnd/375.
#define freqIm2 freqIm/2.

/**
Here we give the constants of our simulation.
*/

#define rho_l 	1.		
#define rho_g 	1.e-3	
#define SIGMA 	1.  	
#define h 	 	1.5 		// height of the wedge (mm)


#define trueSigma 75.e-3 // tension de surface de l'eau
#define trueHeight 0.5e-3

/**
Here we define the adimensionalized numbers of our setup.
*/

#define	Bo		1./30.	// Bond
#define	Oh_l	5.e-3	  // Ohnesorge 	= 1/sqrt(La_l)
#define	Oh_g	1.8e-5 / sqrt(rho_l / rho_g * trueSigma * trueHeight)
// #define Oh_g  1.8e-5 
#define We		2./3.e3	// Weber : Vin 	= sqrt(We)



double visu_speed_min ;
double visu_speed_max ;




/**
##Boundary conditions


The contact angle can be user defined at compilation using:

CFLAGS+=-DTheta0={VALUE} make wedgeContactAngle.tst

or, combined with the previous command :

CFLAGS+="DVin={VALUE} -DTheta0={VALUE2}" make wedgeContactAngle.tst

default value is set at 150°.
*/
#ifndef Theta0
#define Theta0 150
#endif

u.n[left]	= dirichlet(Vin);
u.n[bottom] = dirichlet(0);

u.t[bottom] = dirichlet(0);

f[left]  	= 1.;@

u.n[right] = neumann(0);
p[right]   	= dirichlet(0);

/**
##Geometry of the wedge


*/

// double WedgeAngle = 3*pi/8.;
double WedgeAngle = pi/4.;

double mygeom(double x, double y){
  return max(y-tan(WedgeAngle)*x+h+L0/2., y+L0/2-h);
}

/**
Before getting into contact with the wedge we set the contact angle using
contact.h.
*/

vector h2[];
h2.t[bottom] = contact_angle(Theta0*pi/180.);

double vol_cut;

int main() {
/**
Some initializations...
*/
  L0 = L;

	size (L);
	origin(-L/2,-L/2.);
	init_grid (pow(2,MAXLEVEL));

	rho1= rho_l	;		
	rho2= rho_g	;

	double 	mu_l  = Oh_l;
	double	mu_g  = Oh_g;

	mu1	= mu_l	;
	mu2	= mu_g	; 

	f.sigma	= SIGMA;

	f.height = h2 ; 
	CFL = 0.5;

  vol_cut = 1.*L0*L0/pow(4,MAXLEVEL); 

  visu_speed_min = Vin*0.8;
  visu_speed_max = Vin*1.2;

  fprintf(stderr, "## %d\n", Theta0);

/**
Then we set the contact angle on the wedge.
*/
  const scalar c[] = Theta0*pi/180.;
  contact_angle = c;


  fprintf(stderr, "## Calculation setup:\n");
  fprintf(stderr, "## domain length %f.2 tEnd %f.2 freqIm %f.2 freqIm2 \
   %f.2\n",L, tEnd, freqIm, freqIm2);
	run();
}

/**
## Initial conditions
*/
event init (t = 0) {
	CFL = 0.5;
	
/**
This is still very dirty, we put a mask to get a small channel. This is not
required to run the simulation but speeds it up a little bit.
*/  
  mask (y > -L0/4. ? top : none);

  vertex scalar distn[];
  foreach_vertex() {
    distn[] = mygeom(x,y);
  }
  boundary({distn});
  fractions(distn,cs,fs);

/*
Initial liquid-gas interface is a plane which is VERY dirty. We should
initialize correctly the interface near the wall to remove surface tension
relaxation effects.
*/
	scalar phi[];
	foreach_vertex(){
		// phi[]=(-L0/2. + L/40 -x);
    phi[]=0.;
	}
	fractions(phi,f);
}

/*
## Calculation loop
*/
event acceleration (i++){
	face vector av = a;
	foreach_face(x)
    	av.x[] += Bo;
}

/**
##Mesh adaptation
*/

event adapt (i++) {
	adapt_wavelet ({f,u,cs}, (double[]){1.e-2,8.e-3,8.e-3,1.e-2}, MAXLEVEL, MINLEVEL);
	
/*
We reset the wedge after each adaptation. It shouldn't be mandatory but we
observed better results with this setup.
*/
	vertex scalar distn[];
	foreach_vertex() {
    distn[] = mygeom(x,y);
  }
  boundary({distn});
  fractions(distn,cs,fs);
}

/*
##Small drops removal

Now and then, small drop are created and have a high velocity. Since they do not
have a big influence on the simulation, we can safely remove them during our
calculation
*/

event remove_drops ( i+=10 )
{
  scalar m[];
  foreach()
    m[] = f[] > 0.;
  int n = tag (m);
  double v[n];
  int remove_flag[n];
  for (int j = 0; j < n; j++) {
    v[j] = 0.;
    remove_flag[j] = 0;
  }
  foreach(serial)
    if (m[] > 0) {
      int j = m[] - 1;
      v[j] += dv()*f[];
    }

#if _MPI
  MPI_Allreduce (MPI_IN_PLACE, v, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

  for (int j = 0; j < n; j++)
    if ( v[j] < vol_cut )
       remove_flag[j] = 1;

  int count  =0 ;
  foreach()
    if (m[] > 0) {
      int j = m[] - 1;
      if ( remove_flag[j] == 1 ){
          f[]=0.;
          count++;
        }
    }
  if(count!=0)fprintf(stderr, "## %d cells where water was removed\n", count);
}

/*
##Outputs

Here are the output events
*/

event movies ( t+=freqIm,last;t<tEnd){
	view(fov = 6, width = 1600, height = 500, ty = 0.35, tx = 0);
	draw_vof("f");
	draw_vof ("cs", "fs", filled = -1);
  squares("u.x", linear = true, min = visu_speed_min, max = visu_speed_max);
  box();
  char name[80];
  sprintf (name, "ux_theta_%d_Vin_%d.mp4", Theta0, Vin);
  save(name);


  draw_vof("f");
  draw_vof ("cs", "fs", filled = -1);
  squares("f", min = 0.01, max = 0.99, linear = true);
  box();
  sprintf (name, "f_theta_%d_Vin_%d.mp4", Theta0,Vin);
  save(name);

  draw_vof("f");
  draw_vof ("cs", "fs", filled = -1);
  scalar omega[];
  vorticity (u, omega);  box();
  squares("omega", linear = true);
  sprintf (name, "vorticity_theta_%d_Vin_%d.mp4", Theta0,Vin);
  save(name);
}

// event movies2(t+=freqIm2,last){
//   if(t>0.3*tEnd){
//     char name[80];
//     // view(fov = 4, width = 1600, height = 500, ty = 0.4, tx = -0.1);
//     draw_vof("f");
//     draw_vof ("cs", "fs", filled = -1);
//     squares("u.x", min = visu_speed_min, max = visu_speed_max, linear = true);
//     box();
//     sprintf (name, "ux_zoom_theta_%d.mp4", Theta0);
//     save(name);

//     draw_vof("f");
//     draw_vof ("cs", "fs", filled = -1);
//     squares("f", min = 0.01, max = 0.99, linear = true);
//     box();
//     sprintf (name, "f_zoom_theta_%d.mp4", Theta0);
//     save(name);

//     // draw_vof("f");
//     // draw_vof ("cs", "fs", filled = -1);
//     // squares("p", linear = true);
//     // box();
//     // sprintf (name, "p_zoom_theta_%d.mp4", Theta0);
//     // save(name);
//   }
// }

// event images(t+=freqIm*10,last){
//   if(t>0.3*tEnd){
//     char name[80];
//     view(fov = 4, width = 1600, height = 500, ty = 0.4, tx = -0.1);
//     draw_vof("f");
//     draw_vof ("cs", "fs", filled = -1);
//     squares("u.x", min = visu_speed_min, max = visu_speed_max, linear = true);
//     sprintf (name, "ux_theta_%d_t_%.2f.png", Theta0, t);
//     save(name);
//   }
// }

event mylog(t+=freqIm){
	stats s = statsf (u.x);
  	stats s2 = statsf (u.y);
  	fprintf (stderr, "%g %g %ld %g %g %g %g\n", \
  	t, dt , grid->tn, s.min, s.max, s2.min, s2.max);
}
