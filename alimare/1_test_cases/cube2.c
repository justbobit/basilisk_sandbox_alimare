/**
#Solidification of an undercooled liquid

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = 1$. The ice particle is initially
at $T_S = -1$.

The temperature on the interface is derived from the Gibbs-Thomson relation:
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$
*/
#define DOUBLE_EMBED  1
#define LevelSet      1
#define Gibbs_Thomson 1

#include "embed.h"
#include "../double_embed-tree.h"

#include "../advection_A.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
// #include "../LS_reinit.h"
#include "../../popinet/redistance.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../phase_change_velocity.h"
#include "../LS_curvature.h"
#include "view.h"

#define T_eq         0.
#define TL_inf       -1.
#define TS_inf       -1.

/**
Setup of the numerical parameters
*/
int MAXLEVEL = 8; 
int MINLEVEL = 5;
double H0;

/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[], dist[];
vector v_pc[];

scalar * tracers    = {TL};
scalar * tracers2 = {TS};

scalar * level_set  = {dist};
scalar * LS_speed   = {v_pc.x,v_pc.y};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];


double  latent_heat = 1.;
double  lambda[2]; // thermal capacity of each material
#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
double  epsK = 0.0001, epsV = 0.000;
#else
double  epsK = 0.000, epsV = 0.000;
#endif

#define GT_aniso 1
/**
We take into account an [anisotropic](../phase_change_velocity.h) effect.

*/
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif

int nb_cell_NB;
double  NB_width ;    // length of the NB

scalar curve[];



#define Pi 3.14159265358979323846


/**
The initial geometry is a crystal seed:

$$
r\left(1+ - 0.25 *cos(4\theta) \right) - R
$$
*/

double geometry(double x, double y, coord center, double Radius, double theta0) 
{
  int aniso_init;
  if(aniso !=1){
    aniso_init = aniso;
  }
  else{
    aniso_init = 4;
  }
  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = ( sqrt(R2)*(1.-0.25*cos(aniso_init*theta+theta0)) - Radius);

  return s;
}


/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {
  // TOLERANCE = 1.e-8;
  TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));
  TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));


  TL[top]    = neumann(fabs(dist[]) > 2*Delta ? TL_inf : 0.1); 
  TL[left]   = neumann(fabs(dist[]) > 2*Delta ? TL_inf : 0.1); 
  TL[bottom] = neumann(fabs(dist[]) > 2*Delta ? TL_inf : 0.1); 
  TL[right]  = neumann(fabs(dist[]) > 2*Delta ? TL_inf : 0.1); 

  TS[top]    = neumann(fabs(dist[]) > 2*Delta ?  TS_inf : 0.1); 
  TS[bottom] = neumann(fabs(dist[]) > 2*Delta ?  TS_inf : 0.1); 
  TS[left]   = neumann(fabs(dist[]) > 2*Delta ?  TS_inf : 0.1); 
  TS[right]  = neumann(fabs(dist[]) > 2*Delta ?  TS_inf : 0.1); 


  origin(-L0/2., -L0/2.);
  init_grid (1 << MAXLEVEL);
  run();
}


event init(t=0){
  
  DT         = 0.5*L0 / (1 << MAXLEVEL); // Delta
  nb_cell_NB = 1 << 3 ; // number of cell in the 
                        // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;

/**
And because, why not, we put two crystal seeds, to see them compete during
crystal growth.
*/
  coord center1 = {0.,L0/20.};
  coord center2 = {L0/16.,-L0/16.};
  coord center3 = {-L0/16.,-L0/16.};

  double size = L0/29.99;
  foreach() {
    dist[] = clamp(min(min(geometry(x,y,center1,size,0),
      geometry(x,y,center2,size,0.)),
      geometry(x,y,center3,size,0.)),
    -1.2*NB_width,1.2*NB_width);
  }

  boundary ({dist});
  restriction({dist});

  redistance(dist);

  vertex scalar dist_n[];
  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    double coeff[4];
    dist_n[] = mybilin(point , dist, Stencil, p_interp, coeff);
  }

  boundary ({dist_n});
  restriction({dist_n});

  fractions (dist_n, cs, fs);

  boundary({cs,fs});
  restriction({cs,fs});

  foreach_face(){
    v_pc.x[] = 0.;
  }
  boundary((scalar *){v_pc});

  curvature(cs,curve);
  boundary({curve});


  // curvature_LS(dist, curve);
  foreach() {
    TL[] = TL_inf;
    TS[] = 0.;
  }
}

event properties(i++){
  foreach_face()
  muv.x[] = lambda[i%2]*fs.x[];
  boundary((scalar *) {muv});
}

event tracer_diffusion(i++){
  int kk;
  mgstats mg1;
  for (kk=1;kk<=6;kk++){
    if(i%2==0){
      boundary({TL});
      mg1 = diffusion(TL, dt, D = muv , theta = cs);
    }
    else{
      boundary({TS});
      mg1 = diffusion(TS, dt, D = muv, theta = cs);
    }
    if(mg1.resa > TOLERANCE) {
/**
If the calculation crashes (it often does if the Poisson solver does not
converge) we save the last state of the variables
*/
      scalar ffsx[], ffsy[];
      foreach(){
        ffsx[] = fs.x[];
        ffsy[] = fs.y[];
      }
      boundary({ffsx,ffsy});
      dump();
      exit(1);
    }
  }
}

/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  if(i%2 ==1 && i > 20){

    double L_H       = latent_heat;  

    scalar cs0[];

    foreach(){
      cs0[]   = cs[];
      cs[]    = 1.-cs[];
    }
    foreach_face(){
      fs.x[]  = 1.-fs.x[];
    }

    boundary({cs,fs,cs0});
    restriction({cs,fs});
/**
First, we calculate the velocity on the face centroid
*/
    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, L_H, 
      lambda,epsK, epsV, aniso);
    double deltat  = 0.45*L0 / (1 << MAXLEVEL);  // Delta
    int err = 0;
    int k_limit = 0;

    vector v_pc_r[];
    foreach(){
      foreach_dimension(){
        if(interfacial(point, cs))v_pc_r.x[] = v_pc.x[];
        else v_pc_r.x[] = 0.;
      }
    }
    boundary((scalar * ){v_pc_r});
    restriction((scalar * ){v_pc_r});

    scalar * speed_recons  = {v_pc_r.x,v_pc_r.y};
    recons_speed(dist, deltat, nb_cell_NB, NB_width, speed_recons,
      k_limit, 2.e-7, &err, overshoot = 0.3);

    double dt_LS = timestep_LS (v_pc_r, DT);

    face vector v_pc_f[];
    foreach_face(){
      v_pc_f.x[] = (v_pc_r.x[-1,0]+v_pc_r.x[])/2.;
    }
    boundary ((scalar *){v_pc_f});

    advection_LS (level_set, v_pc_f, dt_LS);
    
    boundary ({dist});
    restriction({dist});

    redistance(dist);

    scalar dist_n[];
    int Stencil[2] = {-1,-1};
    coord p_interp = {-0.5, -0.5};
    foreach_vertex(){
      double coeff[4];
      dist_n[] = mybilin(point , dist, Stencil, p_interp, coeff);
    }
    boundary({dist_n});

    fractions (dist_n, cs, fs);
    
    boundary({cs,fs});
    restriction({cs,fs});

    curvature(cs,curve);
    boundary({curve});
  
    foreach(){
      cs[]      = 1.-cs[];
    }
    foreach_face(){
      fs.x[]      = 1.-fs.x[];
    }

    boundary({cs,fs});
    restriction({cs,fs});

/**
Sometimes, when a new cell becomes an interfacial cell, the Poisson solver has
trouble converging, therefore we iterate a bit more when it occurs.
*/
    k_loop = 0;
    foreach(){
      if( (cs0[] != 1. && cs[] ==1.) || (cs0[] == 0. && cs[] !=0.))k_loop = 1;
    }
  }
}

#if DOUBLE_EMBED
event double_calculation(i++,last){
/**
We recall that a global iteration, is made when `i+=2`, every time `i` is
incremented, the solver (diffusion, N-S.) is applied to only one phase (liquid
or solid). Therefore we have to modify `cs` and `fs` at each iteration.
*/
  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face(){
    fs.x[]      = 1.-fs.x[];
  }

  boundary({cs,fs});
  restriction({cs,fs});
}
#endif



event movies ( i++,last; t<1.)
{
  if(i%2 == 1) {
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    stats s3 = statsf(visu);
    fprintf(stderr, "#temperature %g %g\n",  s3.min, s3.max);  
    view (width = 800, height = 800);
// 

    char filename [100];
    sprintf(filename,"temperature_epsK%g_epsV%g_.mp4",epsK,epsV);

    draw_vof("cs");
    squares("visu", min = -0.01 , max = 0.01);
    save (filename);

    draw_vof("cs");
    squares("dist", min = -0.05 , max = 0.05);
    save ("dist.mp4");


  }
  if(i%40==1) {
    output_facets (cs, stdout);
  }

}

/**
Mesh adaptation. still some problems with coarsening inside the embedded
boundary.
*/
#if 1
event adapt (i++, last) {
  if(i%2 == 1 ){

    foreach_cell(){
      cs2[] = 1.-cs[];
    }
    foreach_face(){
        fs2.x[]      = 1.-fs.x[];
    }

    boundary({cs,cs2,fs,fs2});
    fractions_cleanup(cs,fs,smin = 1.e-14);
    fractions_cleanup(cs2,fs2,smin = 1.e-14);
    restriction({cs,cs2,fs,fs2});
    int n=0;
    stats s2 = statsf(curve);
    fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});

    adapt_wavelet ({cs,curve,visu},
      (double[]){1.e-3,1.e-3,1.e-2},MAXLEVEL, MINLEVEL);
    foreach(reduction(+:n)){
      n++;
    }
    curvature(cs,curve);
    boundary({curve});

    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, latent_heat, 
      lambda,epsK, epsV, aniso);
    fprintf(stderr, "##nb cells %d\n", n);
  }
}
#endif
/**

![Animation of the approximated temperature field](cube/visu.mp4)(loop)


~~~gnuplot Evolution of the interface (zoom)
set key top right
set size ratio -1
plot 'out' w l lw 3 t 'Interface' 
~~~
*/

