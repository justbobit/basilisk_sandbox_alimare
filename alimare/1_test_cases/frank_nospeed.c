/**
#Frank's Spheres

This theory of this test case has been studied originally by 
[Frank](#frank1950radially). We start with a solid circle (or sphere in 3D) at melting
temperature surrounded by an undercooled liquid. This case is stable. 

##Analytical Solutions

A class of self similar solutions has been developed by Frank, in one, two and
three dimensions. For $d =1, 2, 3$ the solid interior of a slab, cylinder or
sphere of radius,
$$
R(t) = S t^{1/2}
$$
parameterized by S and the temperature field is 0 if $s <$ and:
$$
T(x,t) = T(s) = T_\infty * (1- \frac{F_d(s)}{F_d(S)})
$$
if s> S, and:
$$
F_2(s) = E_1(\frac{s^2}{4})
$$
We use the GNU Scientific Library for comparison with theoretical values. $E_1$
is readily available in the GSL, using `gsl_sf_expint_E1` function which is :
$$
E_1(x) = \int_1^\infty \frac{e^{-xt}}{t}dt = \int_x^\infty \frac{e^{-t}}{t}dt
$$
*/

/**
Plots:
~~~gnuplot Test
plot 'log0' u 1:2 w l lc 'green'  t '64x64',\
      '' u 1:5 w l dt 2 lw 1.5 lc rgb 'black' t 'theo'
     'log1' u 1:2 w l lc 'red'    t '128x128',\
~~~

~~~gnuplot Average error convergence CFL = 0.5
ftitle(a,b) = sprintf("%.3fx^\{%4.2f\}", exp(a), -b)

f(x) = a + b*x
fit f(x) 'log' u (log($1)):(log($2)) via a,b

f2(x) = a2 + b2*x
fit f2(x) 'log' u (log($1)):(log($4)) via a2,b2

f3(x) = a3 + b3*x
fit f3(x) 'log' u (log($1)):(log($6)) via a3,b3

f4(x) = a4 + b4*x
fit f4(x) 'log' u (log($1)):(log($9)) via a4,b4

set ylabel 'Average error'
set xrange [32:512]
set yrange [*:*]
set xtics 32,2,512
set format y "%.1e"
set logscale
plot '' u 1:2 pt 7 lc 'blue' t 'all cells', exp(f(log(x))) t ftitle(a,b) lc 'blue', \
     '' u 1:4 pt 7 lc 'green' t 'partial cells', exp(f2(log(x))) t ftitle(a2,b2) lc 'green', \
     '' u 1:6 pt 7 lc 'red' t 'full cells', exp(f3(log(x))) t ftitle(a3,b3) lc 'red',\
     '' u 1:9 pt 7 lc 'black' t 'final position',\
      exp(f4(log(x))) t ftitle(a4,b4) lc 'black'
~~~



~~~gnuplot Evolution of the interface
set term svg size 1000,1000
unset logscale
unset format y
set xrange [0:2.5]
set yrange [0:2.5]
set xtics 0,0.5,2.5
unset ylabel
plot 'out0' w l lc 'black' lw 1.5 t '64x64',\
    'out1' w l lc 'red'   lw 1.5 t '128x128'
~~~

*/


#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define QUADRATIC 1
#define GHIGO 1

#if GHIGO
#include "../../ghigo/src/myembed.h"
#else
#include "embed.h"
#endif
#include "../double_embed-tree.h"

#include "../advection_A.h"
#if GHIGO
#include "../../ghigo/src/mydiffusion.h"
#else
#include "diffusion.h"
#endif

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_advection.h"
#include "view.h"

#define T_eq          0.
#define TL_inf       -1./2
#define TS_inf        0.

#define tstart 0.

int MINLEVEL, MAXLEVEL; 
double latent_heat;

#define DT_MAX  1.

/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[], dist[];

scalar * tracers   = {TL};
scalar * tracers2  = {TS};
scalar * level_set = {dist};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];
double DT2;

double  epsK = 0.000, epsV = 0.000;
scalar curve[];


double lambda[2];

int aniso = 1;


int     nb_cell_NB =  1 << 3 ;  // number of cells for the NB
double  NB_width ;              // length of the NB

double s_clean = 1.e-10; // used for fraction cleaning

#include "../basic_geom.h"
  
mgstats mg1,mg2;




int j;
int k_loop = 0;

double undercooling = -1/2., S = 1.56;


/**
Theoretical functions and parameters for this test case.
Only working in 2D for now.
*/

#include <gsl/gsl_sf_expint.h>
#pragma autolink -lgsl -lgslcblas

#if dimension == 2
double F2(double s) {return gsl_sf_expint_E1(powf(s,2.)/4.);}
#endif

double Theo(double r, double t, double undercooling, double F2_S){
  if(dimension == 2){;
    double s = r/powf(t,0.5);
    return undercooling * (1. - F2(s) / F2_S);
  }
  exit(1); // dimension == 3 not coded
}


TL[embed] = dirichlet(T_eq);
TS[embed] = dirichlet(T_eq);

#define radius(x, y) (sqrt(sq(x)+sq(y)))
TL[top]    = dirichlet(Theo(radius(x,y) , 1., undercooling, F2(S)));
TL[bottom] = dirichlet(Theo(radius(x,y) , 1., undercooling, F2(S)));
TL[left]   = dirichlet(Theo(radius(x,y) , 1., undercooling, F2(S)));
TL[right]  = dirichlet(Theo(radius(x,y) , 1., undercooling, F2(S)));

TS[top]    = dirichlet(TS_inf); 
TS[bottom] = dirichlet(TS_inf); 
TS[left]   = dirichlet(TS_inf); 
TS[right]  = dirichlet(TS_inf); 
/**
*/



/**
These are the parameters from the Almgren paper.
*/
double t_fin = 0.6; // duration of the calculation, final time is therefore 2.
double t0 = 1.;
char filename [100];
FILE * fp1,  * fp2;

/**
*/

void myprop(face vector muv, face vector fs, 
  double lambda){
  foreach_face()
    muv.x[] = lambda*fs.x[];
  boundary((scalar *) {muv});
}

void writefile(mgstats mgd){
    if((mgd.resa > TOLERANCE) ) {
/**
If the calculation crashes (it often does if the Poisson solver does not
converge) we save the last state of the variables
*/
    scalar ffsx[], ffsy[];
    scalar point_f[];
    vector x_interp[];
    foreach(){
      ffsx[] = fs.x[];
      ffsy[] = fs.y[];
      if(interfacial(point,cs)){
        coord n       = facet_normal( point, cs ,fs) , p;
        normalize(&n);
        double alpha  = plane_alpha (cs[], n);
        line_length_center (n, alpha, &p);
        x_interp.x[] = p.x;
        x_interp.y[] = p.y;
        coord segment[2];
        point_f[] = facets (n, alpha, segment);
      }
      else{
        x_interp.x[] = nodata;
        x_interp.y[] = nodata;
        point_f[]    = 0;
      }
    }
    boundary({ffsx,ffsy});
    vertex scalar distn[];
    cell2node(dist,distn);
    dump();
    fprintf(stderr, "#CRASH#");
    exit(1);
  }
}


int main() {

  L0 = 16.;
  CFL = 0.5;
  origin (-0.5*L0, -0.5*L0);
  
  j = 1;
  for (j=0;j<=1;j++){
    snprintf(filename, 100,  "log%d", j);
    fp1 = fopen (filename,"w");
    snprintf(filename, 100,  "out%d", j);
    fp2 = fopen (filename,"w");
/**
Here we set up the parameters of our simulation. The latent heat $L_H$, the
initial position of the interface $h_0$ and the resolution of the grid.
*/
    latent_heat  = 1;
    MAXLEVEL = 6+j, MINLEVEL = 5 ;
    TL.third = true;
    TS.third = true; 
    N = 1 << MAXLEVEL;
    init_grid (N);
    DT2 = 0.01*sq(L0/(1<< MAXLEVEL)); // values of Chen97.
    fprintf(stderr, "### DT2 %g \n", DT2);
    run(); 
    fclose(fp1);
    fclose(fp2);
  }
  exit(1);
}

/**
*/


event init(t=0){
  dt = DT2;
  NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;
  double R_init = 1.56;
  coord center = {0.00,0.00};
  foreach(){
      dist[] = circle(x,y,center,R_init);
  }
  boundary ({dist});
  restriction({dist});


  vertex scalar dist_n[];
  // cell2node(dist,dist_n);
  foreach_vertex(){
      dist_n[] = circle(x,y,center,R_init);
  }

  fractions (dist_n, cs, fs);
  boundary({cs,fs});
  restriction({cs,fs});

#if LS_curve
  curvature_LS(dist,curve);
#else
  curvature(cs,curve);
  boundary({curve});
#endif


  double F2_S  = F2(S);
  scalar error[],theo[];
  foreach(){
    double ray =  radius(x,y);
    if(cs[]>0.){
      TL[] = Theo(ray , t0, undercooling, F2_S);
    }
    else{
      TL[]=  0.;
    }
    TS[] = 0.;
  }

  scalar er[];
  double radiustheo = S*pow(t0,1./2);
  foreach() {
    coord b, n;
    embed_geometry (point, &b, &n);

    er[] = nodata;
    if(cs[]>0. && cs[] <1.)
      er[] = fabs(radius(x+b.x*Delta,y+b.y*Delta)-radiustheo);

  }
  norm nr = normf(er);
  fprintf(stderr, "#INITIAL %g %g\n", nr.avg, nr.max);

  boundary({TL,TS});
  restriction({TL,TS});
  int n =0;
  foreach(reduction(+:n)){
      n++;
    }
  fprintf(stderr, "#CELLS INIT %d\n", n);
  myprop(muv,fs,lambda[0]);

#if GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif
}


event velocity(i++){
  tnext = t+DT2;
  dt = DT2;
}

/**
*/


event tracer_diffusion(i++){
  dt = DT2;
  boundary({TL});
  myprop(muv,fs,lambda[0]);
  mgT = diffusion (TL,dt,muv,theta = cm);
  writefile(mgT);
  stats s = statsf(TL);
  fprintf(stderr, "# TL stats %g %g\n", s.min, s.max);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  boundary({TS});
  mgT = diffusion (TS,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
}


/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  vector vpc[],vpcf[];
  scalar curve[];
/**
Artificial speed (imposed)
*/
  double speed = S/(2*sqrt(t0+t));

  vector ndist[];
  buildNdist(dist, ndist); // build the normal of the level-set function

  vector v[];
// theoretical value of the normal
  foreach(){
    coord d1;
    d1.x = 2*x/sqrt(sq(x) + sq(y));
    d1.y = 2*y/sqrt(sq(x) + sq(y));
    normalize(&d1);
    v.x[] = d1.x;
    v.y[] = d1.y;
  }
  fprintf(stderr, "%g\n", speed);
  exit(1);

  boundary((scalar *){v});
  restriction((scalar *){v});

  foreach(){
    if(fabs(dist[])< NB_width ){
      vpc.x[] = speed*v.x[];
      vpc.y[] = speed*v.y[];
    }
    else{
      vpc.x[] = 0.;
      vpc.y[] = 0.;

    }
  }
/**
Here we output the error given by the theory of [Frank](#frank1950radially).
*/
  boundary((scalar *){vpc});
  restriction((scalar *){vpc});
/**
We reconstruct a cell-centered vpc field in the vicinity of the interface where
vpcr is the solution to the bilinear interpolation of vpc on face centroids.
*/
  int err = 0;

  RK3(dist,vpc,dt, NB_width);  
  boundary ({dist});
  restriction({dist});
  scalar distn[];
  cell2node(dist,distn);
  fractions (distn, cs, fs);
  fractions_cleanup(cs,fs,smin = 1.e-10, opposite = true);
  LS_reinit(dist, it_max = 10);

/**

*/
  foreach_face(){
    uf.x[] = 0.;
  }
  boundary((scalar *){uf});
  restriction((scalar *){uf});
}

/**
*/
event interfaces(t+=0.1,last){
  output_facets(cs,fp2);
}

#define Omovie 1
event movies (i++,last;t<t_fin)
{
// the radius we output is just the maximum of the radii

  double y_max=0,y_max2 = 0.;
  vector h[];
  heights (cs, h);
  boundary((scalar *){h});
  foreach(reduction(max:y_max)){
    if(interfacial(point, cs)){
      coord b, n;
      embed_geometry (point, &b, &n);
      y_max = max (y_max, radius(x+b.x*Delta,y+b.y*Delta));
      if(x == Delta/2){
        y_max2 = y - dist[];
      }

    }     
  }
  double ref = sqrt(sq(S)*(t+t0+dt)-sq(L0/(2.*(1 << MAXLEVEL))));
  fprintf(fp1, "%g %g %g %g %g\n", t, y_max, S*pow(t+t0+dt,1./2),y_max2,ref);
  view(fov = 8.7, width = 1000, height = 1000);
  draw_vof("cs");
  squares("TL");
  save("TL.mp4");
}

event final_error(t=end, last){
    scalar e[], ep[], ef[],er[];

/**
Here we output the error given by the theory of [Frank](#frank1950radially).
*/
  double F2_S  = F2(S);
  double radiustheo = S*pow(t+t0+dt,1./2);
  foreach() {
    coord b, n;
    embed_geometry (point, &b, &n);

    er[] = nodata;
    if(cs[]>0. && cs[] <1.)
      er[] = fabs(radius(x+b.x*Delta,y+b.y*Delta)-radiustheo);

    double ray = radius(x,y);
    if (cs[] == 0.)
      e[] = nodata;
    else {
      e[] = TL[] - Theo(ray , t +t0+dt, undercooling, F2_S);
      ep[] = cs[] < 1. ? e[] : nodata;
      ef[] = cs[] >= 1. ? e[] : nodata;
    }
  }
  norm n = normf (e), np = normf (ep), nf = normf (ef);
  norm nr = normf(er);

  fprintf (stderr, "%d %.3g %.3g %.3g %.3g %.3g %.3g %g %g\n",
   N, n.avg, n.max, np.avg, np.max, nf.avg, nf.max, nr.avg, nr.max);
  dump();

  fflush (ferr);
  if(MAXLEVEL == 7 && Omovie){
    scalar loge[];
    foreach(){
      if (cs[] == 0.)
        loge[] = nodata;
      else
        loge[] = log(fabs(e[]))/log(10.);
    }
    view(fov = 8.7, width = 1000, height = 1000);
    cells();
    squares("loge", min = -4, max = -2.5);
    draw_vof("cs");
    save("error.png");
    // dump();
  }
  fprintf(stderr, "### ite finale %d\n", i);
}

#if 0
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[]      = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-14);
  fractions_cleanup(cs2,fs2,smin = 1.e-14);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});

  adapt_wavelet ({cs,curve,visu},
    (double[]){1.e-3,1.e-3,1.e-3},MAXLEVEL, MINLEVEL);
  foreach(reduction(+:n)){
    n++;
  }
#if LS_curve
  curvature_LS(dist,curve);
#else
  curvature(cs,curve);
  boundary({curve});
#endif
}
#endif

/**


~~~bib

@article{frank1950radially,
  title={Radially symmetric phase growth controlled by diffusion},
  author={Frank, Frederick Charles},
  journal={Proceedings of the Royal Society of London. Series A. Mathematical and Physical Sciences},
  volume={201},
  number={1067},
  pages={586--599},
  year={1950},
  publisher={The Royal Society London}
}

@Article{Almgren1993,
  author        = {R. Almgren},
  title         = {Variational Algorithms and Pattern Formation in Dendritic Solidification},
  year          = {1993},
  volume        = {106},
  pages         = {337-354},
  issn          = {0021-9991},
  doi           = {10.1006/jcph.1993.1112},
}

@article{chen_simple_1997,
  title = {A simple level set method for solving {Stefan} problems},
  volume = {135},
  number = {1},
  journal = {Journal of Computational Physics},
  author = {Chen, S and Merriman, B and Osher, S and Smereka, P},
  year = {1997},
  pages = {8--29}
}

~~~
*/
