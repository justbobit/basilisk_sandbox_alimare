/**
#Solidification minimum working example

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_2 \nabla T_S)
$$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

This test case was described in [Udaykumar et al.](#Udaykumar1999), initial
temperature is :

$$
\phi(x,y,t_0) = 1- \dfrac{erf(\dfrac{1-y}{2\sqrt{t_0}})}{erf(\lambda)}
$$
with $\lambda = 0.9$, $t_0 = 0.25$.

The initial position of the interface is  $1- 2\lambda \sqrt{t_0}$


The interface position should be :
$$
S(t) = 1- 2\lambda \sqrt{t_0}
$$

What is interesting about this case is that we test the ability of
embed_extrapolate to initialize the temperature in newly liquid cells.

~~~gnuplot Position of the interface
set term svg size 1000,1000
set key right
plot 'log0' u 1:2 w p pt 7 ps 0.5 lc 'blue'  t '32x32',\
     'log1' u 1:2 w p pt 7 ps 0.5 lc 'green' t '64x64',\
     'log2' u 1:2 w p pt 7 ps 0.5 lc 'red'   t '128x128',\
     'log3' u 1:2 w p pt 7 ps 0.5 lc 'red'   t '256x256',\
     '' u 1:3 w l dt 2 lw 1.5 t 'reference'
~~~

~~~gnuplot Average error convergence CFL = 0.5
set term svg size 600,600
unset xrange
unset yrange

ftitle(a,b) = sprintf("%.3f/x^{%4.2f}", exp(a), -b)

f(x) = a + b*x
fit f(x) 'log' u (log($1)):(log($2)) via a,b

f2(x) = a2 + b2*x
fit f2(x) 'log' u (log($1)):(log($4)) via a2,b2

f3(x) = a3 + b3*x
fit f3(x) 'log' u (log($1)):(log($6)) via a3,b3

set ylabel 'Average error'
set xlabel 'number of cells per direction, grid NxN'
set xrange [16:512]
set yrange [*:*]
set xtics 16,2,512
set format y "%.1e"
set logscale
  plot '' u 1:2 pt 7 lc 'blue' t 'all cells', exp(f(log(x))) t ftitle(a,b) lc 'blue', \
       '' u 1:4 pt 7 lc 'green' t 'partial cells', exp(f2(log(x))) t ftitle(a2,b2) lc 'green', \
       '' u 1:6 pt 7 lc 'red' t 'full cells', exp(f3(log(x))) t ftitle(a3,b3) lc 'red'
~~~

As convergence analysis shows, we get a second order accuracy.


~~~gnuplot Initial and final temperature plots
set term svg size 1000,1000
set key left
unset logscale
unset xrange
unset xtics
set grid
set ylabel 'Temperature profile' 
set yrange[-0.1:1]
set xrange [0.169:1]
set xtics 0.15,0.1,1
plot 'out0' u 1:2 w p pt 7 lc 'blue' t 'Final temperature 32x32  ',\
     'out1' u 1:2 w p pt 7 lc 'green' t '64x64  ',\
     'out2' u 1:2 w p pt 7 lc 'red' t '128x128', \
     'out3' u 1:2 w p pt 7 lc 'red' t '256x256', \
     '' u 1:3 w l lt -1 t 'Reference final',\
     'init0' u 1:2 w p pt 7 lc 'blue' t 'Initial temperature 32x32 ',\
     'init1' u 1:2 w p pt 7 lc 'green' t '64x64  ',\
     'init2' u 1:2 w p pt 7 lc 'red' t '128x128  ',\
     'init3' u 1:2 w p pt 7 lc 'red' t '256x256  ',\
     '' u 1:3 w l lt -1 t 'Reference initial'
~~~

~~~gnuplot Final error plots
set term svg size 600,600
set key left
set logscale y
set ylabel 'Error profile'
unset yrange
plot 'out0' u 1:4 w p pt 7 lc 'blue' t '32x32',\
     'out1' u 1:4 w p pt 7 lc 'green' t '64x64',\
     'out2' u 1:4 w p pt 7 lc 'red' t '128x128',\
     'out3' u 1:4 w p pt 7 lc 'red' t '256x256'
~~~
*/
#define Gibbs_Thomson 0
#define QUADRATIC 1
#define GHIGO 1

#if GHIGO
#include "../../ghigo/src/myembed.h"
#else
#include "embed.h"
#endif
#include "../double_embed-tree.h"

#include "../advection_A.h"
#if GHIGO
#include "../../ghigo/src/mydiffusion.h"
#else
#include "diffusion.h"
#endif

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_advection.h"
#include "view.h"

#define T_eq          0.
#define TL_inf        1.
#define TS_inf        0.

#define tstart 0.

int MINLEVEL, MAXLEVEL; 
double latent_heat;
char filename [100];
FILE * fp1, * fp2, * fp3;

#define DT_MAX  1.

#define T_eq         0.


#define plane( y,H0) (y -H0)

#define Stefan 2.85761869 // lbd * exp(lbd**2) * erf(lbd) * sqrt(Pi)
#define rootlambda 0.9
#define t0 0.11

double T_init(double y,double t,double y0){
  return 1.-erf((1.-y)/(2.*sqrt(t)))/erf(rootlambda);
}

scalar TL[], TS[], dist[];
vector vpc[];
vector vpcf[];


scalar * tracers   = {TL};
scalar * tracers2  = {TS};
scalar * level_set = {dist};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];
double DT2;
double S0; // initial position of the interface

scalar curve[];

double lambda[2];

int     nb_cell_NB =  1 << 2 ;  // number of cells for the NB
double  NB_width ;              // length of the NB

double  epsK = 0., epsV = 0.;

double eps4 = 0.;

double s_clean = 1.e-10; // used for fraction cleaning

  
mgstats mgT;

TL[embed] = dirichlet(T_eq);
TL[top]   = dirichlet(TL_inf); 

TS[embed]  = dirichlet(T_eq);
TS[bottom] = dirichlet(TS_inf); 

// u.n[embed] = dirichlet(vpcfn(point,vpc));
// u.t[embed] = dirichlet(0.);

int j;
int k_loop = 0; // used when a becomes an interfacial cell, we iterate a bit
// more one the Poisson solver.

#include "../alex_functions2.h"



int main() {
  L0 = 1.;
  periodic(left);
  TL.third = true;
  TS.third = true;

  for (j=0;j<=3;j++){

/**
Here we set up the parameters of our simulation. The latent heat $L_H$, the
initial position of the interface $h_0$ and the resolution of the grid.
*/
    latent_heat  = 1/Stefan;
    MAXLEVEL =  5+j ;
    MINLEVEL = MAXLEVEL -1;
    DT2 = 1.e-2/powf(4,j);
    N = 1 << MAXLEVEL;
    snprintf(filename, 100,  "log%d", j);
    fp1 = fopen (filename,"w");
    snprintf(filename, 100,  "out%d", j);
    fp2 = fopen (filename,"w");
    snprintf(filename, 100,  "init%d", j);
    fp3 = fopen (filename,"w");
    init_grid (N);
    run();
    fclose(fp1); 
    fclose(fp2); 
    fclose(fp3);
  }
  exit(1);
}

event init(t=0){

  NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;

  S0 = 1.-2*rootlambda*sqrt(t0); // initial position of the interface
  foreach(){
    dist[] = clamp(plane(y,S0),-1.1*NB_width,1.1*NB_width);
  }
  boundary ({dist});
  restriction({dist});

  vertex scalar dist_n[];
  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs,s_clean);

  boundary({cs,fs});
  restriction({cs,fs});

  curvature(cs,curve);
  boundary({curve});

  foreach() {
    if(cs[]>0.)
      TL[] = T_init(y,t0,S0);
    else
      TL[] = 0.;
    TS[] = 0.;
  }

  foreach(){
    if(x==(0.5+Delta/2.) && cs[] >0){
      fprintf(fp3, "%.8g %.8g %.8g %.8g\n", y, TL[], T_init(y,t0,S0),
       fabs(TL[]-T_init(y,t0,S0)));}
  }

  foreach_face(){
    vpc.x[] = 0.;
  }


  boundary({TL,TS});
  restriction({TL,TS});
  myprop(muv,fs,lambda[0]);

#if GHIGO // mandatory for GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif
}


event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
    dist,latent_heat,cs,fs,TS,TL,T_eq,
    vpc,vpcf,lambda1,lambda2,
    epsK,epsV,eps4,deltat=0.45*L0/(1<<MAXLEVEL),
    itrecons = 30,tolrecons = 1.e-5,NB_width);
  DT = timestep_LS(vpcf,DT2,dist,NB_width);
  tnext = t+DT;
  dt = DT;
}
/**
*/


event tracer_diffusion (i++) {
  advection_LS(
  dist,
  cs,fs,
  TS,TL,
  vpcf,
  itredist = 5,
  s_clean = 1.e-10,
  NB_width,
  curve);

  boundary({TL});
  myprop(muv,fs,lambda[0]);
  mgT = diffusion (TL,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  boundary({TS});
  mgT = diffusion (TS,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
}

// event LS_advection(i++,last){
//   view(tx=  -0.5, ty = -0.5);
//   draw_vof("cs","fs");
//   squares("dist",min = -NB_width ,max = NB_width);
//   squares("dist",min = -NB_width ,max = NB_width);
//   save("dist.mp4");
// }


/**
We output the interface position as a function of time.
*/

// event outputs ( i++,last;t<0.1)
// {
//   // boundary({TL,TS});
//   // scalar visu[];
//   // foreach(){
//     // visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
//   // }
//   // boundary({visu});
//   // view (tx = -0.5, ty = -0.5);
//   // draw_vof("cs");
//   // squares("visu", min = 0, max = 1);
//   // save ("visu.mp4");
  
//   double y_max=0;
//   vector h[];
//   heights (cs, h);
//   boundary((scalar *){h});
//   foreach(reduction(max:y_max)){
//     if(interfacial(point, cs) && y >0){
//       double yy = y+Delta*height(h.y[]);
//       if(yy < 1.e10)y_max = max(y_max,yy);
//     }     
//   }
//   S0 = 1.-2*rootlambda*sqrt(t0+t);
//   fprintf(fp1, "%g %g %g\n", t+t0+dt, y_max, S0);
// }


event final_error(t=0.1){
  scalar e[], ep[], ef[];
  S0 = 1.-2*rootlambda*sqrt(t0+t);
  foreach() {
    if (cs[] <= 0.)
      ef[] = ep[] = e[] = nodata;
    else {
      e[]  = TL[] - T_init(y,t0+t,S0);
      ep[] = cs[] < 1. ? e[] : nodata;
      ef[] = cs[] >= 1. ? e[] : nodata;
    }
  }
  foreach(){
    if(x==0.5+Delta/2. && cs[] > 0){
      fprintf(fp2, "%.8g %.8g %.8g %.8g\n", y, TL[], T_init(y,t0+t,S0),
        fabs(TL[]-T_init(y,t0+t,S0)));
    }
  }
  norm n = normf (e), np = normf (ep), nf = normf (ef);
  fprintf (stderr, "%d %3.3e %3.3e %3.3e %3.3e %3.3e %3.3e %e\n",
   N, n.avg, n.max, np.avg, np.max, nf.avg, nf.max, dt);
  fflush (ferr);
}


#if 0
event adapt (i++, last) {
  // if(i%10==0)fprintf(stderr, "##%g %d\n", t, grid->n);
  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
    fs2.x[]      = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = s_clean);
  fractions_cleanup(cs2,fs2,smin = s_clean);
  restriction({cs,cs2,fs,fs2});
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});

  adapt_wavelet ({cs,visu,vpcf.x, vpcf.y},
    (double[]){1.e-3,1.e-3,5.e-3,5.e-3},MAXLEVEL, MINLEVEL);
}
#endif

#if 0
event movie(i+=10,last){
  if(MAXLEVEL==6){
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});
    squares("visu");
    save("visu.mp4");
  }
}
#endif


/**
~~~bib
@Article{Udaykumar1999,
  author        = {H. S. Udaykumar and R. Mittal and Wei Shyy},
  title         = {Computation of Solid–Liquid Phase Fronts in the Sharp Interface Limit on Fixed Grids},
  year          = {1999},
  volume        = {153},
  pages         = {535-574},
  issn          = {0021-9991},
  doi           = {10.1006/jcph.1999.6294},
}

~~~
*/