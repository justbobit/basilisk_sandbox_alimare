/*
 * =====================================================================================
 *
 *		Filename: 		/
 *
 *		Description: 	/
 *		Source : 		/
 *					
 *
 *		Version:  		/
 *		Created:  		/
 *		Updated:  		/
 *		Compiler:  		gcc
 *
 *		Author:  		Maël DESSEAUX, mael.desseaux@ariane.group
 *		Company:  		ArianeGroup
 *
 * =====================================================================================
 */

/*==================== Includes ====================*/


#define FILTERED 1 // SMEARING OF THE INTERFACE


#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "view.h"
#include "tag.h"
// #include "axi.h"
// #include "tecplot.h" 

//#include "contact.h"

#include "view.h"

#include <stdio.h>
#include <time.h>


/*===================== Données ====================*/


#define MAXLEVEL 10		// Level max maillage adaptatif
#define MINLEVEL 6		// Level min maillage adaptatif, pas obligatoire de le mettre
double tEnd = 15.;  	// t théorique : t = 5e-3 --> t adim : t = 3.87

//		/!\ VOIR L'ADIMENTIONNEMENT  POUR COMPRENDRE LES VALEURS /!\		//
//		/!\ LES VALEURS NE SONT PAS ARBITRAIRES NI DIMENSIONNEES /!\		//

//On prend :
#define rho_l 	1.		// Masse volumique de l'eau
#define rho_g 	1.e-3	// Masse volumique de l'air
#define SIGMA 	1.  	// Tension de surface
#define h 	 	1. 		// Hauteur de la marche


double sigma_vrai = 75.e-3; // tension de surface de l'eau
double h_vrai = 0.5e-3;
//Nombre adimentionnés
#define	Bo		1./30.	// Nombre de Bond 		--> Accélération  : a 	 	= Bo
#define	Oh_l	5.e-3	// Nombre d' Ohnesorge 	--> Viscosité eau : mu_l 	= Oh_l = 1/sqrt(La_l)

// #define	Oh_g	5.e-5	// Nombre d' Ohnesorge 	--> Viscosité air :	mu_g 	= Oh_g = 1/sqrt(La_g)
#define We		2./3.e3	// Nombre de Weber 		--> Vitesse 	  : Vin 	= sqrt(We)

//On obtient :
#define L		40.		// Longueur du domaine
#define H 		10.		// Hauteur du domaine

double	Vin	  = 10.	;	// Vitesse d'entrée  (problème quand on utilise sqrt(We), trop de chiffre)

// double	Vin	  = 5	;	// Vitesse d'entrée  (problème quand on utilise sqrt(We), trop de chiffre)
//double  a     =	Bo	;
//#define a	 	1./3.e-1	// Accélératoin
double vol_cut;

/*=================== Cond. Bord. ==================*/


u.n[left]	= dirichlet(Vin);
u.n[bottom] = dirichlet(0);

u.t[bottom] = dirichlet(0);

uf.t[left]	= dirichlet(0);
uf.n[bottom]= dirichlet(0);

f[left]  	= 1.0;

p[right]   	= dirichlet(0);

pf[right]	= dirichlet(0);


/*======================= Main =====================*/


int main() {
 	int time = 0;		// Calcul du temps de calcul

	size (L);			// Taille du domaine
	origin(-L/2,-L/2.);		// Origine
	init_grid (512);	// Grille initial

	rho1= rho_l	;		// Masses volumiques
	rho2= rho_g	;

	double Oh_g = 1.8e-5 / sqrt(1e3 * sigma_vrai * h_vrai);

	double 	mu_l  = Oh_l;	// Viscosité de l'eau
	double	mu_g  = Oh_g;	// Viscosité de l'air

	vol_cut = 1.*L0*L0/pow(4,MAXLEVEL);

	mu1	= mu_l	; 		// Viscosités
	mu2	= mu_g	; 

	f.sigma	= SIGMA;	// Tension de surface

	fprintf(stderr, "## valeur ohnesorge %g\n", Oh_g);

	//NITERMAX = 100;
	//NITERMIN = 1;
	//TOLERANCE = 1e-8;
	//DT = 1.e-4;
	CFL = 0.5;
	run();

	time = clock();		// Calcul du temps de calcul
	printf("Temps d'execution = %g secondes", time/1000000.);	//Affichage du temps de calcul
}


/*======================= Init =====================*/


event init (t = 0) {
	CFL = 0.5;
	
	mask (y < -L/2. + h && x > 0. && x < h ? bottom : y > -5 ? top : none);		//Mask de la marche

	scalar phi[];			//Equation de l'interface
	foreach_vertex(){
		phi[]=(-L0/2. + L/40 -x);
	}
	fractions(phi,f);		//Interface
}


event acceleration (i++) {	//Gravité
	face vector av = a;
	foreach_face(x)
    	av.x[] = Bo;		//a = Bo             question : -= ou += ou =
}


/*===================== Output =====================*/


// event output_1 (i += 100) //Log
// {
// 	int cellnumber = 0;
// 	foreach()
// 		cellnumber +=1;

// 	fprintf (stderr,"ité.=%d,	tps=%g,	CFL=%g,	DT=%g,	dt=%g,	Cell_Number=%d\n", i,t, CFL, DT, dt, cellnumber);
// }

// event output_2 (t = 0.; t += 0.025; t <= tEnd) //Tecplot
// {
//     char name [500];
//     sprintf(name, "RESULTATS/Bo=0.033/V=1Bo0.033bis/results_Adim-%f.plt", t);
//     output_tecplot(name, i, t, 0.0);
//  }
 
// event output_3 (t = 0.; t += 0.1; t <= tEnd) { //Qté d'eau et d'air
// 	static FILE * fp3 = fopen("RESULTATS/Bo=0.033/V=1Bo0.033bis/qte.dat", "w");

//  	double va = 0., ve = 0., vt = 0., ma = 0., me = 0., mt = 0., vol = 0.;
// 	foreach(reduction(+:va) reduction(+:ve) reduction(+:vt) reduction(+:ma) reduction(+:me) reduction(+:mt) reduction(+:vol)) {
// 	    vol += dv();
// 	    foreach_dimension() {
// 			va += dv()*(1-f[]); 					// volume d'air
// 			ve += dv()*f[]; 						// volume d'eau
// 			vt += dv()*(1-f[]) + dv()*f[]; 			// volume total
// 			ma += rho2*dv()*(1-f[]);				// masse d'air
// 			me += rho1*dv()*f[];					// masse d'eau
// 			mt += rho2*dv()*(1-f[])+rho1*dv()*f[];	// masse totale
//     	}
//   	}
// 	if (i==0)
// 		fprintf(fp3, "t	V_Air V_Eau V_Tot M_Air M_Eau M_Tot \n");
// 	fprintf(fp3, "%g %g %g %g %g %g %g \n", t, va, ve, vt, ma, me, mt);
// }

// event output_4 (i++; t <= tEnd) { //energie cinétique et dissipation
//     static FILE * fp4 = fopen("RESULTATS/Bo=0.033/V=1Bo0.033bis/Energie.dat", "w");
//     coord ubar;
//     foreach_dimension() {
//         stats s = statsf(u.x);
//         ubar.x = s.sum/s.volume;
//     }

//     double ke = 0., vd = 0., vol = 0.;
//     foreach(reduction(+:ke) reduction(+:vd) reduction(+:vol)) {
//         vol += dv();
//         foreach_dimension() {
//             // mean fluctuating kinetic energy
//             ke += dv()*sq(u.x[] - ubar.x);
//             // viscous dissipation
//             vd += dv()*(sq(u.x[1] - u.x[-1])
//                         + sq(u.x[0,1]- u.x[0,-1])
//                         + sq(u.x[0,0,1] - u.x[0,0,-1]))/sq(2.*Delta);
//         }
//     }
//     ke /= 2.*vol;
//     vd *= mu1/vol;

//     if (i == 0)
//         fprintf (fp4, "t dissipation energy\n");
//     fprintf (fp4, "%g %g %g\n", t, vd, ke );
// }

// event output_5 (t = tEnd) //Images de f : utiliser animate
// {
// 	static FILE * fp5 = fopen("RESULTATS/Bo=0.033/V=1Bo0.033bis/f.ppm", "w");
// 	output_ppm (f, fp5, box = {{0,0}, {L, H}} );
// }

// event output_6_7 (t=0.; t += 0.01; t <= tEnd) //Video de f et u, utiliser : animate f.mp4
// {
// 	static FILE * fp6 = popen("ppm2mpeg > RESULTATS/Bo=0.033/V=1Bo0.033bis/f.mp4", "w");
// 	output_ppm (f, fp6, linear=true, box = {{0,0}, {L, H}} );

// 	scalar omega[];
// 	vorticity (u,omega);
// 	static FILE * fp7 = popen("ppm2mpeg > RESULTATS/Bo=0.033/V=1Bo0.033bis/v.mp4", "w");
// 	output_ppm (omega, fp7, linear=true, box = {{0,0}, {L, H}} );
// }

// event output_8 (t=0.; t += 0.01; t <= tEnd) //video grid
// {
// 	scalar l[];
// 	foreach()
// 		l[] = level;
// 	static FILE * fp8 = popen("ppm2mpeg > RESULTATS/Bo=0.033/V=1Bo0.033bis/grid.mp4", "w");
// 	output_ppm (l, fp8, min = 0, max = 12, linear=true, box = {{0,0}, {L, H}} );
// }

// event output_9_10_11_12 (t <= tEnd; t += 1.0) {
// 	static int nf = 0;

// 	char VOF_name[80];
// 	sprintf (VOF_name, "RESULTATS/Bo=0.033/V=1Bo0.033bis/VOF_t=%d.dat", nf);
// 	FILE * fp9 = fopen(VOF_name, "w");
// 	output_field ({f}, fp9);

// 	scalar l[];
// 	foreach()
// 		l[] = level;
// 	char Grid_name[80];
// 	sprintf (Grid_name, "RESULTATS/Bo=0.033/V=1Bo0.033bis/Grid_t=%d.dat", nf);
// 	FILE * fp10 = fopen(Grid_name, "w");
// 	output_field ({l}, fp10);
	
// 	scalar omega[];
// 	vorticity (u,omega);
// 	char Vorticity_name[80];
// 	sprintf (Vorticity_name, "RESULTATS/Bo=0.033/V=1Bo0.033bis/Vorticity_t=%d.dat", nf);
// 	FILE * fp11 = fopen(Vorticity_name, "w");
// 	output_field ({omega}, fp11);
	
// 	char Velocity_name[80];
// 	sprintf (Velocity_name, "RESULTATS/Bo=0.033/V=1Bo0.033bis/Velocity_t=%d.dat", nf);
// 	FILE * fp12 = fopen(Velocity_name, "w");
// 	output_field ({u.x,u.y}, fp12);
	
// 	nf++;
// 	printf ("%d\n", nf);
// }

// event output_13 (i++; t <= tEnd) { //nombre de cellule
//     static FILE * fp13 = fopen("RESULTATS/Bo=0.033/V=1Bo0.033bis/Cell_Number/Cell_Number.dat", "w");

// 	int cellnumber = 0;
// 	foreach()
// 		cellnumber +=1;

//     if (i == 0)
//         fprintf (fp13, "Nombre de cellule \n");
//     fprintf(fp13, "%d %d\n", i, cellnumber );
// }


/*=============== MAillage Adaptatif ===============*/


event adapt (i++) { //Maillage adaptatif sur f
	adapt_wavelet ({f,u}, (double[]){1.e-2,8.e-3,8.e-3}, MAXLEVEL, MINLEVEL);
}


event movies ( t+=0.01,last;t<tEnd){
	view(width = 1000, height = 1000);
	draw_vof("f");
    squares("u.x", min = 8., max = 12., map = cool_warm);
    save("ux.mp4");
}

event remove_drops ( i+=10 )
{
  scalar m[];
  foreach()
    m[] = f[] > 0.;
  int n = tag (m);
  double v[n];
  int remove_flag[n];
  for (int j = 0; j < n; j++) {
    v[j] = 0.;
    remove_flag[j] = 0;
  }
  foreach(serial)
    if (m[] > 0) {
      int j = m[] - 1;
      v[j] += dv()*f[];
    }

#if _MPI
  MPI_Allreduce (MPI_IN_PLACE, v, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

  for (int j = 0; j < n; j++)
    if ( v[j] < vol_cut )
       remove_flag[j] = 1;

  foreach()
    if (m[] > 0) {
      int j = m[] - 1;
      if ( remove_flag[j] == 1 ){
          f[]=0.;
          fprintf(stderr,"## small drop removed\n");
        }
    }
}

event mylog(i+=5){
	stats s = statsf (u.x);
  	stats s2 = statsf (u.y);
  	fprintf (stderr, "%g %g %ld %g %g %g %g\n", \
  	t, dt , grid->tn, s.min, s.max, s2.min, s2.max);
}

/*======================= Fin ======================*/


event end (t = tEnd) 
{return 1;}
