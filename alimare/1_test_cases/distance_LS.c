/**
# Distance field computation from a 3D model

The goal is to build a distance field representation of a 3D
[CAD](https://en.wikipedia.org/wiki/Computer-aided_design) model. */

#include "grid/octree.h"
#include "embed.h"
#define QUADRATIC 1
#define BGHOSTS 2
#define quadratic(x,a1,a2,a3) \
  (((a1)*((x) - 1.) + (a3)*((x) + 1.))*(x)/2. - (a2)*((x) - 1.)*((x) + 1.))
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "utils.h"
#include "fractions.h"
#include "view.h"
int n_part = 0;
#include "../alex_functions.h"
#include "../LS_reinit.h"

int main()
{
  
  restore("../dump_atomisation3D");
  fprintf(stderr, "END OF RESTORE\n" );
  // taken from https://www.thingiverse.com/thing:4702654
  

  /**
  We initialize the distance field on the coarse initial mesh and
  refine it adaptively until the threshold error (on distance) is
  reached. */

  scalar d[];
  foreach(){
    if(f[]*(1-f[])!=0.){
      coord n       = facet_normal( point, cs ,fs) , p;
      normalize(&n);
      double alpha  = plane_alpha (cs[], n);
      plane_area_center (n, alpha, &p);
      double dotproduct= 0;
      foreach_dimension()
        dotproduct += n.x*p.x;
      double locadist = norm2(p);
      d[] = sign2(dotproduct)*locadist*Delta;
    }
    else if(f[]  == 0){
      d[] = -Delta;
    }
    else if(f[]  > 0){
      d[] = Delta;
    }
  }
  boundary({d});
  restriction({d});
  fprintf(stderr, "END OF INITIALIZATION OF DISTANCE\n" );
  scalar d2[];
  for (int i = 0; i < 10; i++){
      /* code */
    foreach()
      d2[] = d[];
    boundary({d2});
    restriction({d2});
    LS_reinit(d, it_max = 10+i*5); // the first 30 cells will have a correct

    squares("d2", min = -1.e-2, max = 1.e-2);
    cells();
    char name[80];
    view(fox = 5);
    sprintf (name, "reinit_itmax-%d.png", 10+i*5);
    save(name);
  }
  
  fprintf(stderr, "END OF FAST SWEEPING\n" );
  // distance
  dump(list = {d,f});
  exit(1);
}
