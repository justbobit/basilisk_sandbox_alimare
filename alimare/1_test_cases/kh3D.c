/**
![Clouds can serve as tracers to reveal the occurrence of a shear
 instability in the atmosphere. Image courtesy of
 [EarthSky](http://earthsky.org/earth/kelvin-helmholzt-clouds).](http://en.es-static.us/upl/2016/05/kelvin-hemhotltz-wave-cloud-mashka.jpg)

# The Kelvin-Helmholtz instability in two dimensions

On this page a classical shear instability is simulated. According to
the Kelvin-Helmholtz instability description, a thin shearing layer
may be unstable and can then roll-up on itself creating vortices, or
so-called Kelvin-Helmholtz billows. On this page the effect of the
fluid's viscosity on the observed dynamics is investigated.

## Set-up

A fluid with an infinetly thin shear layer is initialized. The
difference in velocity between the induvidual layers *U* and the
length scale associated with the periodicity of the solution
($\mathcal{L}$) in our set-up can be used to normalize the value of
the fluid's viscosity. Resulting in a Reynolds number,

$$Re=\frac{U\mathcal{L}}{\nu}.$$  

In this study, the Reynolds number is varied to be $Re=\{20000, 40000,
80000\}$. In order to analyze the flow's evolution, the emergence of
coherent vortex structures is monitored. Therefore, the number of
vortices is counted using the marvalous `tag()` function. It is
remarkable how complex seamingly simple things can be.
*/
#include "grid/octree.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"

#define Pi 3.141592

// void mynoise(Point point, vector u){
//   u.y[] = 0.0004*noise();
// }

int maxlevel;
double vis, Re, We;
FILE * fpn;

double delta;
scalar omega[];


double plane(Point point, double size){
  return  y  + size/8.*exp(-(x*x+z*z)/(size*size))*sin(3.14*x/(2.*size))*sin(3.14*z/(2.*size));
}

double velo_init(Point point, double delta){
  return (erf( (y-delta)/(delta/2) ) + 1)/2.;
}
/**
  As mentioned earlier periodic boundaries are used in the stream-wise
  direction. Furthermore, the boundaries in the span-wise direction of
  the square domain are of the free-slip type.
*/
int main(){
  periodic(left);
  X0 = Y0 = Z0 =  -L0/2;
  
  maxlevel = 8;
  Re = 100;
  We = 1000;
  delta = 4*L0/(1 << maxlevel);
  init_grid (1 << 8);
  rho1 = 1., rho2 = 1.;
  mu1 = delta/Re, mu2 = delta/Re;
  f.sigma = delta/We;
  run();
}

event init(t = 0){
  /**
     The shear layer is initialized and a small random perturbation is
     added to the span-wise-velocity-componenent field to kick-off the
     growth of the instability.
  */
  astats adapting;
  int n = 0;
  
  do{
    foreach_face(x)
      u.x[] = velo_init(point, delta)-pow(sin(Pi*x),2.)*sin(2.*Pi*y)*exp(-(x*x+y*y+z*z)/(delta*delta));
    foreach_face(y)
      u.y[] = pow(sin(Pi*y),2.)*sin(2.*Pi*x)*exp(-(x*x+y*y+z*z)/(delta*delta));    
    boundary ((scalar *){u});
    vorticity(u,omega);

    vertex scalar LS[];
    scalar LS2[];
    foreach_vertex()
      LS[] = plane(point,delta);

    foreach()
      LS2[] = plane(point,delta);
    boundary({LS,LS2});
    fractions(LS,f);
    boundary({f});
    restriction({f});
    fprintf(stderr, "%d\n", n);
    boundary ((scalar *){u});
// mandatory for adapt_wavelet
    trash ({uf});
    foreach_face()
      uf.x[] = fm.x[]*face_value (u.x, 0);
    boundary ((scalar *){uf});

    /**
    We update fluid properties. */

    event ("properties");

    /**
    We set the initial timestep (this is useful only when restoring from
    a previous run). */

    dtmax = DT;
    event ("stability");

    adapting = adapt_wavelet ({ f,omega}, (double[]){1.e-3,1.e-3}, maxlevel);
    n++;
  }while (adapting.nf && n <100);
  view (quat = {-0.205, 0.234, 0.047, 0.949},
      fov = 5,
      tx = -0.000, ty = 0.002, 
      width = 1000, height = 1000);
  draw_vof (c = "f");
  cells (alpha = -0.2);
  squares (color = "omega", map = cool_warm, alpha = -0.2);
  char name[80];
  sprintf(name,"init%g.png",Re);
  save(name);

  view (quat = {-0.205, 0.234, 0.047, 0.949},
      fov = 5,
      tx = -0.000, ty = 0.002, 
      width = 1000, height = 1000);
  draw_vof (c = "f");
  cells (alpha = -0.2);
  squares (color = "omega", map = cool_warm, alpha = -0.2);
  char name[80];
  sprintf(name,"init%g.png",Re);
  save(name);
  fprintf(stderr, "%g", delta);
 
  /**
     Output file names are defined and their names carry a
     Reynolds-number identifier.
  */
  char name1[99];
  sprintf (name1, "nrofvortices%g", Re);
  fpn = fopen (name1, "w");
}
/**
## Output
   
   The usual suspects of animation types are generated. Meaning that
   the evolution of the vorticity ($\omega$) field and the grid
   resolution are visualized. The maximum value of the vorticity field
   ($\|\omega\|_{max}$) is monitored. consequently, a vortex can be
   defined as a connected region with a vorticity value
   $\|\omega\|>\|\omega\|_{max}/3$. Based on this definition, we count
   and log the number of vortices.
*/

event output (t = 1.; t += 0.01){
  scalar omega[], lev[], f[];
  int n = 0;
  double m = 0;
  foreach(reduction(+:n) reduction(max:m)){
    n++;
    lev[] = level;
    omega[] = (u.x[0,1]-u.x[0,-1] - (u.y[1,0]-u.y[-1,0]))/(2*Delta);
    if (fabs(omega[]) > m)
      m = fabs(omega[]);
  }
  boundary({omega});
  view (quat = {-0.205, 0.234, 0.047, 0.949},
      fov = 5,
      tx = -0.000, ty = 0.002, 
      width = 1000, height = 1000);
  draw_vof (c = "f");
  cells (alpha = -0.2);
  squares (color = "omega", map = cool_warm, alpha = -0.2);
  char name[80];
  sprintf(name,"omega%g.mp4",Re);
  save(name);
  foreach()
    f[] = (omega[] > m/3.); //1 or 0
  int nrv = tag(f);
  fprintf (fpn,"%g\t%d\t%g\t%d\t%d\t%d\n",t, nrv, m,
           n,((1 << (maxlevel*dimension)))/(n), i);
}

/**
## The adaptation and the end-of-run events

The grid resolution is adapted based on the wavelet-estimated
discretization error in the representation of the velocity vector
field. In the end-of-run event, the files associated with the run's
output file pointer is closed.
*/

event adapt (i++){
  vorticity(u,omega);
  adapt_wavelet ({f,omega,u}, (double[]){0.01,0.01,0.01,0.01,0.01}, maxlevel);
  fprintf(stderr, "%g %ld\n", t, grid->tn);
}


event snapshop(t=1;t++){
  char name[80];
  sprintf (name, "snapshot-%g", t);
  dump(name);
}

event end (t = 1.)
  fclose(fpn);

/**
## Results

No results for now
*/

   
