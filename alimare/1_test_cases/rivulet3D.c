/**
Pour le 3D imposer un profil d'entrée parabolique.
Recupere les nombres sans dimension de l'expé.
Reynolds, Weber, effet de la gravité

Effet de la thermique
*/

#include "grid/octree.h"
#include "navier-stokes/centered.h"
#include "contact.h"
#include "two-phase.h"
#include "tension.h"

#include "view.h"

#define Pi 3.141519265
#define ALPHA 30*Pi/180.
#define inletV 1.
#define Radius 0.05
#define yinletcenter 0.005
#define G0 1.
#define SIGMA 3e-5


int maxlevel = 8;
double uemax = 0.05;
scalar f0[];
// scalar TL[],TS[];

face vector av[];
vector h[];

/**
The inlet condition a only for a small portion of the left boundary, so we
define a tagging function for that
*/ 
coord changeref(double x, double y){
  coord X;
  X.x = x*cos(ALPHA) + y*sin(ALPHA);
  X.y = -x*sin(ALPHA) + y*cos(ALPHA);
  return X;
}

double mycylinder(double x, double y, double z){
  coord X = changeref(x,y);
  return sq(Radius) - sq(X.x-yinletcenter) - sq(X.y) - sq(X.z-0.5);
}

// u.n[left] = dirichlet (mycylinder(x,y,z) >0 ? 0. :  inletV*sin(ALPHA));
// u.t[left] = dirichlet (mycylinder(x,y,z) >0 ? 0. : -inletV*cos(ALPHA));
// u.t[bottom] = dirichlet(0.);

#if dimension > 2
// u.r[left]  = dirichlet(0);
#endif
p[left]    = neumann(0);
f[left]    = f0[];

u.n[right] = neumann(0);
p[right]   = dirichlet(0);

int main (){

  init_grid (64);
  rho1 = 1., rho2 = 1./100.;
  mu1 = 2.*rho1/200, mu2 = 2.*rho2/200;  

  DT = 0.1;
  a = av;
  run();
}

event init(i=0){
  f.sigma = SIGMA;
  f.height = h;
  double iteration = 0;
  do {
    iteration++;
    fraction(f0, sq(Radius) - sq(x) - sq(y) - sq(z-0.5));
  } while( adapt_wavelet({f0}, (double []){0.001},
    maxlevel = maxlevel, 5).nf != 0 && iteration <= 10);

  f0.refine = f0.prolongation = fraction_refine;
    restriction ({f0}); // for boundary conditions on levels
    foreach(){
      f[] = f0[];
      u.x[] = f[]*sin(ALPHA);
      u.y[] = -f[]*cos(ALPHA);
    }
    boundary({f,u.x,u.y});


  view (fov = 20.6731, quat = {-0.162898,0.28249,0.158626,1.02224}, 
    tx = -0.0498, ty = 0.36852, bg = {0.3,0.4,0.6});

  box();
  // draw_vof("f");
  cells({0,0,1},0.5);

  // squares("f", min = 0, max =1, n = {0,0,1} , alpha = 0.5);
  squares("u.x", min = 0, max =1, n = {0,0,1} , alpha = 0.5);
  save("init.png");
  dump();
}

event acceleration(i++){
  foreach_face(x){
    av.x[] = G0*sin(ALPHA); 
  }
  foreach_face(y){
    av.y[] = -G0*cos(ALPHA);
  }
  boundary((scalar *){av});
}

event movies(t+=0.01;t<30){

  view (fov = 20.6731, quat = {-0.162898,0.28249,0.158626,1.02224}, 
    tx = -0.0498, ty = 0.36852, bg = {0.3,0.4,0.6});
  box();
  draw_vof("f");
  cells({0,0,1},0.5);
  squares("f", min = 0, max =1, n = {0,0,1}, alpha = 0.5);
  save("f.mp4");
}
event logfile(i++){
  int count=0;
  foreach(reduction(+:count)){
    count++;
  }
  fprintf(stderr, "%g %d\n", t,count);
}
event snapshots(t+=1.){
  // fprintf(stderr, "%g\n", t);
  // output_facets (f, stdout);
  dump();
}

event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){0.01,uemax,uemax,uemax}, maxlevel);
}

event final_image(t=end){
}
