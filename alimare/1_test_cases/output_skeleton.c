#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#define radius 1./12.
#define length 0.025
#define Re 5800
#define SIGMA 3e-5


int maxlevel = 10;
double uemax = 0.1;

void data_analysis(scalar skeleton, scalar f){
  // we try to do some data analysis

}


int main (int argc, char * argv[])
{
  if (argc > 1)
    maxlevel = atoi (argv[1]);
  if (argc > 2)
    uemax = atof (argv[2]);

  init_grid (64);
  origin (0, -1.5, -1.5);
  size (3.);

  /**
  We set the density and viscosity of each phase as well as the
  surface tension coefficient and start the simulation. */
  
  rho1 = 1., rho2 = 1./27.84;
  mu1 = 2.*radius/Re*rho1, mu2 = 2.*radius/Re*rho2;  
  f.sigma = SIGMA;

  run();
}

event init (t = 0) {
  char name[80],name2[80];
  double minmax = 200;
  for (double temp = 0.6; temp < 1; temp+=0.1){
    sprintf (name, "snapshot-%g", temp);
    fprintf(stderr, "%s\n", name);
    if (restore (file = name)) {
      fprintf(stderr, "%g\n", temp);
      view (quat = {0.000, 0.000, 0.000, 1.000},
      fov = 4,
      tx = -0.115, ty = -0.1,
      width = 1620, height = 974);
      scalar omega[];
      vorticity (u, omega);

      squares("omega", map=cool_warm, linear = true, min =-minmax, max = minmax);
      draw_vof("f");
      sprintf (name2, "interface_no-adapt-%g.png", temp);
      save(name2);
    }
  }
  for (double temp = 0.6; temp < 1; temp+=0.1){
    sprintf (name, "snapshot_adapt-%g", temp);
    fprintf(stderr, "%s\n", name);
    if (restore (file = name)) {
      fprintf(stderr, "%g\n", temp);
      view (quat = {0.000, 0.000, 0.000, 1.000},
      fov = 4,
      tx = -0.115, ty = -0.1,
      width = 1620, height = 974);
      scalar omega[];
      vorticity (u, omega);
      squares("omega", map=cool_warm, linear = true, min =-minmax, max = minmax);
      draw_vof("f");
      sprintf (name2, "interface_adapt-%g.png", temp);
      save(name2);
    }
  }
}

