/**
# Simulation of granular flows using cellular automata

For fun.
*/

#define BGHOSTS 2
#include "run.h"
#include "view.h"

double mynoise(Point point, double t){
	double a = ((noise()+1.)/2.); // random number between 0 and 1.
	fprintf(stderr, "%4.2g %g %g\n ", a ,x, y);
	return a;
} 

unsigned int mytime;


scalar cellularA[]; // boolean
scalar timeStill[]; // time a grain has spent still
scalar sandcolor[];
scalar copy[],copycolor[],tag[];




bool cond1(Point point, scalar c, int j){
	return (c[-1,1-j] && c[0,1-j] && c[1,1-j] && c[0,-j] ==0);
}

bool cond2(Point point, scalar c){
	return (c[0,0] && c[0,1] && c[0,2] && c[0,-1] ==0);
}

bool cond3(Point point, scalar c){
	return (c[-1,0] && c[] && c[1,0] && c[0,1] && c[0,-1] ==0);
}

int main(){
	origin(-L0/2.,-L0/2.);
	init_grid(32);
	periodic(right);
	copy[bottom]      = dirichlet(1.);
	timeStill[top]    = dirichlet(mynoise(point,t)*10);
	sandcolor[top]    = dirichlet(12);
	cellularA[bottom] = dirichlet(1.);
	run();
}

event init(i=0){
	// srand (0);
	mytime = 0.;
  foreach(){
    cellularA[] = mynoise(point,0.) > 0.75 ? 1. : 0.;
    timeStill[] = 0.;
    sandcolor[] =(int) (cellularA[] ? (y+0.5*L0)*10+1 : 0.);
  }
  boundary({cellularA,timeStill,sandcolor});
}

event advection(i++;i<200){
	mytime += 1;
	srand(mytime);
	cellularA[top]    = mynoise(point,mytime);
	copy[top]         = mynoise(point,mytime);
	boundary({cellularA});
	fprintf(stderr, "\n");
	fprintf(stderr, "%g\n", L0/2+L0/(1 << (grid->maxdepth)));
	fprintf(stderr, "\n");
/**
Lets copy some stuff + ageing process
*/
	foreach(){
		copy[] = cellularA[];
		copycolor[] = sandcolor[];
		tag[]  = 0.;
		if(cellularA[])
			timeStill[]+=0.1;
		else
			timeStill[] = 0.;
	}
	boundary({copy,tag,copycolor,timeStill});


/**
## Step 1: Advection 

Simple rule
*/
	foreach(){
		if(cond2(point,copy) || cond3(point,copy)){
			tag[] = 1;
			cellularA[] = 0;
		}
	}
	boundary({tag});

	foreach(){
		if(tag[0,1]){
			cellularA[] = 1;
			sandcolor[] = copycolor[0,1];
			timeStill[] = 0.;
		}
	}
	boundary({cellularA});
/**
## Step 2: Erosion
*/
	// tag[bottom] = dirichlet(0.);
	// foreach(){
	// 	if(copy[] ==0. && copy[0,1] && (mynoise() > 0.95-timeStill[0,1]/100.) ){
	// 		tag[] = 10.;
	// 		cellularA[] = 1.; 
	// 		timeStill[] = 0.;
	// 		sandcolor[] = copycolor[0,1];
	// 	}
	// }
	// boundary({tag,cellularA,sandcolor,timeStill});

	// foreach(){
	// 	if(tag[0,-1]==10. && point.j >1){
	// 		cellularA[] = 0.;
	// 		timeStill[] = 0.;
	// 		sandcolor[] = 0.;
	// 	}
	// }
	// boundary({cellularA,timeStill,sandcolor});

	fprintf(stderr, "%d %u\n", i,mytime);
	// char name[80];
	// sprintf(name, "image_%d.png", i);

	// output_ppm(cellularA,n= 512, file = "film.gif", min = 0, max = 1);
	squares("cellularA", map = gray);
	save("film.mp4");
  // squares("timeStill", map = gray, min =0, max = 20);
  // save("time.mp4");
  squares("sandcolor", min = 0, max = 5);
  save("sandcolor.mp4");
	// output_ppm(timeStill, mask  = m, n = 512, file = "time.gif", min = 0, max = 20, opt = "--delay 1");
}

/**
##Outputs


![Animation of the motion of the grains](granularCA1/film.gif)
*/


/**
TODO:
- bool/int fields (medium)
- use chinese method to simulate presence of a dam
- use of adaptive grids: restriction/prolongation for bool (simple)
*/
