/**
# Spinodal decomposition with a phase field method

 */
#define growthTimestep 0.1
#define MU 1e-5
#include "../vdw.h"
#include "view.h"
#include "../diverging_cool_warm.h"
/**



![Filled isoline of the density during simulation + norm of the velocity](spinodal/rho.mp4)

*/

#define LEVEL 8
#define LAMBDA 1.4e-1
#define rho_c 1.06344
#define R_g 1 
#define theta 0.9
#define p_c 1


// Calculate adimensionalized numbers.

double P0(double x)
{
  double rhop;
  rhop=x/rho_c;
  return p_c*rhop*theta*(8/(3-rhop) - 3*rhop/theta);
  // return 0.5*sq(x);
}

double mynoise(){
  return (1-2.*exp(noise())/exp(1));
}
  
int main()
{
  origin (-0.5, -0.5);
  periodic(right);
  periodic(top);
  init_grid (1 << LEVEL);
  DT = 1e-4;
  run();
}

event init (i = 0)
{
  mgu.nrelax = 10;
  lambda=LAMBDA;
  TOLERANCE = 1.e-6;
  foreach()
    {
      foreach_dimension()
        mu.x[] = MU;
      rho[] = rho_c *( 1.+0.2*noise());
      q.x[] = q.y[] = 0.;
    }
  boundary({mu,rho,q});
}

event meshes(t=0.05;t+=0.1){
  isoline("rho", rho_c);
  cells();
  char filename [100];
  sprintf(filename,"mesh%g.png",t);
  save(filename);
}




event outputfile (t=1.e-2;t*=1.01) {
  face vector fs[];
  isoline("rho", rho_c);
  squares("rho",  map = mycoolwarm);
  save("rho.mp4");

  isoline("rho", rho_c);
  cells();
  save("cells.mp4");  

  stats s = statsf (rho);
  stats s2 = statsf(u.x);
  fprintf (stderr, "%6.4g %4.2e    %6.3g %6.2g %4.2g %4.2g %ld\n", t,  dt,
    s.min,s.max,s2.min,s2.max, grid->tn);
}


event adapt(i++){
  adapt_wavelet ({rho,u.x,u.y},
    (double[]){6.5e-3,1.5e-2,1.5e-2},LEVEL, 5);
}


event end (t=10){
  system("rm out.mp4");
  system("ffmpeg -framerate 5 -pattern_type glob -i '*.png' -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4");
  system("rm mesh*");
  dump();
  return 1;
}


/**
~~~gnuplot Number of cells
set logscale y 4
set yrange [0.9*4**7:1.1*4**8]
set format y "2^{%L} * 2^{%L}"
set xlabel "time"
set ylabel "nb cells"
plot 'log' u 1:7 w l t 'nb cells'
~~~
 */