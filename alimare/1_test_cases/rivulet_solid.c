/**
 # Freezing a rivulet 

Test of NS + diffusion in 2D

Effect of gravity
*/


#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846
#define QUADRATIC 1
#define GHIGO 1


#include "../../ghigo/src/myembed.h"
#include "../embed_extrapolate_3.h"


#include "../double_embed-tree.h"

#include "navier-stokes/centered.h"
#include "tracer.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "view.h"

#include "../level_set.h"
#include "../LS_advection.h"

#include "../basic_geom.h"

#define ALPHA 15*Pi/180.
#define inletV 1.
#define G0 10.

#define MAXLEVEL 8
#define MINLEVEL 6
double uemax = 0.1;

void changeref(double x, double y, double * x2, double * y2){
  *x2 = x*cos(ALPHA) + y*sin(ALPHA);
  *y2 = -x*sin(ALPHA) + y*cos(ALPHA);
}


#define T_eq 0
#define TL_inf       1.     // T1 in the paper
#define TS_inf       -10.     // T0 in the paper

scalar TL[];                // Temperature for liquid
scalar * tracers = {TL};   

scalar TS[];                // Temperature for solid
scalar * tracers2 = {TS};

scalar dist[];              // Levelset function
scalar * level_set = {dist};

vector vpc[],vpcf[];

face vector muv[];

double  latent_heat = 10.;
double  lambda[2];   

// no Gibbs Thomson effect.
double  epsK = 0.000, epsV = 0.000;
double eps4 = 0.;

scalar curve[];

int     nb_cell_NB;
double  NB_width ;          // length of the NB


double nu = 1.e-1;             // Viscosity
face vector muc[], av[];

// X= -l0/2 => TL = TL_inf
// X = -L0/4. => TL = 0

TL[bottom] = dirichlet(x > -L0/4. ? TL_inf - (x+1./2)*4:0);
TS[bottom] = dirichlet(x > -L0/4. ? TS_inf : 0.);

TL[embed]  = dirichlet(T_eq);
TS[embed]  = dirichlet(T_eq);

u.n[embed] = dirichlet(0.);
u.t[embed] = dirichlet(0.);

u.n[left] = dirichlet (inletV*sin(ALPHA));
u.n[right] = neumann(0);
p[left]    = neumann(0);

p[right]   = dirichlet(0);

double DT2;
#include "../alex_functions2.h"

int main (){
	origin (-L0/2,-L0/2);
  init_grid (1<<MAXLEVEL);
  mu = muc;
  a = av;

  TL.third = true;
  TS.third = true;

  run();
}


event init(i=0){
	

  double myDelta = L0 / (1 << MAXLEVEL);
  DT2 = myDelta;
  nb_cell_NB = 1 << 2 ;  
  NB_width   = nb_cell_NB * myDelta;

  coord center = {L0/2.,-L0/2.+1.97*myDelta}, size = {0.1099*L0,3.97*myDelta};
  foreach()
  	dist[] = rectangle(x,y,center,size);

  boundary({dist});
  restriction({dist});

  LS_reinit(dist);
  vertex scalar dist_n[];
  cell2node(dist,dist_n);
  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});
  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  lambda[0] = 0.1;
  lambda[1] = 0.1;


  foreach() {
    TL[] = cs[]>0. ? TL_inf: 0.;
    TS[] = TS_inf;
    u.x[] = inletV*sin(ALPHA);
  }
  boundary((scalar *){TL,TS,u});
  restriction((scalar *){TL,TS,u});

  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);

}

event acceleration(i++){
  foreach_face(x){
    av.x[] = G0*sin(ALPHA); 
  }
  foreach_face(y){
    av.y[] = -G0*cos(ALPHA);
  }
  boundary((scalar *){av});
}

event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}


event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
  dist,latent_heat,cs,fs,TS,TL,T_eq,
  vpc,vpcf,lambda1,lambda2,
  epsK,epsV,eps4,deltat=0.45*L0/(1<<MAXLEVEL),
  itrecons = 35,tolrecons = 1.e-6,
  NB_width
  );
  DT = 0.2*timestep_LS(vpcf,DT2,dist,NB_width);
}


event tracer_diffusion (i++) {
  advection_LS(
  dist,
  cs,fs,
  TS,TL,
  vpcf,
  itredist = 5,
  s_clean = 1.e-10,
  NB_width,
  curve);

  boundary({TL});
  event("properties");
  mgstats mgT;
  mgT = diffusion (TL,dt,muc,theta = cm);
  if(writefile(mgT))exit(1);

  invertcs(cs,fs);
  event("properties");
  mgT = diffusion (TS,dt,muc,theta = cm);
  if(writefile(mgT))exit(1);
  invertcs(cs,fs);
  event("properties");

  foreach(){
    if(cs[]==0.)TL[] = 0.;
    if(cs[]==1.)TS[] = 0.;
  }
  boundary({TL,TS});
  restriction({TL,TS});
}


event movies(t+=3e-4;t<0.06){
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[]/fabs(TS_inf) ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof("cs");
  squares("visu", min = -1. , max = TL_inf);
  save("temperature.mp4");

  draw_vof("cs");
  squares("u.x*cs");
  save("vitesse.mp4");
  fprintf(stderr, "%g\n", t);
}


#if 1 
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[] = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  adapt_wavelet ({cs,visu,u},
    (double[]){0.005,1.e-3,uemax,uemax},MAXLEVEL, MINLEVEL);
  int n = 0;
  foreach(reduction(+:n))
    n++;
  if(i%10==0) fprintf(stderr, "##nb cells %d\n", n);
}
#endif
