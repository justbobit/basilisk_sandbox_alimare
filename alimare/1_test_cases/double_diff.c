#define BICUBIC 1
#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846

#include "embed.h"
#include "../double_embed-tree.h"
#include "../advection_A.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "view.h"
#include "../level_set.h"
#include "../LS_reinit.h"
#include "../LS_advection.h"
#include "../LS_recons.h"
#include "../LS_curvature.h"
#include "../phase_change_velocity.h"


#define T_eq          0.
#define TL_inf        1.
#define TS_inf       -1.

#define tstart 0.

double H0;
double latent_heat;

#define DT_MAX  1.

#define T_eq         0.


#define plane(x, y, n) (y  + 0.05*sin(2.*n*Pi*x))
// #define plane(x, y, n) (y  - 0.2)
// #define plane(x, y, n) (y  - 6.e-4)


scalar TL[], TS[], dist[];
scalar * tracers = {TL};
scalar * tracers2 = {TS};
scalar * level_set = {dist};

vector vpc[];
scalar * LS_speed;
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];

#if Gibbs_Thomson
double  epsK = 0.0005, epsV = 0.0005;
#else
double  epsK = 0.000, epsV = 0.000;
#endif
scalar curve[];

#define GT_aniso 0
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif


double lambda[2];

int     nb_cell_NB =  1 << 3 ;  // number of cells for the NB
double  NB_width ;              // length of the NB


  
mgstats mg1,mg2;

TL[embed] = dirichlet(Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));
TL[top]   = dirichlet(TL_inf); 

TS[embed]  = dirichlet(Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));
TS[bottom] = dirichlet(TS_inf); 

int j;
int k_loop = 0;
/**
The domain is 4 units long, centered vertically. */

int main() {
  periodic(right);

  L0 = 1.;
  latent_heat  = 1;

  origin (-0.5*L0, -0.5*L0);
  init_grid (1 << 7);
  run();
}

event init(t=0){

  TOLERANCE = 1.e-8;
  DT = 0.5*L0/(1 << 7);
  lambda[0] = 1.;
  lambda[1] = 1.;


  foreach(){
     dist[] = clamp(plane(x,y,2),-0.5, 0.5);
  }
  boundary ({dist});
  restriction({dist});
  vertex scalar dist_n[];
  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);

  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});

  foreach() {
    TL[] = TL_inf;
    TS[] = TS_inf;
  }
  boundary({TL,TS});
  restriction({TL,TS});

  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc});
}

event tracer_diffusion(i++){
  int kk;
  foreach_face()
    muv.x[] = fs.x[];
  boundary((scalar *) {muv});
  for (kk=1;kk<=4;kk++){
    if(i%2==0){
      boundary({TL});
      diffusion(TL, dt, D = muv , theta = cs);
    }
    else{
      boundary({TS});
      diffusion(TS, dt, D = muv, theta = cs);
    }
  }
}

event LS_advection(i++,last){
  if(i%2 == 1){
    double L_H       = latent_heat;  

    scalar cs0[];

    foreach(){
      cs0[]   = cs[];
      cs[]    = 1.-cs[];
    }
    foreach_face(){
      fs.x[]  = 1.-fs.x[];
    }

    boundary({cs,fs,cs0});
    restriction({cs,fs});

    phase_change_velocity_LS_embed (cs, fs ,TL, TS, vpc, L_H, 
      lambda,epsK, epsV, aniso);

    double deltat  = 0.45*L0 / (1 << grid->maxdepth);  // Delta
    int err = 0;
    int k_limit = 0;

    vector vpc_r[];
    foreach(){
      foreach_dimension(){
        if(interfacial(point, cs))vpc_r.x[] = vpc.x[];
        else vpc_r.x[] = 0.;
      }
    }
    boundary((scalar * ){vpc_r});
    restriction((scalar * ){vpc_r});

    LS_speed  = {vpc_r.x,vpc_r.y};

    recons_speed(dist = dist, dt = deltat, 
      LS_speed = LS_speed,
     k_limit = k_limit, tolerance = 1.e-4, err = &err, 
     nb_iter = 200, overshoot = 0.4,
     cs = cs , fs =fs);

    face vector vpcf[];
    foreach_face(){
      vpcf.x[] = LS_face_value(vpc_r.x,0);
    }
    boundary ((scalar *){vpcf});
    double dt_LS = timestep_LS (vpcf, DT);

    advection_LS (level_set, vpcf, dt_LS);
    // myRK2(dist,vpcf,dt_LS);
    
    boundary ({dist});
    restriction({dist});

/**
After the advection, we need to redistance the level-set function.
*/
    double mingrad = HUGE, maxgrad = -HUGE;
    foreach(reduction(max:maxgrad) reduction(min:mingrad)){
      double grad = 0;
      if(fabs(dist[]) < 4*Delta){
        foreach_dimension(){
          grad += sq((dist[1,0]- dist[-1,0])/(2.*Delta));
        }
        grad = sqrt(grad) -1.;
        mingrad = min(mingrad,grad);
        maxgrad = max(maxgrad,grad);
      }
    }
    double test = max(maxgrad,-mingrad);
    int nbit = 0;
    if(i%4 == 1 || test > 1.e-1)
      nbit = LS_reinit(dist, it_max = 10, RK2 = 1);
    fprintf(stderr, "## nbit %d %g %g %g\n", nbit, mingrad, 
      maxgrad, test);


    scalar dist_n[];
    cell2node(dist,dist_n);
    fractions (dist_n, cs, fs);
    fractions_cleanup(cs,fs);

    boundary({cs,fs});
    restriction({cs,fs});

    curvature_LS(dist, curve);

    foreach(){
      cs[]      = 1.-cs[];
    }
    foreach_face(){
      fs.x[]      = 1.-fs.x[];
    }

    boundary({cs,fs});
    restriction({cs,fs});

    k_loop = 0;
    foreach(){
      if( (cs0[] != 1. && cs[] ==1.) || (cs0[] == 0. && cs[] !=0.))k_loop = 1;
    }
  }
}


event double_calculation(i++,last){
// modify the fs , cs, copy the outer fields in the partially covered cells

  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face(){
    fs.x[]      = 1.-fs.x[];
  }

  boundary({cs,fs});
  restriction({cs,fs});
}

event movies ( i++,last;i<200)
{
  if(i%2 == 1) {

    boundary({TL,TS});
    restriction({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});
    draw_vof ("cs", "fs");
    squares ("visu", min = -1., max = 1.);
    save ("visu.mp4");
    stats s = statsf(TL);
    stats s2 = statsf(TS);
    // fprintf(stderr, "TL %g %g TS %g %g\n",
      // s.min, s.max, s2.min, s2.max);
  }
  if(i%20==1) {
      output_facets (cs, stdout);
    }
}

/**
~~~gnuplot Evolution of the interface
set size ratio 0.5
f(x) = 0
plot 'out' w l t 'dt = 0.5*Delta', f(x)
~~~
*/