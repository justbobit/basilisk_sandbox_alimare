/**
Pour le 3D imposer un profil d'entrée parabolique.
Recupere les nombres sans dimension de l'expé.
Reynolds, Weber, effet de la gravité

Effet de la thermique
*/


#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "view.h"

#define Pi 3.141519265
#define alpha 15*Pi/180.
#define inletV 1.
#define Radius 0.01
#define yinletcenter 0.04
#define G0 1.

int maxlevel = 8;
double uemax = 0.1;
scalar f0[];

face vector av[];
/**
The inlet condition a only for a small portion of the left boundary, so we
define a tagging function for that
*/ 
void changeref(double x, double y, double * x2, double * y2){
  *x2 = x*cos(alpha) + y*sin(alpha);
  *y2 = -x*sin(alpha) + y*cos(alpha);
}

double mycylinder(double x, double y, double z){
  double x2, y2;
  changeref(x,y,&x2,&y2);
  return sq(Radius) - sq(x2-yinletcenter) - sq(z);
}

u.n[left] = dirichlet (f0[]*inletV*sin(alpha));
u.t[left] = dirichlet (-f0[]*inletV*cos(alpha));
u.t[bottom] = dirichlet(0.);

#if dimension > 2
u.r[left]  = dirichlet(0);
#endif
p[left]    = neumann(0);
f[left]    = f0[];

u.n[right] = neumann(0);
p[right]   = dirichlet(0);

int main (){

  init_grid (128);
  rho1 = 1., rho2 = 1./27.84;
  mu1 = 2.*rho1/200, mu2 = 2.*rho2/200;  

  DT = 0.005;
  a = av;
  run();

}

event init(i=0){
  
  fraction (f0, mycylinder(x,y,z));
  view(tx = -0.5, ty = -0.5);
  f0.refine = f0.prolongation = fraction_refine;
  restriction ({f0}); // for boundary conditions on levels
  foreach(){
    double x2, y2;
    changeref(x,y,&x2,&y2);
    f[] = f0[];
    u.x[] = f[]*sin(alpha);
    u.y[] = -f[]*cos(alpha);
  }
  boundary({f,u.x,u.y});
}

event acceleration(i++){
  foreach_face(x){
    av.x[] = G0*sin(alpha); 
  }
  foreach_face(y){
    av.y[] = -G0*cos(alpha);
  }
  boundary((scalar *){av});
}

event movies(t+=0.05;t<5){
  draw_vof("f");
  squares("f", min = 0, max =1);
  save("f.mp4");
}

event logfile(i+=100){
  fprintf(stderr, "%g\n", t);
}

event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){0.01,uemax,uemax,uemax}, maxlevel);
}

event final_image(t=end){
  dump();
}
