/**
# Reconstruction of a field off an interface


This is a test case for the reconstruction of a field only defined on an
interface off of it. It uses most of the functions related to the use of a
level set.
*/

#include "../../ghigo/src/myembed.h"
#include "../advection_A.h"
#include "curvature.h"

#include "view.h"
#define VOF 1
// #include "../level_set.h"
#include "../alex_functions.h"


#include "../LS_reinit.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../LS_curvature.h"


#define MAXLEVEL 7


scalar dist[];
vector v_pc[];
face vector v_pc_f[];
scalar * level_set = {dist};
scalar * tracers   = {NULL};

scalar * LS_speed  = {v_pc.x,v_pc.y};

int nb_cell_NB;
double  NB_width ;              // length of the NB

/**
The initial interface defined by the level set function is a circle + a cosine
function.
*/

double geometry(double x, double y, double Radius) {

  coord center;
  center.x = 0.5;
  center.y = 0.5;

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = -( sqrt(R2*(1.+0.25*cos(8*theta))) - Radius);

  return s;
}


int main(){
	int N = 1 << MAXLEVEL;
	init_grid(N);
	nb_cell_NB = 1 << 3 ;
	NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  vertex scalar dist_n[];
  foreach_vertex() {
    dist_n[] = clamp(-geometry(x,y,L0/3.),-1.5*NB_width,1.5*NB_width);
  }

  boundary ({dist_n});
  restriction({dist_n});

  fractions (dist_n, cs, fs);

  foreach() {
    dist[] = clamp(-geometry(x,y,L0/3.),-1.5*NB_width,1.5*NB_width);
  }

  boundary ({dist});
  restriction({dist});
  int iloop;
  scalar curve[];

  for(iloop=1; iloop<=500;iloop++){
  /**
  We set the velocity phase change field to be dependant on the curvature : 
  $$
  v_{pc} =  \{ -4.5 -\kappa * n_x, -4.5 -\kappa * n_y \}
  $$
  therefore the equilibrium will be reached once we reach a circle whose
  curvature is -4.5.
   */
    curvature(cs,curve);
    stats s = statsf(curve);
    fprintf(stderr, "%d %g %g %g\n", iloop, s.min, s.max, s.stddev);
    double maxk = max(fabs(s.min), fabs(s.max));
    foreach(){
      if(interfacial(point, cs)){
        coord n = normal (point, cs);
        v_pc.x[] = (-5. -curve[])/(maxk)*n.x*L0/(1 << MAXLEVEL);
        v_pc.y[] = (-5. -curve[])/(maxk)*n.y*L0/(1 << MAXLEVEL);
      }
      else{
        v_pc.x[] = 0.;
        v_pc.y[] = 0.;
      }
    }

    boundary((scalar*){v_pc});

  int k_limit = 0; 
  double deltat  = 0.9*L0 / (1 << MAXLEVEL);  // Delta

  vector v_pc_r[];
  foreach(){
    foreach_dimension(){
      if(interfacial(point, cs))v_pc_r.x[] = v_pc.x[];
      else v_pc_r.x[] = 0.;
    }
  }
  boundary((scalar * ){v_pc_r});
  restriction((scalar * ){v_pc_r});

  int err = 0;
  scalar * speed_recons  = {v_pc_r.x,v_pc_r.y};
  recons_speed(dist, deltat, nb_cell_NB, NB_width, speed_recons,
    k_limit, 1.e-4, &err, overshoot = 0.03);

/**
Advection with the reconstructed speed.

We directly use $v_{pc}$ which is a cell-centered field, we could use a
face-centered field, but my first tries were unsuccessful.
*/   


  face vector v_pc_f[];
  foreach_face(){
    v_pc_f.x[] = face_value (v_pc_r.x, 0);
  }
  boundary ((scalar *){v_pc_f});
  double dt_LS = timestep_LS (v_pc_r, DT);

  advection_LS (level_set, v_pc_f, dt_LS);
  boundary ({dist});
  restriction({dist});

  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    // double coeff[4];
    // dist_n[] = mybilin( point , dist, Stencil, p_interp, coeff);
    dist_n[] = bicubic( point , dist, Stencil, p_interp);
  }
  boundary({dist_n});

  fractions (dist_n, cs, fs);


  LS_reinit(dist,0.5*L0/(1 << MAXLEVEL), 6);

  // if( iloop%10==0){
  	view (fov = 20., tx = -0.5, ty = -0.5);
   draw_vof("cs");
   squares("curve", min =-30, max = 30);
   save ("curve.mp4");

   // draw_vof("cs");
   // squares("dist", min =-NB_width, max = NB_width);
   // save ("dist.mp4");
// 
   draw_vof("cs");
   squares("v_pc_r.x", min =-9.e-3, 
    max = 9.e-3);
   save ("vpcx.mp4");
 // }
 if(iloop%50==0) output_facets (cs, stdout);	
}

dump();
}



/**
![Animation of the level set function](recons_speed/dist.mp4)(loop)

![Animation of the phase change velocity](recons_speed/vpcx.mp4)(loop)

~~~gnuplot Curvature evolution
set title 'Curvature' font 'Helvetica,20'
set key left
f(x) =  -4.5
plot 'log' u 1:2 w l t 'min',  \
     'log' u 1:3 w l  t 'max', \
     f(x) dt 2 t 'equilibrium curvature'

~~~

~~~gnuplot Evolution of the interface
set size ratio -1
plot 'out' w l t ''
~~~

## References

~~~bib

@article{peng_pde-based_1999,
  title = {A {PDE}-Based Fast Local Level Set Method},
  volume = {155},
  issn = {0021-9991},
  url = {http://www.sciencedirect.com/science/article/pii/S0021999199963453},
  doi = {10.1006/jcph.1999.6345},
  pages = {410--438},
  number = {2},
  journaltitle = {Journal of Computational Physics},
  author = {Peng, Danping and Merriman, Barry and Osher, Stanley and Zhao, Hongkai and Kang, Myungjoo},
  urldate = {2019-09-09},
  date = {1999-11}
}
~~~

*/
