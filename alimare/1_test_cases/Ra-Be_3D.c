/**
# Rayleigh-Benard convection with a melting boundary

Test case taken from [Favier et al., 1999](#Favier2019).

Rayleigh is $10^5$.

I should add more comments on this test case.

![Animation of the temperature](Favier_Ra-Be/temperature.mp4)(width=100%)


~~~bib
@Article{Favier2019,
  author        = {B. Favier and J. Purseed and L. Duchemin},
  title         = {Rayleigh–Bénard convection with a melting boundary},
  year          = {2019},
  volume        = {858},
  pages         = {437-473},
  issn          = {0022-1120},
  __markedentry = {[limare:6]},
  doi           = {10.1017/jfm.2018.773},
}
~~~

Formulation of the problem:

$$
 \frac{1}{\sigma} (\frac{\partial \vec{u}}{\partial t} + \vec{u}\cdot \nabla \vec{u}) = - \nabla P + Ra\,\theta\,\vec{e_{y}} + \nabla^{2}\vec{u}
$$

$$
\frac{\partial \theta}{\partial t} + \vec{u}\cdot \nabla \theta=\nabla^{2}\theta
$$

$$
\nabla \cdot \vec{u} = 0
$$

where $\vec{u} = (u, w)$ is the velocity, $\theta = (T-T_{0})/(T_{1}-T_{0})$ is the dimensionless temperature and the pressure $P$ has been made dimensionless according to $P_{0} = \rho \kappa_{T}\nu/H^2$. $Ra$ is the Rayleigh number and $\sigma$ is the Prandtl number defined in the usual way by:
$$
    Ra = \frac{g\alpha_{t} \Delta T H^3}{\nu\kappa_{T}}
$$

$$
\sigma = \frac{\nu}{\kappa_{T}}
$$

The equation to describe the evolution of the interface is defined by:
$$
    St\,\vec{v}\cdot \vec{n} = (\nabla\theta^{(L)} - \nabla\theta^{(S)})\cdot \vec{n}
$$
with the condition:
$$
\theta = \theta_{M}
$$
where $\theta_{M} = (T_{M}-T_{0})/(T_{1}-T_{0})$ is the melting temperature.

Required for using hybrid level-set/embedded boundary method. */

#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846
#define QUADRATIC 1
#define GHIGO 1


#include "grid/octree.h"
#include "../../ghigo/src/myembed.h"
#include "../embed_extrapolate_3.h"

#include "../double_embed-tree.h"

#include "../../ghigo/src/mycentered.h"
#include "tracer.h"
#include "../../ghigo/src/mydiffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "view.h"

#include "../level_set.h"
#include "../LS_advection.h"

/**
Parameters for the simulation.
*/

#define T_eq         0.3    // Theta m in the paper
#define TL_inf       1.     // T1 in the paper
#define TS_inf       0.     // T0 in the paper

#define Ra 100000           // Global Rayleigh number
#define h0 0.1             // Initial height of the interface

#define MAXLEVEL 7         // origin 7
#define MINLEVEL 3

#define ratio 8.            // Ratio of length to width in the domain


/**
Two tracers, one for each temperature field.
*/
scalar TL[];                // Temperature for liquid
scalar * tracers = {TL};   

scalar TS[];                // Temperature for solid
scalar * tracers2 = {TS};

scalar dist[];              // Levelset function
scalar * level_set = {dist};

vector vpc[],vpcf[];

face vector muv[];          
mgstats mgT;
stats s3;

double  latent_heat = 1.;
double  lambda[2];   

// no Gibbs Thomson effect.
double  epsK = 0.000, epsV = 0.000;
double eps4 = 0.;

int k_loop = 0;

scalar curve[];

int     nb_cell_NB;
double  NB_width ;          // length of the NB

mgstats mgd;  

TL[bottom]    = dirichlet(TL_inf);
TL[embed]     = dirichlet(T_eq);

// TS[top]       = dirichlet(TS_inf);
TS[embed]     = dirichlet(T_eq);

u.n[embed]    = dirichlet(0.);
u.t[embed]    = dirichlet(0.);
u.r[embed]    = dirichlet(0.);


double nu = 5.;             // Viscocity == Prandtl
face vector muc[], av[];

double DT2;

/**

We consider the following set of initial conditions, which only depends on the vertical coordinate $z$:

$$
 \theta (t = 0) = \left\{
\begin{aligned}
1+(\theta_{M}-1)y/h_{0}  \,\,\,\,\,\,\, if \,y \le h_{0}\\
\theta_{M}(y-1)/(h_{0}-1) \,\,\,\,\,\,\,  if \,y > h_{0}
\end{aligned}
\right.
$$

*/


double TLfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y>H0)
    return Thetam;
  else
    return 1.+(Thetam-1.)*y/H0;
}

/**
Function to initialize TS
*/

double TSfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y > H0)
    return Thetam*(y-1.)/(H0-1.);
  else
    return Thetam;
}

/**
Function to calculate the kinetic energy density
*/

double energy_density(){
  double ke = 0.,se =0.;
  foreach(reduction(+:ke) reduction (+:se)){
    ke+=dv()*sq(u.x[])+sq(u.y[]);
    se+=dv();
  }
  return ke/se;
}

#include "../alex_functions2.h"

/**
Function to calculate the average height of the interface
*/

double output_height (scalar c, face vector s){
  if (!s.x.i) s.x.i = -1;
  double hsum = 0.;
  double xsum = 0.;
  foreach(reduction(+:hsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord v[12];
      int m = facets (n, alpha1, v, 1.);
      for (int i = 0; i < m; i++)
        hsum += y + v[i].y*Delta;
    }
  foreach(reduction(+:xsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord v[12];
      int m = facets (n, alpha1, v, 1.);
      for (int i = 0; i < m; i++)
        xsum += 1.;
    }
  return hsum/xsum+ratio/2.;
}

/**

*/

int main() {
  L0 = ratio;  
  origin (-L0/2,-L0/2,-L0/2);
  int N = 1 << MAXLEVEL;
  init_grid (N);
/**
We use mask() to create rectangle domain
*/
  mask(y > 1.-ratio/2. ? top : none); // 
  mu = muc;
  a = av;

  TL.third = true;
  TS.third = true;

  run();
}

event init (t = 0) {
  DT2 = 0.1*(L0)/( 1 << MAXLEVEL);
  periodic(right);
  periodic(front);
  nb_cell_NB = 1 << 2 ;  
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  foreach()
    dist[] = h0-ratio/2.-y;

  boundary ({dist});
  restriction({dist});
  LS_reinit(dist);
  vertex scalar dist_n[];
  cell2node(dist,dist_n);
  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});
  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    TL[] = TLfield(x,y,T_eq,h0);
    TS[] = TSfield(x,y,T_eq,h0);
  }
  boundary({TL,TS});
  restriction({TL,TS});

    // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
  uf.r[embed] = dirichlet (0.);
}

event stability (i++) {
/**
Sometimes, because the embedded boundary is moving, some cells have uf.x[] != 0.
&& fm.x[] == 0. Therefore, we do an ugly quickfix for cell that are incoherent
with the embedded boundary stability conditions.
*/
  foreach_face()
    if(fm.x[]==0. && uf.x[] !=0.) uf.x[] = 0.;
  boundary((scalar *){uf});
}



event stability(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
  dist,latent_heat,cs,fs,TS,TL,T_eq,
  vpc,vpcf,lambda1,lambda2,
  epsK,epsV,eps4,deltat=0.6*L0/(1<<MAXLEVEL),
  itrecons = 20,tolrecons = 1.e-2,
  NB_width
  );
  dtmax = timestep_LS(vpcf,DT2,dist,NB_width);
}

event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event vof(i++,last){  
  advection_LS(
  dist,
  cs,fs,
  TS,TL,
  vpcf,
  itredist = 5,
  s_clean = 1.e-10,
  NB_width,
  curve);
}

event tracer_diffusion (i++) {
  boundary({TL});
  event("properties");
  mgd = diffusion (TL,dt,muc,theta = cm);
  writefile(mgd);
  invertcs(cs,fs);
  event("properties");
  boundary({TS});
  mgd = diffusion (TS,dt,muc,theta = cm);
  writefile(mgd);
  invertcs(cs,fs);
  event("properties");
}


/**
Boussinesq term.
*/
event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;
  foreach_face(z)
    av.z[] = 0;
  foreach_face(y)
    av.y[] = cs[]*Ra*(TL[]+TL[-1])/2.;
  boundary((scalar *) {av});

}


/**
FIXME :
Still some issues with mesh adaptation, especially an assert() in
refine_embed_linear().
*/
#if 1 
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[] = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  adapt_wavelet ({cs,visu,u,vpc},
    (double[]){0.005,0.005,0.005,0.005,0.005,0.01,0.01,0.01},MAXLEVEL, MINLEVEL); // cs, visu, u.x,
  // u.y
  foreach(reduction(+:n)){
    n++;
  }
}
#endif

event movie1 (t+=0.005,last){
  // fprintf(stderr, "t = %g\n",t );
  view(quat = {0.,0.,0.,1.});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof("cs","fs");
  squares("visu", min = 0. , max = 1.);
  save("temperature1.mp4");

  view(quat = {-0.707, 0.000, 0.000, 0.707});

  squares("visu", min = 0. , max = 1.,alpha = output_height(cs,fs), n = {0,1,0});
  save("temperature2.mp4");
  view(quat = {-0.155, 0.339, 0.050, 0.927},ty = 0.36);
  draw_vof("cs","fs");
  save("vof.mp4");
}

event movie2 (t+=0.005,last; t<1.6){
  scalar omega[];
  view(quat = {-0.707, 0.000, 0.000, 0.707});
  vorticity (u, omega);
  squares("omega",linear = true,alpha = output_height(cs,fs), n = {0,1,0});
  save("velocity.mp4");
}

event myoutput(t+=0.05){
  char name[80];
  sprintf (name, "dump_%g", t);
  dump(name);
}

/**
We create an event to output effective Rayleigh number, everge height of interface and kinetic energy density 
**/


event Calculation(i++,last){
  double hbar = output_height(cs,fs);
  double Rae = Ra*(1-T_eq)*pow(hbar,3);
  fprintf(stdout, "%g %g %g %g \n", t, Rae, energy_density(), hbar);
  fprintf(stderr, "t = %g \n", t);
}
