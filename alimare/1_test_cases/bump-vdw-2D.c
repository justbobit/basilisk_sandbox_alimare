/**

# 2D Test code + mesh adaptation

![Output height using the phase field method](bump-vdw-2D/h.mp4)

*/
#define growthTimestep 0.1
#define MU 1e-3
#define LAMBDA 1.e-2
#include "../vdw.h"
// #include "saint-venant.h"
#include "view.h"

int LEVEL = 7;

#define rho_c 1
#define R_g 1 
#define theta 0.95
#define p_c 1


double P0(double x)
{
  double rhop;
  rhop=x/rho_c;
  return p_c*rhop*theta*(8/(3-rhop) - 3*rhop/theta);
}


int main (int argc, char * argv[])
{
  origin (-0.5, -0.5);
  init_grid (1 << LEVEL);
  DT = 1e-3;
  run();
}

event init (i = 0)
{
  mgu.nrelax = 10;
  lambda=LAMBDA;
  TOLERANCE = 1.e-6;
  foreach()
    {
      foreach_dimension()
        mu.x[] = MU;
      rho[] = 0.1 + exp(-200.*(sq(y)+sq(x)));
      q.x[] = q.y[] = 0.;
    }
}

event logfile (i++) {
  stats s = statsf (rho);
  if((s.min)<1.e-3){
    fprintf (ferr, "%4.2g %5d %4.2g %4.2g %.5f\n", t, i, s.min, s.max, s.sum);
    dump();
    exit(1);
  }
}

event adapt(i++;t<=0.8){
  adapt_wavelet ({rho,u.x,u.y},
    (double[]){1.e-3,1.e-2,1.e-2},7, 5);
}

event logfiles(t+=0.01){
  fprintf(stderr, "%g %ld %6.4e\n", t, grid->tn, dt);
}

event movie(t+=5.e-3){
  // cells();
  squares("rho", min = 0.01, max = 0.35);
  save("rho.mp4");
}

