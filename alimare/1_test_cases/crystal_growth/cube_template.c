/**
#Solidification of an undercooled liquid

We want to study the dendritic growth of a solid in an undercooled liquid. We
simulate the diffusion of two tracers separated by an embedded
boundary. The interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
$$

where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = -1$. The ice particle is initially
at $T_S = 0$.

The temperature on the interface is derived from the Gibbs-Thomson relation:
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$

~~~gnuplot Evolution of the interface (zoom)
K  = "`head -1 log | awk '{print $1}'`"
K2 = "`head -1 log | awk '{print $2}'`"
K  = K + 0
K2 = K2 + 0
graph(n,n2)  = sprintf("epsK = %.1e , epsV = %.1e",n,n2)
set key top right
set size ratio -1
set xrange [-0.55:0.55]
set yrange [-0.55:0.55]
unset xlabel
unset xtics
unset ytics
unset border
unset margin
unset ylabel
plot 'out' w l lw 3 t graph(K,K2) 
~~~

*/
#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 1
#define Pi 3.14159265358979323846
#define QUADRATIC 1
#define GHIGO 1

#if GHIGO
#include "../../../ghigo/src/myembed.h"
#else
#include "embed.h"
#endif

#include "../../double_embed-tree.h"

#include "../../advection_A.h"
#if GHIGO
#include "../../../ghigo/src/mydiffusion.h"
#else
#include "diffusion.h"
#endif

#include "fractions.h"
#include "curvature.h"

#include "../../level_set.h"
#include "../../LS_advection.h"
#include "view.h"

#define T_eq         0.
#define TL_inf       -1.
#define TS_inf       -1.

/**
Setup of the numerical parameters
*/
int MAXLEVEL = 7; 
int MINLEVEL = 5;
double H0;

/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[], dist[];
vector vpc[],vpcf[];

scalar * tracers    = {TL};
scalar * tracers2 = {TS};

scalar * level_set  = {dist};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];
double DT2;


double  latent_heat = 2.;
double  lambda[2]; // thermal capacity of each material
#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
#ifdef EPSK
double epsK = EPSK;
#else
double epsK = 1.e-4;
#endif
#ifdef EPSV
double epsV = EPSV;
#else
double epsV = 1.e-4;
#endif

#else // Gibbs_Thomson
double  epsK = 0.000, epsV = 0.000;
#endif // Gibbs_Thomson

/**
We take into account an [anisotropic](../phase_change_velocity.h) effect.
We define 2 functions to take into account the anisotropy in the change velocity
where we set the following condition :
$$
\epsilon = \overline{\epsilon} \left( 1. - \alpha \cos(n \theta +
\theta_0)\right)
$$
where $\theta$ is the angle between the interface and the x-axis, $\alpha =0.5$
and $\theta_0 = 0$ are hardcoded. This will be used in the Gibbs-Thomson
formulation.
*/
#define GT_aniso 0
#if GT_aniso
int aniso = 4;
#else
int aniso = 1;
#endif

int nb_cell_NB;
double  NB_width ;    // length of the NB

scalar curve[];


#define Pi 3.14159265358979323846


/**
The initial geometry is a crystal seed:

$$
r\left(1+ - 0.25 *cos(4\theta) \right) - R
$$
*/

double geometry(double x, double y, coord center, double Radius) {

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = ( sqrt(R2)*(1.-0.3*cos(4*theta)) - Radius);

  return s;
}

double TL_init(double val,double V, double t){
  if(val>V*t)
    return -1+exp(-V*(val-V*t));
  else
    return 0.;
}

#include "../../alex_functions2.h"

/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {
  TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));
  TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, aniso));

  TL[top]    = dirichlet(TL_inf); 
  TL[left]   = dirichlet(TL_inf); 
  TL[bottom] = dirichlet(TL_inf); 
  TL[right]  = dirichlet(TL_inf); 

  TS[top]    = dirichlet(TS_inf); 
  TS[bottom] = dirichlet(TS_inf); 
  TS[left]   = dirichlet(TS_inf); 
  TS[right]  = dirichlet(TS_inf); 

  origin(-L0/2., -L0/2.);
  init_grid (1 << MAXLEVEL);
  fprintf(stderr, "%g %g\n", epsK, epsV);
  run();
}


event init(t=0){

  DT2         = 5.*sq(L0/(1<<MAXLEVEL));  // Delta
  nb_cell_NB = 1 << 3 ; // number of cell in the 
                        // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 0.1;
  lambda[1] = 0.1;

  coord center1 = {0.,0.};
  // coord center1 = {-L0/20.,L0/20.};
  // coord center2 = {L0/20.,-L0/20.};

/**
And because, why not, we put two crystal seeds, to see them compete during
crystal growth.
*/
  double size = L0/14.99;

  foreach() {
    dist[] = geometry(x,y,center1,size);
  }

  boundary ({dist});
  restriction({dist});

  vertex scalar distn[];
  cell2node(dist,distn);

  fractions (distn, cs, fs);

  boundary({cs,fs});
  restriction({cs,fs});

  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc});

  curvature(cs,curve);
  boundary({curve});

  foreach() {
    TL[] = TL_init(dist[], 60, 0.);
    TS[] = 0.;
  }
  boundary({TL,TS});
  restriction({TL,TS});

  myprop(muv,fs,lambda[0]);
#if GHIGO // mandatory for GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif
}

event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
  dist,latent_heat,cs,fs,TS,TL,T_eq,
  vpc,vpcf,lambda1,lambda2,
  epsK,epsV,aniso,deltat=0.45*L0/(1<<MAXLEVEL),
  itrecons = 60,tolrecons = 1.e-12
  );
  double DT3 = timestep_LS(vpcf,DT2,dist,NB_width);
  tnext = t+DT3;
  dt = DT3;
  // fprintf(stderr, "## %g %g %g\n", t, DT2, DT3);
}

event tracer_diffusion(i++){
  boundary({TL});
  myprop(muv,fs,lambda[0]); // MANDATORY, the interface has moved !!!!!
  mgT = diffusion (TL,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  boundary({TS});
  mgT = diffusion (TS,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
}

/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  double lambda1 = lambda[0], lambda2 = lambda[1];
  advection_LS(
  dist,
  latent_heat,
  cs,fs,
  TS,TL,
  T_eq,
  vpc,vpcf,
  lambda1,lambda2,
  epsK,epsV,aniso,
  curve,
  &k_loop,
  deltat = 0.45*L0 / (1 << grid->maxdepth),
  itredist = 10,
  tolredist = 3.e-3,
  itrecons = 60,
  tolrecons = 1.e-12,
  s_clean = 1.e-10,
  NB_width);

  foreach_face(){
    uf.x[] = 0.;
  }
  boundary((scalar *){uf});
  restriction((scalar *){uf});  
}

event interface2(t+=0.025,last){
  output_facets (cs, stdout);
}

event movies (t+=0.0025; t<1.)
{
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  stats s3 = statsf(visu);
  fprintf(stderr, "#temperature %g %g\n",  s3.min, s3.max);  
  view (width = 800, height = 800);
// 
  char filename [100];
  sprintf(filename,"temperature_epsK%g_epsV%g_aniso%d.mp4",epsK,epsV,aniso);

  draw_vof("cs");
  squares("visu", min = -1. , max = 0.01);
  save (filename);
}

/**
Mesh adaptation. still some problems with coarsening inside the embedded
boundary.
*/
#if 1
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[]      = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  stats s2 = statsf(curve);
  fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});

  adapt_wavelet ({cs,visu},
    (double[]){1.e-3,1.e-4},MAXLEVEL, MINLEVEL);
  foreach(reduction(+:n)){
    n++;
  }
  fprintf(stderr, "##nb cells %d\n", n);
}
#endif
/**



*/

