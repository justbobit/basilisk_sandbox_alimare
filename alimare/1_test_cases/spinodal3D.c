/**
# 3D-Spinodal decomposition with a phase field method

 */
#define growthTimestep 0.1
#define MU 1e-5
#include "grid/octree.h"
#include "../vdw3D.h"
#include "view.h"

/**



![Filled isoline of the density during simulation + norm of the velocity](spinodal/rho.mp4)

*/

#define LEVEL 7

#define LAMBDA 8.e-2
#define rho_c 1.06344
#define R_g 1 
#define theta 0.9
#define p_c 1

double P0(double x)
{
  double rhop;
  rhop=x/rho_c;
  return p_c*rhop*theta*(8/(3-rhop) - 3*rhop/theta);
  // return 0.5*sq(x);
}


  
int main()
{
  origin (-0.5, -0.5,-0.5);
  periodic(right);
  periodic(top);
  periodic(back);
  init_grid (1 << LEVEL);
  DT = 2e-4;
  run();
}

event init (i = 0)
{
  lambda=LAMBDA;
  foreach(){
    foreach_dimension()
        mu.x[] = MU;
      rho[] = rho_c *( 1.+0.2*noise());
      q.x[] = q.y[] = q.z[] = 0.;
  }
  boundary({mu,rho,q});
}

event outputfile (t=1.e-2;t*=1.01) {
  face vector fs[];
  view(quat = {-0.168, -0.373, -0.071, 0.910});
  isosurface ("rho", rho_c);
  squares("rho", map = cool_warm);
  squares("rho", map = cool_warm, n = {0,1,0});
  squares("rho", map = cool_warm,n = {1,0,0});
  save("rho.mp4");
  stats s = statsf (rho);
  stats s2 = statsf(u.x);
  fprintf (stderr, "%6.4g %4.2e    %6.3g %6.2g %4.2g %4.2g %ld\n", t,  dt,
    s.min,s.max,s2.min,s2.max, grid->tn);

}

event adapt(i++){
  adapt_wavelet ({rho,u.x,u.y,u.z},
    (double[]){6.5e-3,1.5e-2,1.5e-2,1.5e-2},LEVEL, 5);
}


event end (t=5)
  dump();