/**
# Distance field computation from a 3D model

The goal is to build a distance field representation of a 3D
[CAD](https://en.wikipedia.org/wiki/Computer-aided_design) model. */

#include "grid/octree.h"
#include "utils.h"
#include "distance.h"
#include "fractions.h"
#include "view.h"
#include "../thinning.h"
int n_part = 0;
#include "../../Antoonvh/scatter.h"


int main()
{
  coord * p = input_stl (fopen ("../snake.stl", "r"));
  // taken from https://cults3d.com/en/3d-model/game/snake-cornsnake
  coord min, max;
  bounding_box (p, &min, &max);  
  double maxl = -HUGE;
  foreach_dimension()
    if (max.x - min.x > maxl)
      maxl = max.x - min.x;
  
  init_grid (64);
  size (1.2*maxl);
  origin ((max.x + min.x)/2. - L0/2,
      (max.y + min.y)/2. - L0/2,
      (max.z + min.z)/2. - L0/2);

  /**
  We initialize the distance field on the coarse initial mesh and
  refine it adaptively until the threshold error (on distance) is
  reached. */

  scalar d[];
  distance (d, p);
  while (adapt_wavelet ({d}, (double[]){5e-4*L0}, 10,5).nf);

  /**
  We display an isosurface of the distance function coloured with the
  level of refinement. */

  view (fov = 7.29342, quat = {0.493796,-0.218547,-0.324026,0.776793}, tx = 0.0495556, ty = 0.038888, 
    width = 800, height = 800, bg = {1,1,1});
  isosurface ("d", 0, color = "level", min = 5, max = 11);
  save ("snake.png");

  /**
  We also compute the volume and surface fractions from the distance
  field. We first construct a vertex field interpolated from the
  centered field and then call the appropriate VOF functions. */

  vertex scalar phi[];
  foreach_vertex()
    phi[] = (d[] + d[-1] + d[0,-1] + d[-1,-1] +
         d[0,0,-1] + d[-1,0,-1] + d[0,-1,-1] + d[-1,-1,-1])/8.;
  boundary ({phi});
  scalar f[];
  face vector s[];
  fractions (phi, f, s);
  
  /**
  Finally we display the surface reconstructed from volume fractions. */

  clear();
  draw_vof ("f", "s", edges = true, lw = 0.5);
  save ("vof.png");

  scalar c[];
  foreach(){
    if(f[])c[] =1;
    else c[] = 0;
  }
  boundary({c});
  thinning3D(c);

  Cache skeleton = {0}; // will be used later on

  // create cache
  foreach(){
    if(c[]){
      cache_append (&skeleton, point, 0);
      n_part++;
    }
  }

  // init part in cache
  coord * loc = malloc (n_part*sizeof(coord));
  int n = 0;
  foreach_cache(skeleton) {
    coord cc = {x, y, z};
    foreach_dimension()
      loc[n].x = cc.x;
    n++;
  }
  // draw parts
  scatter(loc);
  save("medial_axis.png");
  dump();
  free (loc);

}

/**
Note that the "tail" of the horse is an artefact due to an
inconsistency in the surface mesh, which is self-intersecting near
this point.

![Isosurface of the distance function coloured with level of refinement.](skeleton_snake/snake.png)

![Reconstructed VOF surface.](skeleton_snake/vof.png)

## See also

* [Computation of a levelset field from a contour](/src/test/basilisk.c)
*/
