/*
 * =====================================================================================
 *
 *		Filename: 		/
 *
 *		Description: 	/
 *		Source : 		/
 *					
 *
 *		Version:  		/
 *		Created:  		/
 *		Updated:  		/
 *		Compiler:  		gcc
 *
 *		Author:  		Maël DESSEAUX, mael.desseaux@ariane.group
 *		Company:  		ArianeGroup
 *
 * =====================================================================================
 */

/*==================== Includes ====================*/


#define FILTERED 1 // SMEARING OF THE INTERFACE

#include "embed.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "view.h"
// #include "axi.h"
// #include "tecplot.h" 
#include "contact.h"
#include "../../../wiki/sandbox/popinet/contact/contact-embed.h"
#include "../basic_geom.h"
#include "tag.h"


#include "view.h"

#include <stdio.h>
#include <time.h>


/*===================== Données ====================*/


#define MAXLEVEL 11		// Level max maillage adaptatif
#define MINLEVEL 6		// Level min maillage adaptatif, pas obligatoire de le mettre
double tEnd = 15.;  	// t théorique : t = 5e-3 --> t adim : t = 3.87

//		/!\ VOIR L'ADIMENTIONNEMENT  POUR COMPRENDRE LES VALEURS /!\		//
//		/!\ LES VALEURS NE SONT PAS ARBITRAIRES NI DIMENSIONNEES /!\		//

//On prend :
#define rho_l 	1.		// Masse volumique de l'eau
#define rho_g 	1.e-3	// Masse volumique de l'air
#define SIGMA 	1.  	// Tension de surface
#define h 	 	1. 		// Hauteur de la marche


double sigma_vrai = 75.e-3; // tension de surface de l'eau
double h_vrai = 0.5e-3;
//Nombre adimentionnés
#define	Bo		1./30.	// Nombre de Bond 		--> Accélération  : a 	 	= Bo
#define	Oh_l	5.e-3	// Nombre d' Ohnesorge 	--> Viscosité eau : mu_l 	= Oh_l = 1/sqrt(La_l)

// #define	Oh_g	5.e-5	// Nombre d' Ohnesorge 	--> Viscosité air :	mu_g 	= Oh_g = 1/sqrt(La_g)
#define We		2./3.e3	// Nombre de Weber 		--> Vitesse 	  : Vin 	= sqrt(We)

//On obtient :
#define L		80.		// Longueur du domaine
#define H 		10.		// Hauteur du domaine

double	Vin	  = 5.	;	// Vitesse d'entrée  (problème quand on utilise sqrt(We), trop de chiffre)
double theta0 = 45.;
vector h2[];
h2.t[bottom] = contact_angle(theta0*pi/180.);

/*=================== Cond. Bord. ==================*/


u.n[left]	= dirichlet(Vin);
u.n[bottom] = dirichlet(0);

u.t[bottom] = dirichlet(0);

uf.t[left]	= dirichlet(0);
uf.n[bottom]= dirichlet(0);

f[left]  	= 1.0;

p[right]   	= dirichlet(0);

pf[right]	= dirichlet(0);

double vol_cut;

/*======================= Main =====================*/
int main() {
 	int time = 0;		// Calcul du temps de calcul

	size (L);			// Taille du domaine
	origin(-L/2,-L/2.);		// Origine
	init_grid (256);	// Grille initial

	rho1= rho_l	;		// Masses volumiques
	rho2= rho_g	;

	double Oh_g = 1.8e-5 / sqrt(1e3 * sigma_vrai * h_vrai);

	double 	mu_l  = Oh_l;	// Viscosité de l'eau
	double	mu_g  = Oh_g;	// Viscosité de l'air

	mu1	= mu_l	; 		// Viscosités
	mu2	= mu_g	; 

	f.sigma	= SIGMA;	// Tension de surface

	f.height = h2 ; // fonction de hauteur

	fprintf(stderr, "## valeur ohnesorge %g\n", Oh_g);

	//NITERMAX = 100;
	//NITERMIN = 1;
	//TOLERANCE = 1e-8;
	//DT = 1.e-4;
	CFL = 0.5;

	const scalar c[] = theta0*pi/180.;
  contact_angle = c;

  vol_cut = 1.*L0*L0/pow(4,MAXLEVEL);
	run();

	time = clock();		// Calcul du temps de calcul
	printf("Temps d'execution = %g secondes", time/1000000.);	//Affichage du temps de calcul
}


/*======================= Init =====================*/


event init (t = 0) {
	CFL = 0.5;
	
	// mask (y < -L/2. + h && x > 0. && x < h ? bottom : y > -5 ? top : none);		//Mask de la marche

  coord center_rectangle, size_rectangle;
  center_rectangle.y = -L0/2.;

  size_rectangle.x = h;
  size_rectangle.y = 2*h; 

	vertex scalar distn[];
	foreach_vertex() {
    distn[] = rectangle (x, y, center_rectangle, size_rectangle);
  }
  boundary({distn});
  fractions(distn,cs,fs);

	scalar phi[];			//Equation de l'interface
	foreach_vertex(){
		phi[]=(-L0/2. + L/40 -x);
	}
	fractions(phi,f);		//Interface
	view(fov = 6, width = 1600, height = 500, ty = 0.35);
	draw_vof ("cs", "fs", filled = -1);
  squares("f");
	mask ( y > -L/4. ? top : none);
  save("cs.png");
  // exit(1);
}


event acceleration (i++) {	//Gravité
	face vector av = a;
	foreach_face(x)
    	av.x[] = Bo;		//a = Bo             question : -= ou += ou =
}



/*=============== MAillage Adaptatif ===============*/


event adapt (i++) { //Maillage adaptatif sur f
	adapt_wavelet ({f,u}, (double[]){1.e-2,8.e-3,8.e-3}, MAXLEVEL, MINLEVEL);
	
	coord center_rectangle, size_rectangle;
  center_rectangle.y = -L0/2.;

  size_rectangle.x = h;
  size_rectangle.y = 2*h; 
	vertex scalar distn[];
	foreach_vertex() {
    distn[] = rectangle (x, y, center_rectangle, size_rectangle);
  }
  boundary({distn});
  fractions(distn,cs,fs);


}


event movies ( t+=2.5e-2,last;t<tEnd){
	view(fov = 6, width = 1600, height = 500, ty = 0.35);
	draw_vof("f");
	draw_vof ("cs", "fs", filled = -1);
  squares("u.x", min = 4., max = 8., map=cool_warm);
  save("ux.mp4");

}

event remove_drops ( i+=10 )
{
  scalar m[];
  foreach()
    m[] = f[] > 0.;
  int n = tag (m);
  double v[n];
  int remove_flag[n];
  for (int j = 0; j < n; j++) {
    v[j] = 0.;
    remove_flag[j] = 0;
  }
  foreach(serial)
    if (m[] > 0) {
      int j = m[] - 1;
      v[j] += dv()*f[];
    }

#if _MPI
  MPI_Allreduce (MPI_IN_PLACE, v, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

  for (int j = 0; j < n; j++)
    if ( v[j] < vol_cut )
       remove_flag[j] = 1;

  foreach()
    if (m[] > 0) {
      int j = m[] - 1;
      if ( remove_flag[j] == 1 ){
          f[]=0.;
          fprintf(stderr,"## small drop removed\n");
        }
    }
}

event mylog(i++){
	stats s = statsf (u.x);
  	stats s2 = statsf (u.y);
  	fprintf (stderr, "%g %g %ld %g %g %g %g\n", \
  	t, dt , grid->tn, s.min, s.max, s2.min, s2.max);
}

/*======================= Fin ======================*/


event end (t = tEnd) 
{return 1;}
