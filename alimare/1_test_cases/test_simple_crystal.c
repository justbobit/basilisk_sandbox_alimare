/**
#Melt of a solid particle

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = 1$. The ice particle is initially
at $T_S = -1$.

The temperature on the interface is derived from the Gibbs-Thomson relation,
withtout any anisotropic effect :
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$

![Animation of the temperature field](crystal/temperature.mp4)(loop)
*/


#define GHIGO 1
#define QUADRATIC 1
#define DOUBLE_EMBED 1
#define CURVE_LS 1
#if GHIGO 
#include "../../ghigo/src/myembed.h"
#include "../../ghigo/src/mydiffusion.h"

#include "../double_embed-tree.h"
#include "../embed_extrapolate_3.h"
#else
#include "embed.h"
#include "diffusion.h"
#endif
#include "advection.h"
#include "tracer.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_advection.h"
scalar curve[];
#include "view.h"

#define T_eq         0.
#define TL_inf       1.
#define TS_inf       0.


/**
Setup of the numerical parameters
*/
int MAXLEVEL, MINLEVEL;
double  H0;


/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[],dist[];
vector vpc[], vpcf[];
face vector muv[];

scalar * tracers = {TL};
scalar * tracers2 = {TS};
scalar * level_set = {dist};
double DT2;
double  latent_heat = 1.;
double  lambda[2];    // thermal capacity of each material
double  epsK = 0.005, epsV = 0.001;
double eps4;
int     nb_cell_NB;
double  NB_width ;    // length of the NB

double s_clean = 1.e-10; // used for fraction cleaning

void myprop(face vector muv, face vector fs, 
  double lambda){
  foreach_face()
    muv.x[] = lambda*fs.x[];
  boundary((scalar *) {muv});
}

TL[top]    = dirichlet(TL_inf); 
TL[bottom] = dirichlet(TL_inf); 
TL[left]   = dirichlet(TL_inf); 
TL[right]  = dirichlet(TL_inf); 

/**
Initial geometry definition. Here the interface equation is :

$$
r\left(1+ 0.2 *cos(8\theta) \right) - R
$$
where $r = \sqrt{x^2 + y^2}$, $\theta = \arctan(x,y)$ and $R = \frac{L_0}{5}$

Notice that the initial dist[] field is not really a distance, it is modified
after a few iterations of the LS_reinit() function.
*/
double geometry(double x, double y, double Radius) {

  coord center;
  center.x = 0.5;
  center.y = 0.5;

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = -( sqrt(R2)*(1.+0.2*cos(6*theta)) - Radius);
  // double s = -( sqrt(R2)*(1.+0.*cos(6*theta)) - Radius);


  return s;
}

/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {
  CFL = 0.5;
  MAXLEVEL  = 7;
  MINLEVEL = 4;
  int N     = 1 << MAXLEVEL;
  init_grid (N);
  TOLERANCE = 1.e-6;
  NITERMIN = 4;
  TL.third = true;
  TS.third = true;
  run();
}


event init(t=0){
  DT2        = sq(L0/(1<<MAXLEVEL));  // Delta

  nb_cell_NB = 6 ;               // number of cell in the 
                                      // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;

  foreach() {
    dist[] = -geometry(x,y,L0/4.);
  }

  boundary ({dist});
  restriction({dist});

  vertex scalar distn[];
  cell2node(dist,distn);

  fractions (distn, cs, fs);
  fractions_cleanup(cs,fs,smin = s_clean);
  boundary({cs,fs});
  restriction({cs,fs});

#if GHIGO
  foreach(){
    csm1[] = cs[];
  }
  boundary({csm1});
#endif //GHIGO
  
  foreach() {
    TL[] = TL_inf;
    TS[] = TS_inf;
  }
  boundary({TL,TS});
  restriction({TL,TS});
#if CURVE_LS
  curvature_LS(dist,curve);
#else
  curvature(cs,curve);
#endif
  boundary({curve});
  TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, eps4));
  TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, vpc, curve, fs, cs, eps4));
#if GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif
}


event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
  dist,latent_heat,cs,fs,TS,TL,T_eq,
  vpc,vpcf,lambda1,lambda2,
  epsK,epsV,eps4,deltat=0.45*L0/(1<<MAXLEVEL),
  itrecons = 60,tolrecons = 1.e-5, NB_width
  );
  DT = 0.4*timestep_LS(vpcf,DT2,dist,NB_width);
}

/*event tracer_advection(i++){
  
}*/

event tracer_diffusion (i++,t < 2.) {
  advection_LS(dist,cs,fs,TS,TL,vpcf,itredist=10,
    s_clean,NB_width,curve);
  myprop(muv,fs,lambda[0]);
  diffusion (TL,dt,muv,theta = cm);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  diffusion (TS,dt,muv,theta = cm);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
  fprintf(stderr, "%g %g\n", dt, DT);
}

event output(i+=30, last){
  output_facets(cs,stdout);
}

event final(t=2.)
  fprintf(stderr, "FIN\n");
