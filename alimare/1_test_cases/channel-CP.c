/**
# Coughmachine:

We simulate the atomization of a liquid sheet due to a gas stream
in a solid channel of finite length.
*/

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "reduced.h"
#include "tag.h"
#include "maxruntime.h"
#include "view.h"
#include "../thinning.h"

double tmax   = 0.025;
double dtSnap   = 2.e-4;

// water/glycerol mix, vol fraction 1/0.62 gives a fluid similar to reference
//http://www.met.reading.ac.uk/~sws04cdw/viscosity_calc.html
//new values from Pallav experiments
double RHOL  = 1.1e3;  // kg/m^3
double RHOG  = 1.15;  //  kg/m^3
double MUL  = 5.5e-3;  // kg/s
double MUG  = 1.78e-5;  // kg/s
double SIGMA  = 6.5e-2;  // N/m

int MAXLEVEL  = 12;
double uemax  = 0.1;

double UIN    = 15;   // m/s velocity
double ramp   = 1.0;   // UIN weight
double dtRamp = 1e-3;   // ramping interval

/**
Thinning
*/



/**
## Geometry
The tube has a rectangular cross-section (l x L) and a given *length*.
The liquid film covers all the length but the first 5 mm.
*/
double ht     = 1e-3 ;   // m   liquid film thickness
double L      = 2e-2 ;   // m   length on z
double l      = 1e-2 ;   // m   length on y
double length = 2.e-1 ;   // m   length on x
int baseLev = 6;
scalar solid[];

/**
## Brief description of boundaries:
The left patch is the inlet. The velocity is 0 all over the patch, except within
the tube; there, the velocity is uniform in space, evolving in time linearly
from 0 to UIN (in a dtRamp time-lapse).

The right patch of the cubic domain have an outlet boundary condition.
All the other patches employ the default symmetry boundary condition.
*/
u.n[left] = ((fabsf(y - ht/2) < (l/2 - ht/2))&&(fabsf(z) < L/2)) ? dirichlet(UIN*ramp) : dirichlet(0);
u.t[left] = ((fabsf(y) < (1.4*l)/2)&&(fabsf(z) < (1.2*L)/2) ? dirichlet(0) : neumann(0));
#if dimension > 2
u.r[left]  = ((fabsf(y) < (1.4*l)/2)&&(fabsf(z) < (1.2*L)/2) ? dirichlet(0) : neumann(0));
#endif

p[left]   = neumann(0);
pf[left]  = neumann(0);
f[left]   = 0; 

u.n[right] = neumann(0);
u.t[right] = neumann(0);
#if dimension > 2
u.r[right] = neumann(0);
#endif
p[right]   = dirichlet(0);
f[right]   = 0; 


/**
The executable will admit 4 parameters: MAXLEVEL, UIN, ht and length.
Optionally, you can add max clock time with the -m flag (02:00:00 will save a
restart at 2 hours and  finish the simulation) Just run:

`mpirun -n $NPROCS run -m <clockwall> <MAXLEVEL> <UIN> <ht> <length>`

*/

int main (int argc, char * argv[])
{
  skeleton.restriction  =  myrestrict;
  skeleton.prolongation =  myprolongation;

  maxruntime (&argc, argv);
  
  if (argc > 1)
    MAXLEVEL = atoi (argv[1]);
  if (argc > 2)
    UIN = atof (argv[2]);
  if (argc > 3)
    ht = atof (argv[3]);
  if (argc > 4)
    length = atof (argv[4]);
  if (argc > 5)
    layers = atof (argv[5]);

  solid.refine = solid.prolongation = fraction_refine;

  uemax = UIN*0.05; //velocity tolerance is fixed as 5% of maximum inlet value
  L0 = length;
  TOLERANCE=1e-3; // Poisson solver tolerance might be modified

  tmax = max(2*L0/UIN, 0.025);
#if dimension == 2
  origin (0., -L0/2.);
#else
  origin (0., -L0/2., -L0/2.);
#endif

  init_grid(1<<baseLev);

  rho2 = RHOG, rho1 = RHOL;
  mu2 = MUG, mu1 = MUL;

  f.sigma = SIGMA;

  // checks the physical's properties and else

  double RHOL = rho1;
  double RHOG = rho2;
  double MUL  = mu1;
  double MUG  = mu2;

  double ReL = RHOL*UIN*L/MUL;
  double WeL = RHOL*UIN*UIN*L/SIGMA;
  double ReG = RHOG*UIN*L/MUG;
  double WeG = RHOG*UIN*UIN*L/SIGMA;

  fprintf(ferr,"# CASE SETTING SUMMARY:\n"); 
  fprintf(ferr,"# Numerical settings \n");
  fprintf(ferr,"# MAXLEVEL uemax (abs)[m/s] minDelta [m]\n");
  fprintf(ferr,"# %d, %g, %g\n\n", MAXLEVEL, uemax, L0/(1 << MAXLEVEL));

  fprintf(ferr,"# Physical properties values:\n");
  fprintf(ferr,"# Ui L rhoL rhoG muL muG SIGMA \n");
  fprintf(ferr,"# %g %g %g %g %g %g %g \n\n",
  UIN, L, rho1, rho2, mu1, mu2, f.sigma);

  fprintf(ferr,"# Problem parameters (Len = %g):\n", L);
  fprintf(ferr,"# ReL WeL ReG WeG rhoL/rhoG muL/muG \n");
  fprintf(ferr,"# %g %g %g %g %g %g\n\n",
  ReL, WeL, ReG, WeG, rho1/rho2, mu1/mu2);

  run();
}


/**
## Careful init refinement
We first refine the channel sides one level at a time.
Then, we initialize the liquid film, refining based on wavelets.
*/

event init (t = 0) {
  scalar myscalar[];
  if (!restore (file = "restart")) 
  {
    
    
    for (int j = 0; j < 2; ++j)
    {
      baseLev++;
      double minDelta = L0/(1 << baseLev);
#if dimension==3     
      refine (fabs(y) > (l/2 - 4.*minDelta) &&
              fabs(z) > (L/2 - 4.*minDelta) &&
              level < MAXLEVEL);
#else
      refine (((fabs(y-l/2.) < 2.*minDelta) || (fabs(y+l/2.) < 2.*minDelta)) && (level < MAXLEVEL));
#endif
      
    }
    
    int count = 0;
    do
    {
      count++;
      fprintf (ferr, "# Liquid layer definition number %d\n", count);
      
      // liquid layer
      fraction (f,-max(max(fabs(y + l/2 - ht/2) - ht/2, (fabs(z) - L/2)), (5e-3 - x)));

      // the solid: all outside the centered channel of size l(y) X L(z)
      fraction (solid, max(fabs(y) - l/2, fabs(z) - L/2));
    }while (adapt_wavelet ({f, solid}, (double[]){1e-2, 1e-2}, MAXLEVEL).nf &&
            count < 5);

    dump(list = (scalar *){f, u, solid}, file = "./snapshot-init");
  }
  else
  {
    ramp = 1.;
    fprintf (ferr, "# Restored at time %g\n", t);  
  }

  fprintf (fout, "# i, t, index, vol, area, xd, yd, zd, ud, vd, wd\n");
  
  fprintf (ferr, "# t, liq Vol [m^3], UXmean[m/s], LKE [kg m^2/s^2], TotKE, Epsilon\n");

}

event ramping (i = 1; t < dtRamp*1.1; i++) {
  ramp = min(t/dtRamp, 1.);
}

event snap_ramp (t = dtRamp/10; t < dtRamp*1.1; t += dtRamp/10) {
  event ("snapshot");
}

/**
Enhance chinese method: force high inertia on solid and turn velocity to 0
before advection.
*/
event advection_term (i++) {
  // Overload density field
  foreach()
    rhov[] = cm[]*(rho(sf[]) + 1e5*solid[]);

  boundary ({alpha, mu, rho});

  // Re set solid velocity to 0 before time-step
  foreach()
    foreach_dimension()
      u.x[] *= (1 - solid[]);

  boundary(all);
}

/**
Force 0 velocity on solid (chinese method)
*/
event solid_tube(i++) {
	
  foreach()
    foreach_dimension(){
      u.x[] *= (1. - solid[]);
    }
  boundary ((scalar *){u});
}


/**
Gravity acceleration acts in the negative y direction
*/
event acceleration (i++) {
  face vector av = a;
  foreach_face(y)
    av.y[] -= 9.8*(solid[] < 1e-5);
}

/**
Output: Show 5 stats in time each 20 iterations
*/
event logfile (i += 20){

  double Vol = 0., LKE = 0., TotKE = 0., UXmean = 0., Epsilon = 0.;
  foreach(reduction (+:Vol) reduction (+:UXmean)
          reduction (+:LKE) reduction (+:TotKE) reduction (+:Epsilon))
  {
    Vol += f[]*dv();
    UXmean += f[]*dv()*u.x[];
    double DD = 0.;
    foreach_dimension()
    {
      LKE += RHOL*f[]*dv()*sq(u.x[]);
      TotKE += (RHOL*f[] + RHOG*(1 - f[]))*(1 - solid[])*dv()*sq(u.x[]);
#if dimension == 3
      DD += sq((u.x[1,0,0] - u.x[-1,0,0])/(2*Delta)) +
                 sq((u.x[0,1,0] - u.x[0,-1,0])/(2*Delta)) +
                 sq((u.x[0,0,1] - u.x[0,0,-1])/(2*Delta)); 
#else //dim == 2
      DD += sq((u.x[1,0] - u.x[-1,0])/(2*Delta)) +
                    sq((u.x[0,1] - u.x[0,-1])/(2*Delta));
#endif

    }
    
    Epsilon += 2.*mu(f[])/rho(f[])*DD; //2*nu*local square vorticity

  }
  
  fprintf (ferr, "%g %g %g %g %g %g %ld\n",
     t, Vol, UXmean/Vol, LKE/Vol, TotKE/(l*L*length), Epsilon/(l*L*length), grid->tn);
}


/**
Output: Basilisk snapshot
*/
event snapshot (t += dtSnap; t <= 1.02*tmax) 
{
  scalar omega[];
  vorticity (u, omega);
  if (t>0){
  /**
  Set name of output file: */
  char name[80];
  sprintf (name, "./snapshot-%05.2fms", t*1000);
  dump (list = (scalar *){f, u, solid,omega,skeleton}, file = name);
  }

  view (fov = 5,tx = -0.235, ty = -0.029, 
      width = 1400, height = 800);
  clear();
  draw_vof("solid", filled = 1);
  squares ("omega", map=cool_warm, min = -20000, max = 20000);
  draw_vof("f");
  save("vorticity.mp4");
}

/**
Output: Drop statistics (volume, surface, position, velocity)
*/
event droplets (t += 1e-5; t<= tmax)
{
  scalar m[];
  foreach()
    m[] = f[] > 1e-6;
  int n = tag (m);
  
  double v[n], s[n];
  coord b[n], vel[n];
  for (int j = 0; j < n; j++)
    v[j] = s[j] = b[j].x = b[j].y = b[j].z = vel[j].x = vel[j].y = vel[j].z = 0.;

 	
  foreach_leaf()
    if (m[] > 0)
    {
      int j = m[] - 1;
      
      v[j] += dv()*f[];

    	if(f[] > 1e-4 && f[] < 1. - 1e-4)
    	{
        coord m = mycs (point, f);
        double alpha = plane_alpha (f[], m);
        coord pp;
        double area = Delta*plane_area_center(m, alpha, &pp);
  #if dimension==3
        area *= Delta;
  #endif
        s[j] += area;
    	}

      coord p = {x,y,z};
      foreach_dimension()
      {
      	b[j].x += dv()*f[]*p.x;
      	vel[j].x += dv()*f[]*u.x[];
    	}
    	
    }

#if _MPI
  MPI_Allreduce (MPI_IN_PLACE, v, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce (MPI_IN_PLACE, s, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce (MPI_IN_PLACE, b, dimension*n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce (MPI_IN_PLACE, vel, dimension*n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

  for (int j = 0; j < n; j++){
      fprintf (fout, "%d %g %d %g %g %g %g %g %g %g %g\n", i, t,
	     j, v[j], s[j], b[j].x/v[j], b[j].y/v[j], b[j].z/v[j],
	     vel[j].x/v[j], vel[j].y/v[j], vel[j].z/v[j]);
  }
  fflush (fout);
}

event adapt (i++) {
  filtered_thinning(skeleton,f,1,30);

#if dimension == 2
  adapt_wavelet ({f, solid, u,skeleton}, (double[]){1e-3, 1e-3, uemax,uemax,0.1}, MAXLEVEL);
#else
  adapt_wavelet ({f, solid, u}, (double[]){1e-5, 1e-10, uemax, uemax, uemax}, MAXLEVEL);
#endif
}
