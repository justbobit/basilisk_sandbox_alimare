/**


Taken from [Nochetto et al.](#Nochetto1991).


We set an initial temperature profile:

$$
\theta_{0}(x,y) =  \left\{ \begin{array}{ll}
0.25(r^2 -1),& r\leq 1,\text{ }y\geq 2; \\
0.25(x^2 -1),& |x| < 1,\text{ }y <2; \\
(r-1),& r> 1,\text{ }y\geq 2;\\
5(|x| -1), & |x|> 1 ,\text{ }y<1;\\
(|x| -1)(3-2\cos (\pi(y-2))), & |x| > 1,\text{ }1\leq y<2.\\
\end{array}\right.
$$


with r the radius.
This initial condition with a double diffusion prolbme (Stefan) should lead to a
cusp formation. We check the correct handling of such a situation by the hybird
level-set/embedded boundary method.
*/
#define radius(x, y) (sqrt(sq(x)+sq(y)))

/**
In the original calculation the domain was $\Omega = (-2,4)\times(0,5)$, we
took a $6\times6 box with the top boundary being 6. We apply Dirichlet boundary
condition $\theta_{0}(1+t)$ on the left, right and top borders.


*/
/**
~~~bib
@Article{Nochetto1991,
  author        = {R. H. Nochetto and M. Paolini and C. Verdi},
  title         = {An Adaptive Finite Element Method for Two-Phase Stefan Problems in Two Space Dimensions. II: Implementation and Numerical Experiments},
  year          = {1991},
  volume        = {12},
  pages         = {1207-1244},
  issn          = {0196-5204},
  doi           = {10.1137/0912065},
}
~~~
*/