/**

# 2D Test code + mesh adaptation

![Output height using the phase field method](bump-vdw-2D/h.mp4)

*/

#include "grid/octree.h"
#include "../vdw3D.h"
// #include "../vdw.h"
// #include "saint-venant.h"
#include "view.h"

int LEVEL = 7;

#define rho_c 1
#define R_g 1 
#define theta 0.95
#define p_c 1

#define MU 1e-3
#define LAMBDA 1.e-3


double P0(double x)
{
  double rhop;
  rhop=x/rho_c;
  return p_c*rhop*theta*(8/(3-rhop) - 3*rhop/theta);
}


int main (int argc, char * argv[])
{
  CFL = 0.01;
  origin (-0.5, -0.5,-0.5);
  init_grid (1 << LEVEL);
  DT = 1e-2;
  run();
}

event init (i = 0)
{
  lambda=0;
  foreach()
    {
      mu[] = 1.e-5;
      rho[] = 0.1 + 0.5*exp(-200.*(sq(x)+sq(y)+sq(z)));
      q.x[] = q.y[] = q.z[] = 0.;
    }
}

event logfile (i++) {
  stats s = statsf (rho);
  fprintf (ferr, "%4.3g %5d %4.2g %4.2g %4.2g %4.2g %ld\n", t, s.min, s.max, dtmax, dt, grid->tn);
  if((s.min)<1.e-3){
    dump();
    exit(1);
  }
}

event adapt(i++;t<=0.8){
  adapt_wavelet ({rho,u.x,u.y,u.z},
    (double[]){1.e-2,1.e-2,1.e-2,1.e-2},7, 5);
}


event movie(t+=1.e-2){
  view(quat = {-0.168, -0.373, -0.071, 0.910});
  squares("rho", min = 0.01, max = 0.15);
  squares("rho", min = 0.01, max = 0.15, n = {0,1,0});
  squares("rho", min = 0.01, max = 0.15, n = {1,0,0});
  // isosurface("rho",0.11);
  save("rho.mp4");
}

