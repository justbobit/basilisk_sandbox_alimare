// Cominaison des solveur de Navier-Stokes et l'adection des temperature.


#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846
#define QUADRATIC 1

#include "../ghigo/src/myembed.h"
#include "../alimare/embed_extrapolate_3.h"

#include "../alimare/double_embed-tree.h"

#include "../ghigo/src/mycentered.h"
#include "tracer.h"
#include "../ghigo/src/mydiffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "../alimare/level_set.h"
#include "../alimare/LS_advection.h"
#include "view.h"

#define T_eq         0.05  // Theta m in the paper
#define TL_inf       1.     // T1 in the paper
#define TS_inf       0.     // T0 in the paper

#define Ra 1.e7            // Rayleigh number
#define h0 0.02

#define MAXLEVEL 10 // origin 7
#define MINLEVEL 3

//noncubic parameter
#define ratio 8.

double DT2;
scalar TL[];                // Temperature for liquid
scalar * tracers = {TL};   

scalar TS[];                // Temperature for solid
scalar * tracers2 = {TS};

scalar dist[];              // Levelset function
scalar * level_set = {dist};

vector vpc[],vpcf[];               // Extension velocity

face vector muv[];

stats s3;

double  latent_heat = 1.;
double  lambda[2];   

// no Gibbs Thomson effect.
double  epsK = 0.000, epsV = 0.000;
int aniso = 1;

int k_loop = 0;

scalar curve[];

int     nb_cell_NB;
double  NB_width ;    // length of the NB

mgstats mgd;  

TL[bottom] = dirichlet(TL_inf);
TL[embed]  = dirichlet(T_eq);

TS[top]    = dirichlet(TS_inf);
TS[embed]  = dirichlet(T_eq);

u.n[embed] = dirichlet(0.);
u.t[embed] = dirichlet(0.);

uf.n[embed] = dirichlet (0.);
uf.t[embed] = dirichlet (0.);

double nu = 1.;
face vector muc[], av[];

double TLfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y>H0)
    return Thetam;
  else
    return 1.+(Thetam-1.)*y/H0;
}

double TSfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y > H0)
    return Thetam*(y-1.)/(H0-1.);
  else
    return Thetam;
}

double energy_density(){
  double se = 0.;
  double ke = 0.;
  foreach(reduction(+:ke))
    if (cs[]>1.-1.e-6)
      ke+=dv()*(sq(u.x[])+sq(u.y[]));
  foreach(reduction(+:se)){
    if (cs[]>1.-1.e-6)
      se+=dv();
  }
  return ke/se;
}

double output_height (struct OutputFacets p){
  scalar c = p.c;
  face vector s = p.s;
  if (!p.fp) p.fp = stdout;
  if (!s.x.i) s.x.i = -1;
  double hsum = 0.;
  double xsum = 0.;
  foreach(reduction(+:hsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        hsum += y+(segment[0].y*Delta+segment[1].y*Delta)/2.;
      }
    }
  foreach(reduction(+:xsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        xsum += 1.;
      }
    }
  return hsum/xsum+ratio/2.;
  fflush (p.fp);
}

double avergeH(){
  double htot = 0.;
  double stot = 0.;
  foreach(reduction(+:htot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      htot += (y+ratio/2.)*Delta;
  foreach(reduction(+:stot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      stot += Delta;
  return htot/stot;
}

int main() {
  L0 = ratio;
  origin (-L0/2,-L0/2);
  int N = 1 << MAXLEVEL;
  init_grid (N);
  mask(y > 1.-ratio/2. ? top : none);
  mu = muc;
  a = av;
  run();
}

event init (t = 0) {
  DT2 = 0.1*(L0)/( 1 << MAXLEVEL);//0.8
  periodic(right);
  nb_cell_NB = 1 << 3 ;  //
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  foreach()
    dist[] = h0-ratio/2.-y;

  boundary ({dist});
  restriction({dist});
  LS_reinit(dist);
  vertex scalar dist_n[];
  cell2node(dist,dist_n);
  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});
  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    TL[] = TLfield(x,y,T_eq,h0);
    TS[] = TSfield(x,y,T_eq,h0);
  }
  boundary({TL,TS});
  restriction({TL,TS});
}

event stability (i++) {
/**
Sometimes, because the embedded boundary is moving, some cells have uf.x[] != 0.
&& fm.x[] == 0. Therefore, we do an ugly quickfix for cell that are incoherent
with the embedded boundary stability conditions. ()
*/
  foreach_face()
    if(fm.x[]==0. && uf.x[] !=0.) uf.x[] = 0.;
  boundary((scalar *){uf});
}


event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event renforcemask(i++){
  mask(y > 1.-ratio/2. ? top : none);
  foreach()
    if(y > 1.-ratio/2.)
      TS[] = TS_inf;
}

event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1];
  advection_LS(
  dist,
  latent_heat,
  cs,fs,
  TS,TL,
  T_eq,
  vpc,vpcf,
  lambda1,lambda2,
  epsK,epsV,aniso,
  curve,
  &k_loop,
  deltat = 0.45*L0 / (1 << grid->maxdepth),
  itredist = 10,
  tolredist = 3.e-3,
  itrecons = 60,
  tolrecons = 1.e-12,
  s_clean = 1.e-10,
  NB_width);

  foreach_face(){
    uf.x[] = 0.;
  }
  boundary((scalar *){uf});
  restriction((scalar *){uf});  
}


event tracer_diffusion (i++) {
  mgd = diffusion (TL,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("renforcemask");
  event("properties");
  mgd = diffusion (TS,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("properties");
  event("renforcemask");
}

event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;
  foreach_face(y)
    av.y[] = cs[]*Ra*(TL[]+TL[-1])/2.;
  boundary((scalar *) {av});

}

event LS_advection(i++,last){  
}

#if 1
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[] = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  adapt_wavelet ({cs,visu,u},
    (double[]){0.005,1.e-4,0.005,0.005},MAXLEVEL, MINLEVEL);

  int n = 0;
  foreach(reduction(+:n))
    n++;
  if(i%10==0) fprintf(stderr, "##nb cells %d\n", n);

}
#endif


event movie1 (t+=0.01,last){
  fprintf(stderr, "##t = %g\n",t );
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof("cs","fs");
  squares("visu", min = 0. , max = 1.);
  char name[80];
  sprintf (name, "temp-%g.png", t);
  save(name);
}

event movie(t+= 0.01, last){
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof ("cs");
  squares ("visu", min = 0, max = 1);
  save ("temperature.mp4");
}
/**

event movie2 (t+=0.0001,last){
  scalar visu[];
  vorticity(u,visu);
  boundary({visu});
  restriction({visu});
  stats vorti = statsf(visu);
  draw_vof("cs","fs");
  squares("visu",max = 0.25*vorti.max,min = 0.25*vorti.min);
  char name[80];
  sprintf (name, "omega-%g.png", t);
  save(name);

 
//   // draw_vof ("cs", "fs", fc = {0.9, 0.9, 0.9});
//   // squares ("TL", min = 0, max = 1);
//   // save ("TL.mp4");
}
**/
// event movie2 (i = 2; i+=10){
//   scalar Theta[];
//   foreach()
//     Theta[] = u.y[];
//   boundary({Theta});
//   restriction({Theta});
//   output_ppm(Theta,linear = true,spread = 2, file ="uy.mp4",n=200);
// }



event Calculation(i++){
  double Rae = Ra*(1-T_eq)*pow(output_height(cs,stdout),3);
  double Hhalf = 0.5*output_height(cs,stdout);
  char name2[100];
  sprintf(name2,"out2");
  static FILE * fpp1 = fopen(name2,"w");
  foreach(){
     if((y+ratio/2. > Hhalf-0.01)&&(y+ratio/2. <  Hhalf+0.01))
    fprintf(fpp1,"%g %g %g \n",t,x,TL[]);        
  }
}
// event logcurve(i++){
//   if(i%1000==1) {
//     output_facets (cs, stdout);
//   }
//   curvature(cs,curve);
//   boundary({curve});
//   stats s2 = statsf(curve);
//   fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
// }




event final (t = 0.1)
{
  // char name[80];
  // sprintf (name, "mu-%g.png", t);
  // output_ppm (TL, file = name, n = 200, linear = true, spread = 2);
  draw_vof ("cs", "fs");
  squares ("TL", min = 0, max = 1);
  save ("TL.png");
  dump();
}



/**

~~~gnuplot Kinetic energy density as a function of Effective Rayleigh number
set key left
set yrange[0.000000000001 : 0.001]
set logscale y
set xrange[400:2000]
plot 'out1' u 2:3 w p t '0.33',  \
'out2' u 2:3 w p t '0.37',  \
'out3' u 2:3 w p t '0.41',  \
'out4' u 2:3 w p t '0.45',  \
~~~

**/
