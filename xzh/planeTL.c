// Cominaison des solveur de Navier-Stokes et l'adection des temperature.


#include "embed.h"
#include "navier-stokes/centered.h"
//#include "../alimare/centered_alex.h"
#include "tracer.h"
#include "diffusion.h"
#include "view.h"
#include "../alimare/alex_functions.h"
scalar T[];
scalar * tracers = {T};

face vector muv[];

mgstats mgd;
T[bottom]     = dirichlet(1.);
T[embed]     = dirichlet(0.1);

// T[left]    = y<32? dirichlet(1):dirichlet(0);
// T[right]   = y<32? dirichlet(1):dirichlet(0);
u.n[embed] = dirichlet(0.);
u.t[embed] = dirichlet(0.);
// u.n[left]     = neumann(0.);
// u.n[right]    = neumann(0.);

// u.n[top]      = dirichlet(0.);
// u.n[bottom]   = dirichlet(0.);


// p[left]       = dirichlet(0.);
// pf[left]      = dirichlet(0.);

// p[right]      = dirichlet(0.);
// pf[right]     = dirichlet(0.);

double nu = 1.;
face vector muc[], av[];

stats s3;
double TLfield (double x, double y, double Thetam, double H0){
  y+=0.5;
  H0+=0.5;
  if (y>H0)
    return Thetam;
  else
    return 1+(Thetam-1)*y/H0;
}

int main() {
  DT = 0.1;
  periodic(left);
  L0 = 1;
  origin (-L0/2,-L0/2);
  int N = 1 << 5;
  init_grid (N);
  mu = muc;
  a = av;
  run();
}

event init (t = 0) {
  vertex scalar phi[];
  foreach_vertex()
    phi[] = -y;
  boundary ({phi});
  fractions (phi, cs, fs);
  foreach()
    T[] = TLfield(x,y,0.01,0.);
  boundary({T});
}

event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event tracer_diffusion (i++) {
  mgd = diffusion (T,dt,mu,theta = cm);
}

event acceleration (i++) {
  s3 = statsf(g.y);
  fprintf(stderr, "%g %g\n", s3.min, s3.max);

  foreach_face(x)
    av.x[] = 0;

  foreach_face(y)
    av.y[] = cs[]*15180*(T[]+T[-1])/2.;
  boundary((scalar *) {av});
}

event movie1 (i++,last){
  draw_vof ("cs", "fs", fc = {0.9, 0.9, 0.9});
  squares ("T", min = 0,max = 1,linear = true);
  save ("movie.mp4");
  Point p = locate(0.,-0.1);

  double val = capteur(p, T);
  double val2 = capteur(p, u.y);
  fprintf(stderr, "##valeur temperature %g vitesse %g \n", val,val2);
}

event movie2 (i++,last){
  scalar Theta[];
  foreach()
    Theta[] = u.y[];
  boundary({Theta});
  output_ppm(Theta,linear = true,spread = 2, file ="uy.mp4",n=200);


  Point p = locate(0.,-0.1);

  double val = capteur(p, T);
  fprintf(stderr, "##valeur temperature %g\n", val);

}


// event draw_inter (i=1;i+=1){
//   view ();
//   draw_vof ("cs", "fs");
//   squares ("u.x", linear = 1, spread = -1);
//   cells(); 
//   char name[70];
//   sprintf(name,"mypicture.mp4");
//   save (name);
// }


event adapt (i++)
  adapt_wavelet ((scalar*){cs,T, u}, (double[]){0.01, 0.05, 0.02, 0.02}, 9, 5);

event final (i = 1)
{
  char name[80];
  sprintf (name, "mu-%g.png", t);
  output_ppm (T, file = name, n = 200, linear = true, spread = 2);
  dump();
}

