/**
#Melt of a solid particle

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = 1$. The ice particle is initially
at $T_S = -1$.

The temperature on the interface is derived from the Gibbs-Thomson relation,
withtout any anisotropic effect :
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$
*/
#define BICUBIC 1
#define DOUBLE_EMBED  1
#define LevelSet      1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846
#define Centered 1

#define JACOBI 1



#if Centered
#include "embed.h"
#include "../alimare/double_embed-tree.h"
#include "./centered_xzh.h"
#include "./advection_B.h"
#include "diffusion.h"

#else
#include "embed.h"
#include "../alimare/double_embed-tree.h"
#include "../alimare/advection_A.h"
#include "diffusion.h"

#endif

#include "fractions.h"
#include "curvature.h"

#include "view.h"
#include "../alimare/level_set.h"
#include "../alimare/LS_advection.h"
#include "../alimare/LS_curvature.h"

// #include "./center_RB.h"

#define T_eq         0.05
#define TL_inf       1.
#define TS_inf       0.

#define tstart 0.

// temporary variables. comparing curvature and redistancing methods.
#define LS_curve 0
#define redist 1

// Numerical parameters for the case A in the paper 
#define Ra 256
#define St 10
#define epsilon 4.e-3
#define alpha 2500
#define h0 0

/**
Setup of the numerical parameters
*/  
int MAXLEVEL = 7; 
int MINLEVEL = 3;
double  H0;


/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[], dist[];
vector vpc[];

vector u2[];
face vector uf2[];


scalar * tracers   = {TL};
scalar * tracers2  = {TS};
scalar * level_set = {dist};
scalar * LS_speed  = {vpc.x,vpc.y};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];


double  latent_heat = 10.;
double  lambda[2];    // thermal capacity of each material
#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
double  epsK = 0.005, epsV = 0.001;
#else
double  epsK = 0.000, epsV = 0.000;
#endif

#define GT_aniso 0
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif

int     nb_cell_NB;
double  NB_width ;    // length of the NB

scalar curve[];

TL[embed] = dirichlet(T_eq);
TL[bottom]  = dirichlet(TL_inf); 

TS[embed]  = dirichlet(T_eq);
TS[top] = dirichlet(TS_inf); 

#if Centered

u.n[embed]  = dirichlet(0.);
u.t[embed]  = dirichlet(0.);
#endif


/**
Initial geometry definition. Here the interface equation is :

$$
r\left(1+ 0.2 *cos(8\theta) \right) - R
$$
where $r = \sqrt{x^2 + y^2}$, $\theta = \arctan(x,y)$ and $R = \frac{L_0}{5}$

Notice that the initial dist[] field is not really a distance, it is modified
after a few iterations of the LS_reinit() function.
*/



/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;
double nu = 0.0001;
face vector muc[], av[];


double Tfield (double x, double y, double Thetam, double H0){
  y+=0.5;
  H0+=0.5;
  if (y>H0)
    return Thetam*(y-1)/(H0-1);
  else
    return 1+(Thetam-1)*y/H0;
}

int main() {
  periodic(right);
  L0 = 1;
  origin (-L0/2,-L0/2);
  TOLERANCE = 1.e-6;
  NITERMIN = 5;
  int N     = 1 << MAXLEVEL;
  init_grid (N);

#if Centered

  mu = muc;
  a = av;
#endif
  run();
}


event init(t=0){
  DT         = 0.01*L0 / (1 << MAXLEVEL);  // Delta
  nb_cell_NB = 1 << 3 ;               // number of cell in the 
                                      // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  face vector g[];

  lambda[0] = 1.;
  lambda[1] = 1.;

  foreach() {
    //dist[] = clamp(-geometry(x,y,L0/3),-1.5*NB_width, 1.5*NB_width);
    //dist[] = cercle(x,y,0.2);
    //dist[] = circle(x,y,center1,0.1);
    dist[] = -tanh((y-h0)/2/sqrt(2)/epsilon);
  }

  boundary ({dist});
  restriction({dist});

  LS_reinit(dist);

  vertex scalar dist_n[];

  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});

  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  
  curvature(cs,curve);
  // curvature_LS(dist,curve);

  boundary({curve});


  foreach() {
    TL[] = Tfield(x,y,T_eq,0.);
    TS[] = Tfield(x,y,T_eq,0.);
  }

  boundary({TL,TS});
  restriction({TL,TS});
}

event change_velocity(i++){
  if(i%2==1){
    foreach()
      foreach_dimension(){
        u2.x[] = u.x[];
        u.x[] = 0.;
      }
    boundary ((scalar *){u2});
    foreach_face(){
      uf2.x[] = uf.x[];
      uf.x[] = 0.;
    } 
    boundary ((scalar *){uf2});
  }
  if(i%2==0 && i>1){
    foreach()
      foreach_dimension(){
        u.x[] = u2.x[];
        u2.x[] = 0.;
      }
    boundary ((scalar *){u});
    foreach_face(){
      uf.x[] = uf2.x[];
      uf2.x[] = 0.;
    }
    boundary ((scalar *){uf}); 
  }
}

event properties (i++){
  foreach_face()
    muc.x[] = fs.x[]*nu;
  boundary((scalar*){muc});
}


event acceleration(i++,last){
	foreach_face(x)
		av.x[] = 0;
	foreach_face(y)
		av.y[] = 100.*(cs[]*TL[] + (1.-cs[])*TS[]);
  boundary ((scalar *){av});
}

event tracer_diffusion(i++){

  int kk;
  mgstats mg1;
  for (kk=1;kk<=4;kk++){
    if(i%2==0){
      boundary({TL});
      mg1 = diffusion(TL, dt, D = muc, theta = cs);
    }
    else{
      boundary({TS});
      mg1 = diffusion(TS, dt, D = muc, theta = cs);
    }
    if(mg1.resa > TOLERANCE) {
      scalar ffsx[], ffsy[];
      foreach(){
        ffsx[] = fs.x[];
        ffsy[] = fs.y[];
      }
      boundary({ffsx,ffsy});
      dump();
      exit(1);
    }
  }
}

event LS_advection(i++,last){  
  if(i%2 == 1){
    invertcs(cs,fs);
    double lambda1 = lambda[0], lambda2 = lambda[1];
    
    advection_LS(
    dist,
    latent_heat,
    cs,fs,
    TS,TL,
    vpc,
    lambda1,lambda2,
    epsK,epsV,aniso,
    curve,
    &k_loop,
    k_limit = 0,
    overshoot = 0.3,
    deltat = 0.45*L0 / (1 << grid->maxdepth),
    itredist = 20,
    itrecons = 300,
    tolrecons = 1.e-5,
    s_clean = 1.e-10,
    NB_width);

    invertcs(cs,fs);
  }
}
/**
We need to change cs to 1-cs for all the level-set functions to work.
*/


#if DOUBLE_EMBED
event double_calculation(i++,last){
  invertcs(cs,fs);
}
#endif


event movies ( i++,last;t<0.02)
{
  if(i%10==1) {
    output_facets (cs, stdout);
  }
}

event movie1 (i = 0; i+=2){
  draw_vof ("cs", "fs");
  squares ("TL", min =0, max = 1, linear = true);
  save ("TL.mp4");
}

event movie2 (i = 0; i+=2){
  draw_vof ("cs", "fs");
  squares ("TS", min =0, max = 1,linear = true);
  save ("TS.mp4");
}

event movie3 (i = 0; i+=2){
  scalar visu[];
  foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
  boundary({visu});
  restriction({visu});
  draw_vof ("cs", "fs");
  squares ("visu", min =0, max = 1,linear = true);
  save ("visu.mp4");
}

event movie4 (i = 0; i+=10){
  scalar omega[];
  vorticity (u, omega);
  draw_vof ("cs", "fs");
  squares ("omega", min =0, max = 100,linear = true);
  save ("vorticity.mp4");
}


// event movie3 (i = 0; i+=2){
//   scalar visu[];
//   foreach(){
//       visu[] = u.y[] ;
//     }
//   boundary({visu});
//   restriction({visu});
//   draw_vof ("cs", "fs");
//   squares ("visu", min =0, max = 1);
//   save ("u.mp4");
// }

// event final (t = 0.35)
// {
//   char name[80];
//   sprintf (name, "final.png", t);
//   scalar Tfinal[];
//   foreach()
//     Tfinal[] = cs[]*TL[]+(1.-cs[])*TS[];
//   output_ppm (Tfinal, file = name, n = 200, linear = true, spread = 2);
// }

#if 1
event adapt (i++, last) {

  if(i%2 == 1 ){


    foreach_cell(){
      cs2[] = 1.-cs[];
    }
    foreach_face(){
      fs2.x[]      = 1.-fs.x[];
    }

    boundary({cs,cs2,fs,fs2});
    fractions_cleanup(cs,fs,smin = 1.e-14);
    fractions_cleanup(cs2,fs2,smin = 1.e-14);
    restriction({cs,cs2,fs,fs2});
    int n=0;
    stats s2 = statsf(curve);
    fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});

    adapt_wavelet ({cs,visu,curve},
      (double[]){1.e-3,1.e-3,1.e-3},MAXLEVEL, MINLEVEL);
    foreach(reduction(+:n)){
      n++;
    }
    phase_change_velocity_LS_embed (cs, fs ,TL, TS, vpc, latent_heat,
      lambda,epsK, epsV, aniso);
    // curvature(cs,curve);
    // boundary({curve});
    curvature_LS(dist, curve);


    fprintf(stderr, "##nb cells %d\n", n);
  }
}
#endif

// event debug1(i=6){
//   foreach()
//     fprintf(stderr, "TL = %g\n", TL[]);
//   exit(1);
// }




/**

~~~gnuplot Temperature on the boundary
set key left
plot 'log' u 1:2 w l t 'curvature min',  \
     'log' u 1:3 w l  t 'curvature max'
~~~

~~~gnuplot Evolution of the interface (zoom)
set term @PNG enhanced size 1000,1000
set output "Interface.png"
set size ratio -1
set key top right
unset xlabel
unset xtics
unset ytics
unset border
unset margin
unset ylabel
plot 'out' w l lw 2
~~~
**/
