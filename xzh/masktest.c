// Cominaison des solveur de Navier-Stokes et l'adection des temperature.


#define BICUBIC 1
#define DOUBLE_EMBED  1
#define LevelSet      1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846

#include "embed.h"
#include "../alimare/double_embed-tree.h"

#include "navier-stokes/centered.h"
#include "tracer.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "view.h"

#include "../alimare/level_set.h"
#include "../alimare/LS_advection.h"

#define T_eq         0.1  // Theta m in the paper
#define TL_inf       1.     // T1 in the paper
#define TS_inf       0.     // T0 in the paper

#define Ra 15180              // Rayleigh number
#define St 10     
#define epsilon 4.e-3
#define alpha 2500
#define h0 0.45

#define MAXLEVEL 9 // origin 7
#define MINLEVEL 3

//noncubic parameter
#define ratio 8.


scalar TL[];                // Temperature for liquid
scalar * tracers = {TL};   

scalar TS[];                // Temperature for solid
scalar * tracers2 = {TS};

scalar dist[];              // Levelset function
scalar * level_set = {dist};

vector vpc[];               // Extension velocity
scalar * LS_speed  = {vpc.x,vpc.y};

face vector muv[];

stats s3;

double  latent_heat = 10.;
double  lambda[2];   

// no Gibbs Thomson effect.
double  epsK = 0.000, epsV = 0.000;
int aniso = 1;

int k_loop = 0;

scalar curve[];

int     nb_cell_NB;
double  NB_width ;    // length of the NB

mgstats mgd;  

TL[bottom]    = dirichlet(TL_inf);
TL[embed]     = dirichlet(T_eq);

TS[top]   = dirichlet(TS_inf);
TS[embed]     = dirichlet(T_eq);

u.n[embed]    = dirichlet(0.);
u.t[embed]    = dirichlet(0.);


double nu = 1.;
face vector muc[], av[];

double TLfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y>H0)
    return Thetam;
  else
    return 1.+(Thetam-1.)*y/H0;
}

double TSfield (double x, double y, double Thetam, double H0){
  y += ratio/2.;
  if (y > H0)
    return Thetam*(y-1.)/(H0-1.);
  else
    return Thetam;
}

double energy_density(){
  double se = 0.;
  double ke = 0.;
  foreach(reduction(+:ke))
    if (cs[]>1.-1.e-6)
      ke+=dv()*(sq(u.x[])+sq(u.y[]));
  foreach(reduction(+:se)){
    if (cs[]>1.-1.e-6)
      se+=dv();
  }
  return ke/se;
}

double output_height (struct OutputFacets p){
  scalar c = p.c;
  face vector s = p.s;
  if (!p.fp) p.fp = stdout;
  if (!s.x.i) s.x.i = -1;
  double hsum = 0.;
  double xsum = 0.;
  foreach(reduction(+:hsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        hsum += y+(segment[0].y*Delta+segment[1].y*Delta)/2.;
      }
    }
  foreach(reduction(+:xsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        xsum += 1.;
      }
    }
  return hsum/xsum+ratio/2.;
  fflush (p.fp);
}

double avergeH(){
  double htot = 0.;
  double stot = 0.;
  foreach(reduction(+:htot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      htot += (y+ratio/2.)*Delta;
  foreach(reduction(+:stot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      stot += Delta;
  return htot/stot;
}

int main() {
  L0 = 8;
  origin (-L0/2,-L0/2);
  int N = 1 << MAXLEVEL;
  init_grid (N);
  mask(y > 1.-ratio/2. ? top : none);
  mu = muc;
  a = av;
  run();
}

event init (t = 0) {
  DT = 0.01*(L0)/( 1 << MAXLEVEL);//0.8
  periodic(right);
  nb_cell_NB = 1 << 3 ;  //
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  foreach()
    dist[] = h0-ratio/2.-y;

  boundary ({dist});
  restriction({dist});
  LS_reinit(dist);
  vertex scalar dist_n[];
  cell2node(dist,dist_n);
  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});
  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    TL[] = TLfield(x,y,T_eq,h0);
    TS[] = TSfield(x,y,T_eq,h0);
  }
  boundary({TL,TS});
  restriction({TL,TS});
}

event stability (i++) {
/**
Sometimes, because the embedded boundary is moving, some cells have uf.x[] != 0.
&& fm.x[] == 0. Therefore, we do an ugly quickfix for cell that are incoherent
with the embedded boundary stability conditions. ()
*/
  foreach_face()
    if(fm.x[]==0. && uf.x[] !=0.) uf.x[] = 0.;
  boundary((scalar *){uf});
}


event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event renforcemask(i++){
  mask(y > 1.-ratio/2. ? top : none);
  foreach()
    if(y > 1.-ratio/2.)
      TS[] = TS_inf;
}

event tracer_diffusion (i++) {
  mgd = diffusion (TL,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("renforcemask");
  event("properties");
  mgd = diffusion (TS,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("properties");
  event("renforcemask");
}

event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;
  foreach_face(y)
    av.y[] = cs[]*Ra*(TL[]+TL[-1])/2.;
  boundary((scalar *) {av});

}

event LS_advection(i++,last){  
  double lambda1 = lambda[0], lambda2 = lambda[1];
  
  advection_LS(
  dist,
  latent_heat,
  cs,fs,
  TS,TL,
  T_eq,
  vpc,
  lambda1,lambda2,
  epsK,epsV,aniso,
  curve,
  &k_loop,
  k_limit = 0,
  overshoot = 1.,
  deltat = 0.01*L0 / (1 << grid->maxdepth),
  itredist = 5,
  tolredist = 2.e-2,
  itrecons = 30,
  tolrecons = 1.e-5,
  s_clean = 1.e-10,
  NB_width);

}

#if 0
event adapt (i++, last) {

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[] = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  adapt_wavelet ({cs,visu,u},
    (double[]){0.005,0.001,0.005,0.005},MAXLEVEL, MINLEVEL); // cs, visu, u.x,
  // u.y
  foreach(reduction(+:n)){
    n++;
  }
  phase_change_velocity_LS_embed (cs, fs ,TL, TS, T_eq, vpc, latent_heat,
    lambda, epsK, epsV, aniso);
}
#endif

event movie1 (t+=0.005,last){
  fprintf(stderr, "t = %g\n",t );
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof("cs");
  squares("visu", min = 0. , max = 1.);
  save("temperature.mp4");

  
  // draw_vof ("cs", "fs", fc = {0.9, 0.9, 0.9});
  // squares ("TL", min = 0, max = 1);
  // save ("TL.mp4");
}

event movie2 (t+=0.005,last){
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  draw_vof("cs");
  squares("TS", min = TS_inf , max = T_eq);
  save("TS.mp4");

  
  // draw_vof ("cs", "fs", fc = {0.9, 0.9, 0.9});
  // squares ("TL", min = 0, max = 1);
  // save ("TL.mp4");
}
// event movie2 (i = 2; i++){
//   scalar Theta[];
//   foreach()
//     Theta[] = u.y[];
//   boundary({Theta});
//   restriction({Theta});
//   output_ppm(Theta,linear = true,spread = 2, file ="uy.mp4",n=200);
// }

event Calculation(i++){
  double Rae = Ra*(1-T_eq)*pow(output_height(cs,stdout),3);
  fprintf(stdout, "%g %g %g %g \n", t, Rae, energy_density(), output_height(cs,stdout));
  fprintf(stderr, "t = %g \n", t);
}


event final (t = 1.)
{
  // char name[80];
  // sprintf (name, "mu-%g.png", t);
  // output_ppm (TL, file = name, n = 200, linear = true, spread = 2);
  draw_vof ("cs", "fs");
  squares ("TL", min = 0, max = 1);
  save ("TL.png");
  dump();
}



/**

~~~gnuplot Kinetic energy density as a function of Effective Rayleigh number
set key left
set yrange[0.000000000001 : 0.001]
set logscale y
set xrange[400:2000]
plot 'out1' u 2:3 w p t '0.33',  \
'out2' u 2:3 w p t '0.37',  \
'out3' u 2:3 w p t '0.41',  \
'out4' u 2:3 w p t '0.45',  \
~~~

**/
