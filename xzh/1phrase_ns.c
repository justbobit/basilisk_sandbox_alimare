// Cominaison des solveur de Navier-Stokes et l'adection des temperature.


#include "embed.h"
#include "navier-stokes/centered.h"
//#include "../alimare/centered_alex.h"
#include "tracer.h"
#include "diffusion.h"
#include "view.h"

scalar T[];
scalar * tracers = {T};

face vector muv[];

mgstats mgd;
T[bottom]     = dirichlet(1.);
T[top]     = dirichlet(0.);
T[embed]     = dirichlet(0.);

// T[left]    = y<32? dirichlet(1):dirichlet(0);
// T[right]   = y<32? dirichlet(1):dirichlet(0);
u.n[embed] = dirichlet(0.);
u.t[embed] = dirichlet(0.);
// u.n[left]     = neumann(0.);
// u.n[right]    = neumann(0.);

// u.n[top]      = dirichlet(0.);
// u.n[bottom]   = dirichlet(0.);


// p[left]       = dirichlet(0.);
// pf[left]      = dirichlet(0.);

// p[right]      = dirichlet(0.);
// pf[right]     = dirichlet(0.);

double nu = 0.0001;
face vector muc[], av[];

int main() {
  DT = 0.1;
  periodic(left);
  L0 = 4;
  origin (-L0/2,-L0/2);
  int N = 1 << 5;
  init_grid (N);
  mu = muc;
  a = av;
  run();
}

event init (t = 0) {
  vertex scalar phi[];
  foreach_vertex()
    phi[] = -y;
  boundary ({phi});
  fractions (phi, cs, fs);
  foreach()
    T[] = -0.5*y;
}

event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event tracer_diffusion (i++) {
  mgd = diffusion (T,dt,mu,theta = cm);
}

event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;

  foreach_face(y)
    av.y[] = 1000*T[];
  boundary((scalar *) {av});
}

event movie1 (i = 1; i+=1){
  draw_vof ("cs", "fs", filled = -1, fc = {0.9, 0.9, 0.9});
  squares ("T", min = 0,max = 1,linear = true);
  save ("movie.mp4");
}

event movie2 (i = 1; i+=1){
  scalar Theta[];
  foreach()
    Theta[] = u.y[];
  output_ppm(Theta,linear = true,spread = 2, file ="uy.mp4",n=200);
}


// event draw_inter (i=1;i+=1){
//   view ();
//   draw_vof ("cs", "fs");
//   squares ("u.x", linear = 1, spread = -1);
//   cells(); 
//   char name[70];
//   sprintf(name,"mypicture.mp4");
//   save (name);
// }


event final (i = 100)
{
  char name[80];
  sprintf (name, "mu-%g.png", t);
  output_ppm (T, file = name, n = 200, linear = true, spread = 2);
}

event adapt (i++)
  adapt_wavelet ((scalar*){cs,T, u}, (double[]){0.01, 0.05, 0.02, 0.02}, 9, 5);
