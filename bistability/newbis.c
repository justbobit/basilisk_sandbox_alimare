// Cominaison des solveur de Navier-Stokes et l'adection des temperature.


#define QUADRATIC 1
#define DOUBLE_EMBED  1
#define LevelSet      1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846
#define GHIGO 1

#include "../ghigo/src/myembed.h"
#include "../alimare/embed_extrapolate_3.h"
#include "../alimare/double_embed-tree.h"

#include "../ghigo/src/mycentered.h"
#include "tracer.h"
#include "../ghigo/src/mydiffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "view.h"

#include "../alimare/level_set.h"
#include "../alimare/LS_advection.h"

#define T_eq         0.8  // Theta m in the paper
#define TL_inf       1.     // T1 in the paper
#define TS_inf       0.     // T0 in the paper

#define Ra 500000              // Rayleigh number
double h0;

#define MAXLEVEL 9 // origin 7
#define MINLEVEL 6

//noncubic parameter
#define ratio 8.


scalar TL[];                // Temperature for liquid
scalar * tracers  = {TL};   

scalar TS[];                // Temperature for solid
scalar * tracers2 = {TS};

scalar dist[];              // Levelset function
scalar * level_set= {dist};

vector vpc[],vpcf[];               // Extension velocity

face vector muv[];

double DT2;

stats s3;

double  latent_heat = 1.;
double  lambda[2];   

// no Gibbs Thomson effect.
double  epsK = 0.000, epsV = 0.000;
double eps4 = 0.;

int k_loop = 0;

scalar curve[];

int     nb_cell_NB;
double  NB_width ;    // length of the NB

mgstats mgd;  

TL[bottom]    = dirichlet(TL_inf);
TL[embed]     = dirichlet(T_eq);

TS[top]   = dirichlet(TS_inf);
TS[embed]     = dirichlet(T_eq);




double nu = 1.;
face vector muc[], av[];

double TLfield (double y, double frac, double Thetam, double H0){
  y += ratio/2.;
  if (frac > 0.)
    return 1.+(Thetam-1.)*y/H0;
  else
    return Thetam;
}

double TSfield (double y, double frac, double Thetam, double H0){
  y += ratio/2.;
  if (frac < 1.)
    return Thetam*(y-1.)/(H0-1.);
  else
    return Thetam;
}

double energy_density(){
  double se = 0.;
  double ke = 0.;
  foreach(reduction(+:ke))
    if (cs[]>1.-1.e-6)
      ke+=dv()*(sq(u.x[])+sq(u.y[]));
  foreach(reduction(+:se)){
    if (cs[]>1.-1.e-6)
      se+=dv();
  }
  return ke/se;
}

double output_height (struct OutputFacets p){
  scalar c = p.c;
  face vector s = p.s;
  if (!p.fp) p.fp = stdout;
  if (!s.x.i) s.x.i = -1;
  double hsum = 0.;
  double xsum = 0.;
  foreach(reduction(+:hsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        hsum += y+(segment[0].y*Delta+segment[1].y*Delta)/2.;
      }
    }
  foreach(reduction(+:xsum))
    if (c[] > 1e-6 && c[] < 1. - 1e-6) {
      coord n = facet_normal (point, c, s);
      double alpha1 = plane_alpha (c[], n);
      coord segment[2];
      if (facets (n, alpha1, segment) == 2){
        xsum += 1.;
      }
    }
  return hsum/xsum+ratio/2.;
  fflush (p.fp);
}

double avergeH(){
  double htot = 0.;
  double stot = 0.;
  foreach(reduction(+:htot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      htot += (y+ratio/2.)*Delta;
  foreach(reduction(+:stot))
    if (cs[]>1.e-6&&cs[]<1.-1.e-6)
      stot += Delta;
  return htot/stot;
}

int main(int argc, char * argv[]) {
  if(argc > 1){
    h0 = atof(argv[1]);
  }
  else
    h0 = 0.2; // default value

  TOLERANCE = 1.e-6;
  L0 = 8;
  origin (-L0/2,-L0/2);
  int N = 1 << MAXLEVEL;
  init_grid (N);
  mask(y > 1.-ratio/2. ? top : none);
  mu = muc;
  a  = av;
  DT2= 0.1*(L0)/( 1 << MAXLEVEL);
  run();
}

event init (t = 0) {
  periodic(right);

  nb_cell_NB= 1 << 2 ;  //
  NB_width  = nb_cell_NB * L0 / (1 << MAXLEVEL);

  foreach()
    dist[] = h0-ratio/2.-y;

  boundary ({dist});
  restriction({dist});
  LS_reinit(dist);
  vertex scalar dist_n[];
  cell2node(dist,dist_n);
  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs);
  boundary({cs,fs});
  restriction({cs,fs});
  foreach_face(){
    vpc.x[] = 0.;
  }
  boundary((scalar *){vpc,av});
  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    TL[] = TLfield(y,cs[],T_eq,h0);
    TS[] = TSfield(y,cs[],T_eq,h0);
  }
  boundary({TL,TS});
  restriction({TL,TS});  

  u.n[embed]    = dirichlet(0.);
  u.t[embed]    = dirichlet(0.);

  uf.n[embed]    = dirichlet(0.);
  uf.t[embed]    = dirichlet(0.);
  fprintf(stderr, "######\n");
  fprintf(stderr, "%g\n", h0);
  fprintf(stderr, "##TEST\n" );
  // view (fov = 2.75, ty = 0.44);
  // draw_vof("cs");
  // squares("TL", min = 0. , max = 1.);
  // save("temperature1.png");

  // view (fov = 1.5, ty = 0.48);
  // draw_vof("cs");
  // squares("TL", min = 0. , max = 1.);
  // save("temperature2.png");

  // view (fov = 1, ty = 0.48);
  // draw_vof("cs");
  // squares("TL", min = 0. , max = 1.);
  // save("temperature3.png");
  // exit(1);
}

event stability (i++) {
/**
Sometimes, because the embedded boundary is moving, some cells have uf.x[] != 0.
&& fm.x[] == 0. Therefore, we do an ugly quickfix for cell that are incoherent
with the embedded boundary stability conditions. ()
*/
  foreach_face()
    if(fm.x[]==0. && uf.x[] !=0.) uf.x[] = 0.;
  boundary((scalar *){uf});
}


event properties (i++){
  foreach_face()
    muc.x[] = fm.x[]*nu;
  boundary((scalar*){muc});
}

event renforcemask(i++){
  mask(y > 1.-ratio/2. ? top : none);
  foreach()
    if(y > 1.-ratio/2.)
      TS[] = TS_inf;
}

event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
    dist,latent_heat,cs,fs,TS,TL,T_eq,
    vpc,vpcf,lambda1,lambda2,
    epsK,epsV,eps4,deltat=0.45*L0/(1<<MAXLEVEL),
    itrecons = 30,tolrecons = 1.e-5,NB_width);
  DT = 0.4*timestep_LS(vpcf,DT2,dist,NB_width);
  tnext = t+DT;
  dt = DT;
}

event tracer_diffusion (i++) {
  advection_LS(
  dist,
  cs,fs,
  TS,TL,
  T_eq,
  vpcf,
  curve,
  &k_loop,
  itredist = 5,
  tolredist = 3.e-3,
  s_clean = 1.e-10,
  NB_width);
  boundary({TL});
  event("properties");
  mgd = diffusion (TL,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("properties");
  event("renforcemask");
  mgd = diffusion (TS,dt,muc,theta = cm);
  invertcs(cs,fs);
  event("properties");
  event("renforcemask")
}

event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;
  foreach_face(y)
    av.y[] = cs[]*Ra*(TL[]+TL[-1])/2.;
  boundary((scalar *) {av});

}

#if 1
event adapt (i++, last ) {

  scalar normvpc[];

  foreach(){
    if(fabs(dist[]< NB_width)){
      coord temp;
      foreach_dimension()
        temp.x = vpcf.x[];

      normvpc[] = norm2(temp);
    }
    else{
      normvpc[] = 0.;
    }
  }

  boundary({normvpc});
  restriction({normvpc});

  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
      fs2.x[] = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = 1.e-10);
  fractions_cleanup(cs2,fs2,smin = 1.e-10);
  restriction({cs,cs2,fs,fs2});
  int n=0;
  boundary({TL,TS});
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});

  if(t<0.3) //very low threshold, we start far from equilibrium, any coarsening causes instabilities in the simulation.
    adapt_wavelet ({cs,visu,u,normvpc},
      (double[]){0.005,1.e-6,0.005,0.005,1.e-3},MAXLEVEL, MINLEVEL); // cs, visu, u.x,
  else
    adapt_wavelet ({cs,visu,u,normvpc},
      (double[]){0.005,1.e-3,0.005,0.005,1.e-3},MAXLEVEL, MINLEVEL); // cs, visu, u.x,
  // u.y
  foreach(reduction(+:n)){
    n++;
  }
  if(i%10==0) fprintf(stderr, "##nb cells %d\n", n);
}
#endif



event movie1 (i++,last){
  fprintf(stderr, "t = %g\n",t );
  scalar visu[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
  }
  boundary({visu});
  restriction({visu});
  view (fov = 1, tx = 0.1, ty = 0.48);
  draw_vof("cs");
  squares("visu", min = 0. , max = 1.);
  save("temperature.mp4");
}




event Calculation(i++){
  double Rae = Ra*(1-T_eq)*pow(output_height(cs,stdout),3);
  fprintf(stdout, "%g %g %g %g \n", t, Rae, energy_density(), output_height(cs,stdout));
  fprintf(stderr, "t = %g \n", t);
}


event final (t = 0.03)
{
  draw_vof ("cs", "fs");
  squares ("TL", min = 0, max = 1);
  save ("TL.png");
  dump();
}



/**

~~~gnuplot Kinetic energy density as a function of Effective Rayleigh number
set key left
set yrange[0.000000000001 : 0.001]
set logscale y
set xrange[400:2000]
plot 'out1' u 2:3 w p t '0.33',  \
'out2' u 2:3 w p t '0.37',  \
'out3' u 2:3 w p t '0.41',  \
'out4' u 2:3 w p t '0.45',  \
~~~

**/
